#!/usr/bin/env bash

aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 339898008043.dkr.ecr.eu-west-2.amazonaws.com
cd ..
docker build -t roi-local:latest ./
cd docker
docker service update --image roi-local:latest --force roi-roi_app
docker stack deploy -c docker-compose.yml roi --with-registry-auth --resolve-image always --prune
