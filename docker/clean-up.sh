#!/usr/bin/env bash

echo "Removing roi stack"
docker stack rm roi

echo "Waiting for stack to be removed"
sleep 20

echo "Removing roi volumes"
docker volume rm roi_redis roi_mysql
