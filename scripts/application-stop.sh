#!/usr/bin/env bash
if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    SUPERVISOR_HORIZON="myroi-test-horizon"
    cd /var/www/myroi/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    SUPERVISOR_HORIZON="myroi-staging-horizon"
    cd /var/www/myroi/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    SUPERVISOR_HORIZON="myroi-production-horizon"
    cd /var/www/myroi/production
fi

echo "=======================" > ./scripts/log.txt
echo "START: application-stop.sh" >> ./scripts/log.txt
echo $(date) >> ./scripts/log.txt

#Stop Horizon
sudo supervisorctl stop $SUPERVISOR_HORIZON >> ./scripts/log.txt

echo "END: application-stop.sh" >> ./scripts/log.txt
echo "=======================" >> ./scripts/log.txt
