#!/usr/bin/env bash

#Remove extra files
rm .editorconfig .env.ci .env.cypress* .env.dusk.* .git* appspec.yml.* .styleci.yml .shiftrc README.md phpunit.xml \
package.json tailwind.config.js webpack.mix.js yarn.lock cypress.json Dockerfile phpstan.neon sonar-project.properties

#Remove extra folders
rm -r test-reports tests cypress node_modules docker .bitbucket

ls -la
