#!/usr/bin/env bash
if [ "$DEPLOYMENT_GROUP_NAME" == "test" ]; then
    SUPERVISOR_HORIZON="myroi-test-horizon"
    SUPERVISOR_OCTANE="myroi-test-octane"
    cd /var/www/myroi/test
elif [ "$DEPLOYMENT_GROUP_NAME" == "staging" ]; then
    SUPERVISOR_HORIZON="myroi-staging-horizon"
    SUPERVISOR_OCTANE="myroi-staging-octane"
    cd /var/www/myroi/staging
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]; then
    SUPERVISOR_HORIZON="myroi-production-horizon"
    SUPERVISOR_OCTANE="myroi-production-octane"
    cd /var/www/myroi/production
fi

echo "=======================" >> ./scripts/log.txt
echo "START: application-start.sh" >> ./scripts/log.txt

if [[ -f "artisan" ]]; then
    #Pull app out of maintenance mode
    php artisan up >> ./scripts/log.txt
fi

#Start supervisor
sudo supervisorctl start $SUPERVISOR_HORIZON >> ./scripts/log.txt
sudo supervisorctl restart $SUPERVISOR_OCTANE >> ./scripts/log.txt

echo "END: application-start.sh" >> ./scripts/log.txt
echo "=======================" >> ./scripts/log.txt
