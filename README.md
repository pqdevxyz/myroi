# MyROI
A project for keeping track of investments for small to medium size portfolios.

## Authors
- [Paul Quine](https://bitbucket.org/pqdevxyz/)

## License
The project is licensed under [MIT](https://bitbucket.org/pqdevxyz/myroi/src/master/LICENSE.md)

## Acknowledgements
- [Laravel](https://laravel.com/)
- [Tailwind](http://tailwindcss.com/)
- [MJML](https://mjml.io/)
