<?php

namespace Database\Data;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class HistoricPriceSeedData
{
    private array $data = [
        'A.TEST' => [
            ['close_price' => 1.25, 'roi' => 0, 'date' => '2020-01-01'],
            ['close_price' => 1.35, 'roi' => 0, 'date' => '2020-07-01'],
        ],
        'B.TEST' => [
            ['close_price' => 0.75, 'roi' => 0, 'date' => '2020-02-01'],
            ['close_price' => 1.1, 'roi' => 0, 'date' => '2020-08-01'],
        ],
        'C.TEST' => [
            ['close_price' => 12.5, 'roi' => 0, 'date' => '2020-03-01'],
            ['close_price' => 20, 'roi' => 0, 'date' => '2020-09-01'],
        ],
    ];

    public function __invoke(string $ticker): Collection|null
    {
        foreach (array_keys($this->data) as $key) {
            if (Str::contains($ticker, $key)) {
                return collect($this->data[$key]);
            }
        }

        return null;
    }
}
