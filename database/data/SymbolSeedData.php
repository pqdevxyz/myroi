<?php

namespace Database\Data;

class SymbolSeedData
{
    private array $symbols = ['A', 'B', 'C'];
    private array $data = [
        'name' => 'Test Symbol ',
        'ticker' => '.TEST',
        'index' => 'FTSE',
    ];

    public function __invoke(int $number)
    {
        return (collect($this->symbols))->map(function ($symbol) use ($number) {
            return [
                'name' => $this->data['name'] . $number . $symbol,
                'ticker' => $number . $symbol . $this->data['ticker'],
                'index' => $this->data['index'],
                'closed' => $symbol == 'A' ? 1 : 0,
            ];
        });
    }
}
