<?php

namespace Database\Data;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class TransactionSeedData
{
    private array $data = [
        'A.TEST' => [
            ['quantity' => 10, 'buy_price' => 1, 'charges' => 0],
            ['quantity' => 10, 'buy_price' => 1.1, 'charges' => 0],
            ['quantity' => -20, 'buy_price' => 1.3, 'charges' => 0],
        ],
        'B.TEST' => [
            ['quantity' => 20, 'buy_price' => 0.5, 'charges' => 0],
            ['quantity' => 5, 'buy_price' => 0.99, 'charges' => 1],
        ],
        'C.TEST' => [
            ['quantity' => 2, 'buy_price' => 10, 'charges' => 5],
            ['quantity' => 1, 'buy_price' => 15, 'charges' => 2.5],
        ],
    ];

    public function __invoke(string $ticker): Collection|null
    {
        foreach (array_keys($this->data) as $key) {
            if (Str::contains($ticker, $key)) {
                return collect($this->data[$key]);
            }
        }

        return null;
    }
}
