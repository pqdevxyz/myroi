<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roi30AddRoiToHistoricPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('historic_prices')) {
            Schema::table('historic_prices', function (Blueprint $table) {
                $table->double('roi', 16, 6)->default(0)->after('close_price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('historic_prices', 'roi')) {
            Schema::table('historic_prices', function (Blueprint $table) {
                $table->dropColumn('roi');
            });
        }
    }
}
