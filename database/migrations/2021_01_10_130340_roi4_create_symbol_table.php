<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roi4CreateSymbolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('symbols')) {
            Schema::create('symbols', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('portfolio_id')->unsigned()->index();
                $table->string('name');
                $table->string('ticker');
                $table->string('index');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symbols');
    }
}
