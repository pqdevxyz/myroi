<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roi4CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('transactions')) {
            Schema::create('transactions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('symbol_id')->unsigned()->index();
                $table->double('quantity', 16, 4);
                $table->double('buy_price', 16, 6);
                $table->double('charges', 16, 6);
                $table->double('tax', 16, 6);
                $table->double('adjustment', 16, 6)->default(0);
                $table->date('buy_date');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
