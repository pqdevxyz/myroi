<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roi4CreateHistoricPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('historic_prices')) {
            Schema::create('historic_prices', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('symbol_id')->unsigned()->index();
                $table->double('close_price', 16, 6);
                $table->date('date');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historic_prices');
    }
}
