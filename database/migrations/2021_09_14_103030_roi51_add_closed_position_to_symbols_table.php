<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roi51AddClosedPositionToSymbolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('symbols')) {
            Schema::table('symbols', function (Blueprint $table) {
                $table->tinyInteger('is_closed')->default(0)->after('index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('symbols', 'roi')) {
            Schema::table('symbols', function (Blueprint $table) {
                $table->dropColumn('is_closed');
            });
        }
    }
}
