<?php

namespace Database\Factories;

use App\Models\Portfolio;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PortfolioFactory extends Factory
{
    /**
     * @var class-string<Portfolio>
     */
    protected $model = Portfolio::class;

    /**
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->randomElement(['ISA', 'Dealing', 'SIPP']);

        return [
            'name' => $name,
            'user_id' => $this->faker->randomNumber(1),
            'type' => Str::slug($name),
            'colour' => $this->faker->randomElement(['orange', 'blue', 'green', 'red', 'yellow']),
        ];
    }
}
