<?php

namespace Database\Factories;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * @var class-string<Transaction>
     */
    protected $model = Transaction::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'symbol_id' => $this->faker->randomNumber(1),
            'quantity' => $this->faker->randomFloat(2, 10, 500),
            'buy_price' => $this->faker->randomFloat(2, 10, 50),
            'charges' => $this->faker->randomElement([9.95, 1.50]),
            'tax' => 0,
            'adjustment' => 0,
            'buy_date' => Carbon::now()->subDays(rand(1, 180)),
        ];
    }
}
