<?php

namespace Database\Factories;

use App\Models\Symbol;
use Illuminate\Database\Eloquent\Factories\Factory;

class SymbolFactory extends Factory
{
    /**
     * @var class-string<Symbol>
     */
    protected $model = Symbol::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'portfolio_id' => $this->faker->randomNumber(1),
            'name' => $this->faker->sentence(3),
            'ticker' => $this->faker->regexify('[A-Z]{4}'),
            'index' => 'FTSE100',
            'is_closed' => 0,
        ];
    }
}
