<?php

namespace Database\Factories;

use App\Models\HistoricPrice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoricPriceFactory extends Factory
{
    /**
     * @var class-string<HistoricPrice>
     */
    protected $model = HistoricPrice::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'symbol_id' => $this->faker->randomNumber(1),
            'close_price' => $this->faker->randomFloat(5, 10, 50),
            'roi' => $this->faker->randomFloat(5, -50, 50),
            'date' => Carbon::now()->subDays(rand(1, 180)),
        ];
    }
}
