<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PasswordResetSeeder extends Seeder
{
    protected string $email;

    public function __construct(string $email = 'paul@quine.co.uk')
    {
        $this->email = $email;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('password_resets')->truncate();
        DB::table('password_resets')->insert(
            [
                'email' => $this->email,
                'token' => Str::random(32),
                'created_at' => Carbon::now(),
            ]
        );
    }
}
