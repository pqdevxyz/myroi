<?php

namespace Database\Seeders;

use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Database\Data\HistoricPriceSeedData;
use Database\Data\SymbolSeedData;
use Database\Data\TransactionSeedData;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PortfolioSeeder extends Seeder
{
    private User|null $user = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->user = User::where('email', 'paul@quine.co.uk')->first();
        if (is_null($this->user)) {
            /** @var User $newUser */
            $newUser = User::factory()->create([
                'first_name' => 'Paul',
                'last_name' => 'Q',
                'email' => 'paul@quine.co.uk',
            ]);
            $this->user = $newUser;
        }

        DB::table('portfolios')->truncate();
        DB::table('symbols')->truncate();
        DB::table('transactions')->truncate();
        DB::table('historic_prices')->truncate();

        Portfolio::factory()->count(2)->create([
            'user_id' => $this->user->id,
        ])->each(function ($portfolio) {
            /** @var Portfolio $portfolio */
            $symbolsData = (new SymbolSeedData())($portfolio->id);

            $symbolsData->each(function ($symbolData) use ($portfolio) {
                /** @var Portfolio $portfolio */
                /** @var Symbol $symbol */
                $symbol = Symbol::factory()->create([
                    'portfolio_id' => $portfolio->id,
                    'name' => $symbolData['name'],
                    'ticker' => $symbolData['ticker'],
                    'index' => $symbolData['index'],
                    'is_closed' => $symbolData['closed'],
                ]);

                $transactionsData = (new TransactionSeedData())($symbolData['ticker']);

                $transactionsData->each(function ($transactionData) use ($symbol) {
                    Transaction::factory()->create([
                        'symbol_id' => $symbol->id,
                        'quantity' => $transactionData['quantity'],
                        'buy_price' => $transactionData['buy_price'],
                        'charges' => $transactionData['charges'],
                    ]);
                });

                $historicPricesData = (new HistoricPriceSeedData())($symbolData['ticker']);

                $historicPricesData->each(function ($historicPriceData) use ($symbol) {
                    HistoricPrice::factory()->create([
                        'symbol_id' => $symbol->id,
                        'close_price' => $historicPriceData['close_price'],
                        'roi' => $historicPriceData['roi'],
                        'date' => Carbon::parse($historicPriceData['date']),
                    ]);
                });
            });
        });
    }
}
