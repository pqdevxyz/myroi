# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project using to [Calendar Versioning](https://calver.org/overview.html) with the following format `YY.MM.PATCH`

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

<!-- Remember to update sonar-project.properties with the correct version number -->
## 22.08.1
### Security
- Updated dependencies

## 22.06.1
### Removed
- Enlightn package
### Security
- Updated dependencies

## 22.05.2
### Changed
- Updated packages
- Deploy scripts

## 22.05.1
### Changed
- Improvements from infection testing

## 22.03.1
### Added
- Added infection testing package
### Changed
- Upgraded Font Awesome
- Changes to docker scripts
- Fixed tests & made them work in parallel
- Change to PQ Cache method

## 22.02.3
### Added
- Laravel octane for better performance

## 22.02.2
### Changed
- Update to Laravel 9

## 22.02.1
### Changed
- Update to PHP 8.1

## 22.01.4
### Added
- Redis DBs to example env file

## 22.01.3
### Changed
- Updated packages
- Docker changes

### Removed
- `owen-it/laravel-auditing` package

### Fixed
- Bug with PurpleModel

## 22.01.2
### Changed
- Sentry transaction sample rate

## 22.01.1
### Changed
- Sentry transaction sample rate
- Schedule health check max time to 5 mins

## 21.12.13
### Added
- Dark mode
- Laravel health check

## 21.12.12
### Changed
- Moved ECR region to us-east-1

## 21.12.11
### Added
- Added route caching to deployment script
- HTTP2 to nginx config
### Changed
- To ensure text remains visible during webfont loading

## 21.12.10
### Added
- Cache headers to js, css, png, jpg, gif, svg, ico assets
### Changed
- Moved CSP header into laravel middleware instead of nginx config
### Fixed
- Moved settings out of php.ini into docker containers to fix issue with pcov

## 21.12.9
### Added
- Running enlightn after each deployment
### Changed
- Updated horizon config
- Update nginx config (added CSP header & gzip compression)
- Split css into 2 separate files

## 21.12.8
### Added
- Installed & setup enlightn

## 21.12.7
### Changed
- Deployment script

## 21.12.6
### Changed
- Improved mobile responsiveness

## 21.12.5
### Added
- Added snyk to CI & setup snyk SAST for code scanning
### Changed
- Updated to tailwind v3

## 21.12.4
### Changed
- Pipelines to use smaller containers for some steps
- Clean up pipelines deployment steps

## 21.12.3
### Added
- StyleCI to process
### Changed
- Reconnected cypress dashboard to pipelines

## 21.12.2
### Changed
- Cleaned phpunit output in pipelines to get SonarCloud working correctly

## 21.12.1
### Added
- Docker container for local development
### Changed
- Updates to pipelines to enable better dev experience

## 21.11.2
### Security
- Updated composer & yarn packages

## 21.11.1
### Security
- Updated composer & yarn packages

## 21.09.1
### Added
- Functionally to "close" a symbol, which will then display the profit from that symbol

### Fixed
- Issue with wrong total costs displayed when total symbol quantity is zero e.g. postition has been closed

## 21.07.1
### Added
- Setup & installed larastan

### Changed
- Updated composer & yarn packages

## 21.05.5
### Changed
- Updated logos & favicon
- Updated reset password email
- Updated dependencies
