<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetupInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roi:init {--U|onlyUser}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the MyROI project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('clear-compiled');
        $this->call('migrate:fresh');
        if ($this->option('onlyUser')) {
            $this->call('db:seed', ['class' => 'UserSeeder']);
        } else {
            $this->call('db:seed');
        }
        $this->call('package:discover');
    }
}
