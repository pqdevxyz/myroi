<?php

namespace App\Models;

use App\Helpers\PQCache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $type
 * @property string $colour
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 */
class Portfolio extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $appends = [
        'total_cost',
        'total_quantity',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function symbols(): HasMany
    {
        return $this->hasMany(Symbol::class, 'portfolio_id');
    }

    /**
     * @return float
     */
    protected function getTotalCostAttribute(): float
    {
        return PQCache::retrieveFromCache('portfolios', $this->id, 'total_cost', function () {
            $total = 0;
            foreach ($this->symbols as $symbol) {
                $total += $symbol->total_cost;
            }

            return $total;
        });
    }

    /**
     * @return float
     */
    protected function getTotalQuantityAttribute(): float
    {
        return PQCache::retrieveFromCache('portfolios', $this->id, 'total_quantity', function () {
            return DB::table((new Transaction())->getTable())->selectRaw('SUM(quantity) as total_quantity')->whereIn('symbol_id', $this->symbols()->pluck('id')->toArray())->whereNull('deleted_at')->first()->total_quantity ?? 0;
        });
    }
}
