<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $symbol_id
 * @property float $quantity
 * @property float $buy_price
 * @property float $charges
 * @property float $tax
 * @property float $adjustment
 * @property string|Carbon $buy_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 */
class Transaction extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * @var string[]
     */
    protected $dates = [
        'buy_date',
    ];

    /**
     * Get the symbol the Transaction belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function symbol()
    {
        return $this->belongsTo(Symbol::class, 'symbol_id');
    }

    /**
     * Get the total cost.
     *
     * @return float
     */
    public function getTotalCostAttribute()
    {
        return ($this->quantity * $this->buy_price) + $this->charges + $this->tax + $this->adjustment;
    }

    /**
     * Get the consideration cost.
     *
     * @return float
     */
    public function getConsiderationCostAttribute()
    {
        return $this->quantity * $this->buy_price;
    }
}
