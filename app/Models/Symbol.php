<?php

namespace App\Models;

use App\Helpers\PQCache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $portfolio_id
 * @property string $name
 * @property string $ticker
 * @property string $index
 * @property bool $is_closed
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 */
class Symbol extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * Get the portfolio the Symbol belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function portfolio()
    {
        return $this->belongsTo(Portfolio::class, 'portfolio_id');
    }

    /**
     * Get the transactions the Symbol has.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'symbol_id')->orderBy('buy_date', 'desc');
    }

    /**
     * Get the historic prices the Symbol has.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function historicPrices()
    {
        return $this->hasMany(HistoricPrice::class, 'symbol_id')->orderBy('date', 'desc');
    }

    /**
     * Get the total amount of money spent for this symbol (including charges & tax).
     *
     * @return float
     */
    public function getTotalCostAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'total_cost', function () {
            if ($this->total_quantity == 0) {
                return 0;
            }

            return DB::table((new Transaction())->getTable())->selectRaw('SUM((quantity * buy_price) + charges + tax + adjustment) as total_cost')->where('symbol_id', $this->id)->whereNull('deleted_at')->first()->total_cost ?? 0;
        });
    }

    /**
     * Get the total quantity held.
     *
     * @return float
     */
    public function getTotalQuantityAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'total_quantity', function () {
            return DB::table((new Transaction())->getTable())->selectRaw('SUM(quantity) as total_quantity')->where('symbol_id', $this->id)->whereNull('deleted_at')->first()->total_quantity ?? 0;
        });
    }

    /**
     * Get the total amount of money spent for this symbol (excluding charges & tax).
     *
     * @return int|mixed
     */
    public function getTotalConsiderationAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'total_consideration', function () {
            return DB::table((new Transaction())->getTable())->selectRaw('SUM(quantity * buy_price) as total_consideration')->where('symbol_id', $this->id)->whereNull('deleted_at')->first()->total_consideration ?? 0;
        });
    }

    /**
     * Get the latest historic price for the symbol.
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getLatestPriceAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'latest_price', function () {
            return $this->historicPrices()->latest('date')->first();
        });
    }

    /**
     * Get the next date we need a historic price for the symbol.
     *
     * @return Carbon|null
     */
    public function getNextPriceDateAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'next_price_date', function () {
            return $this->latest_price?->date->addMonths(6);
        });
    }

    /**
     * Get the total sale amount based on the latest pricing.
     *
     * @return float|int
     */
    public function getTotalSaleAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'total_sale', function () {
            return isset($this->latest_price) ? $this->latest_price->close_price * $this->total_quantity : 0;
        });
    }

    /**
     * Get the gross roi of a symbol.
     *
     * @return mixed
     */
    public function getGrossRoiAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'gross_roi', function () {
            return $this->total_sale - $this->total_consideration;
        });
    }

    /**
     * Get the gross roi percentage of a symbol.
     *
     * @return float|int
     */
    public function getGrossRoiPercentageAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'gross_roi_percentage', function () {
            return $this->total_consideration != 0 ? ($this->gross_roi / $this->total_consideration) * 100 : 0;
        });
    }

    /**
     * Get the net roi of a symbol.
     *
     * @return mixed
     */
    public function getNetRoiAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'net_roi', function () {
            return $this->total_sale - $this->total_cost;
        });
    }

    /**
     * Get the net roi percentage of a symbol.
     *
     * @return float|int
     */
    public function getNetRoiPercentageAttribute()
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'net_roi_percentage', function () {
            return $this->total_cost != 0 ? ($this->net_roi / $this->total_cost) * 100 : 0;
        });
    }

    /**
     * Get the symbols percentage of total cost compared to the overall portfolio.
     *
     * @return float|int
     */
    public function getPercentageOfPortfolioAttribute()
    {
        return $this->portfolio->total_cost != 0 ? ($this->total_cost / $this->portfolio->total_cost) * 100 : 0;
    }

    /**
     * Get the total profit for this symbol.
     *
     * @return float|int
     */
    public function getTotalProfitAttribute(): float|int
    {
        return PQCache::retrieveFromCache('symbols', $this->id, 'total_profit', function () {
            if (! $this->is_closed) {
                return 0;
            }

            return DB::table((new Transaction())->getTable())->selectRaw('SUM((quantity * buy_price) + charges + tax + adjustment) * -1 as total_profit')->where('symbol_id', $this->id)->whereNull('deleted_at')->first()->total_profit ?? 0;
        });
    }
}
