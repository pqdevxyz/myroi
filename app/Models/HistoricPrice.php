<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $symbol_id
 * @property float $close_price
 * @property float $roi
 * @property string|Carbon $date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class HistoricPrice extends Model
{
    use HasFactory;

    protected $table = 'historic_prices';

    /**
     * @var string[]
     */
    protected $dates = [
        'date',
    ];

    /**
     * @return BelongsTo
     */
    public function symbol(): BelongsTo
    {
        return $this->belongsTo(Symbol::class, 'symbol_id');
    }

    /**
     * @return float|int
     */
    protected function getRoiPercentageAttribute(): float|int
    {
        return ($this->roi / $this->close_price) * 100;
    }
}
