<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class PQCache
{
    /**
     * Calculate the correct key based on the input vars for consistent caching.
     *
     * @param  string  $type
     * @param  int  $id
     * @return string
     */
    public static function getCacheKey(string $type, int $id): string
    {
        //Cache Key: {User Id}:{Model Type}.{Model Id}
        return Auth::id() . ':' . Str::slug($type) . '.' . $id;
    }

    /**
     * Calculate the TTL for a cache item based on the input vars.
     *
     * @param  string  $type
     * @return int
     */
    public static function getCacheTTL(string $type): int
    {
        return match ($type) {
            'portfolios', 'symbols' => 60 * 60,
            default => 1 * 60,
        };
    }

    /**
     * Clear all the cache keys for the model type provided.
     *
     * @param  string  $type
     * @param  int  $id
     * @return bool
     */
    public static function removeFromCache(string $type, int $id): bool
    {
        return Cache::forget(self::getCacheKey($type, $id));
    }

    /**
     * Retrieves an item from the cache or sets it using the callback if it doesn't already exist.
     *
     * @param  string  $type
     * @param  int  $id
     * @param  string  $variable
     * @param  callable|null  $callback
     * @return mixed
     */
    public static function retrieveFromCache(string $type, int $id, string $variable, callable $callback = null)
    {
        $cache = Cache::get(self::getCacheKey($type, $id));

        if (isset($cache, $cache[$variable])) {
            return $cache[$variable];
        }

        $cache = array_merge($cache ?? [], [$variable => $callback()]);
        Cache::put(self::getCacheKey($type, $id), $cache, self::getCacheTTL($type));
        return $cache[$variable];
    }
}
