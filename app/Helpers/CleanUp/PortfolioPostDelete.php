<?php

namespace App\Helpers\CleanUp;

use App\Helpers\PQCache;
use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;

class PortfolioPostDelete
{
    /**
     * @param  int  $portfolioId
     * @return void
     */
    public static function run(int $portfolioId): void
    {
        /** @var Portfolio */
        $portfolio = Portfolio::withTrashed()->find($portfolioId);

        $symbols = Symbol::where('portfolio_id', $portfolio->id)->get();

        foreach ($symbols as $symbol) {
            $transactions = Transaction::where('symbol_id', $symbol->id)->get();
            $historicPrices = HistoricPrice::where('symbol_id', $symbol->id)->get();

            foreach ($transactions as $transaction) {
                $transaction->deleted_at = $portfolio->deleted_at;
                $transaction->save();
            }

            foreach ($historicPrices as $historicPrice) {
                $historicPrice->delete();
            }

            PQCache::removeFromCache('symbols', $symbol->id);
            $symbol->deleted_at = $portfolio->deleted_at;
            $symbol->save();
        }
    }
}
