<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Sentry as SentrySDK;

class AddSentryInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (app()->bound('sentry')) {
            //Add User Context
            $this->addUserContext($request);

            $agent = new Agent();

            //Add Browser Context
            $this->addBrowserContext($agent);

            //Add OS Context
            $this->addOperatingSystemContext($agent);
        }

        return $next($request);
    }

    /**
     * Add User Context to sentry.
     *
     * @param  Request  $request
     */
    private function addUserContext(Request $request)
    {
        if (auth()->check()) {
            SentrySDK\configureScope(function (SentrySDK\State\Scope $scope) use ($request) {
                $scope->setUser([
                    'id' => $request->user()->id,
                    'name' => $request->user()->full_name,
                    'email' => $request->user()->email,
                    'ip' => $request->ip(),
                ]);
            });
        } else {
            SentrySDK\configureScope(function (SentrySDK\State\Scope $scope) use ($request) {
                $scope->setUser([
                    'id' => null,
                    'ip' => $request->ip(),
                ]);
            });
        }
    }

    /**
     * Add Browser Context to sentry.
     *
     * @param  Agent  $agent
     */
    private function addBrowserContext(Agent $agent)
    {
        $version = $agent->version($agent->browser());
        SentrySDK\configureScope(function (SentrySDK\State\Scope $scope) use ($agent, $version) {
            $scope->setExtras([
                'browser.name' => $agent->browser(),
                'browser.version' => ! $version ? $version : 'Unknown Version',
            ]);
        });
    }

    /**
     * Add OS Context to sentry.
     *
     * @param  Agent  $agent
     */
    private function addOperatingSystemContext(Agent $agent)
    {
        $version = $agent->version($agent->platform());
        SentrySDK\configureScope(function (SentrySDK\State\Scope $scope) use ($agent, $version) {
            $scope->setExtras([
                'client_os.name' => $agent->platform(),
                'client_os.version' => ! $version ? $version : 'Unknown Version',
            ]);
        });
    }
}
