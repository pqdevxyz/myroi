<?php

namespace App\Http\Controllers;

use App\Helpers\PQCache;
use App\Jobs\PortfolioPostDeleteJob;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PortfolioController extends Controller
{
    /* ==============
     * Portfolio Pages
     * ==============
     */

    /**
     * Get the Portfolio Overview page.
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function viewPortfolio(Request $request)
    {
        //All Portfolios
        $portfolios = Portfolio::where('user_id', Auth::id())->get();

        //Account Totals (Cost & Quantity)
        $totals = [
            'cost' => 0,
            'quantity' => 0,
        ];

        foreach ($portfolios as $portfolio) {
            $totals['cost'] += $portfolio->total_cost;
            $totals['quantity'] += $portfolio->total_quantity;
        }
        $totals['cost'] = number_format($totals['cost'], 2);
        $totals['quantity'] = number_format($totals['quantity'], 2);

        //All Symbols
        $symbols = Symbol::with(['portfolio', 'historicPrices'])->where('is_closed', 0)->whereIn('portfolio_id', $portfolios->pluck('id')->toArray())->get();

        //->Next 3 by next date
        $next3 = $symbols->whereNotNull('next_price_date')->sortBy('next_price_date')->take(3);

        //->All grouped by portfolio
        $symbols = $symbols->groupBy('portfolio_id');

        //Closed Symbols
        $closed = Symbol::with(['portfolio', 'historicPrices'])->where('is_closed', 1)->whereIn('portfolio_id', $portfolios->pluck('id')->toArray())->get();

        return view('dashboard.portfolio.overview', ['portfolios' => $portfolios, 'totals' => $totals, 'symbols' => $symbols, 'next3' => $next3, 'closed' => $closed]);
    }

    /**
     * Get the Create Portfolio page.
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getCreatePortfolio(Request $request)
    {
        return view('dashboard.portfolio.create', ['portfolio' => new Portfolio()]);
    }

    /**
     * Create the portfolio.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createPortfolio(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'colour' => 'required',
        ]);

        //Create Portfolio
        /** @var Portfolio */
        $portfolio = new Portfolio();
        $portfolio->name = $request->input('name');
        $portfolio->type = $request->input('type');
        $portfolio->colour = $request->input('colour');
        $portfolio->user_id = Auth::id();

        if ($portfolio->save()) {
            return redirect()->route('portfolio.overview')->with('success', 'Portfolio created');
        } else {
            return redirect()->back()->with('error', 'Could not save portfolio');
        }
    }

    /**
     * Get the Manage Portfolio page.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getManagePortfolio(Request $request, int $id)
    {
        $portfolio = Portfolio::with(['symbols'])->find($id);
        if (is_null($portfolio)) {
            abort(404);
        }

        return view('dashboard.portfolio.manage', ['portfolio' => $portfolio]);
    }

    /**
     * Update the portfolio.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function updatePortfolio(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'colour' => 'required',
        ]);

        //Get Portfolio
        /** @var Portfolio|null $portfolio */
        $portfolio = Portfolio::find($id);
        if (is_null($portfolio)) {
            return redirect()->back()->with('error', 'Portfolio Not Found');
        }

        $portfolio->name = $request->input('name');
        $portfolio->type = $request->input('type');
        $portfolio->colour = $request->input('colour');

        if ($portfolio->save()) {
            return redirect()->route('portfolio.manage', ['id' => $portfolio->id])->with('success', 'Portfolio updated');
        } else {
            return redirect()->back()->with('error', 'Could not save portfolio');
        }
    }

    /**
     * Delete the portfolio.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function deletePortfolio(Request $request, int $id)
    {
        //Get Portfolio
        /** @var Portfolio|null $portfolio */
        $portfolio = Portfolio::find($id);
        if (is_null($portfolio)) {
            return redirect()->back()->with('error', 'Portfolio Not Found');
        }

        if ($portfolio->delete()) {
            PortfolioPostDeleteJob::dispatch($portfolio->id)->onQueue('long');
            PQCache::removeFromCache('portfolios', $portfolio->id);

            return redirect()->route('portfolio.overview')->with('success', 'Portfolio deleted');
        } else {
            return redirect()->back()->with('error', 'Could not delete portfolio');
        }
    }
}
