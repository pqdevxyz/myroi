<?php

namespace App\Http\Controllers;

use App\Helpers\PQCache;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SymbolController extends Controller
{
    /* ==============
     * Symbol Pages
     * ==============
     */

    /**
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function viewOpenSymbol(Request $request, int $id)
    {
        $symbol = Symbol::with(['transactions', 'historicPrices', 'portfolio'])->find($id);
        if (is_null($symbol)) {
            abort(404);
        }

        return view('dashboard.symbol.open', ['symbol' => $symbol]);
    }

    /**
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function viewClosedSymbol(Request $request, int $id)
    {
        $symbol = Symbol::with(['transactions', 'historicPrices', 'portfolio'])->find($id);
        if (is_null($symbol)) {
            abort(404);
        }

        return view('dashboard.symbol.closed', ['symbol' => $symbol]);
    }

    /**
     * Create the symbol.
     *
     * @param  Request  $request
     * @param  int  $portfolioId
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createSymbol(Request $request, int $portfolioId)
    {
        $request->validate([
            'name' => 'required',
            'ticker' => 'required',
            'index' => 'required',
        ]);

        //Create Symbol
        /** @var Symbol */
        $symbol = new Symbol();
        $symbol->name = $request->input('name');
        $symbol->ticker = $request->input('ticker');
        $symbol->index = $request->input('index');
        $symbol->portfolio_id = $portfolioId;

        if ($symbol->save()) {
            return redirect()->route('portfolio.manage', ['id' => $portfolioId])->with('success', 'Symbol created');
        } else {
            return redirect()->back()->with('error', 'Could not save symbol');
        }
    }

    /**
     * Edit the symbol.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function editSymbol(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'ticker' => 'required',
            'index' => 'required',
        ]);

        //Get Symbol
        /** @var Symbol|null $symbol */
        $symbol = Symbol::find($id);
        if (is_null($symbol)) {
            return redirect()->back()->with('error', 'Symbol Not Found');
        }

        $symbol->name = $request->input('name');
        $symbol->ticker = $request->input('ticker');
        $symbol->index = $request->input('index');

        if ($symbol->save()) {
            return redirect()->route('portfolio.manage', ['id' => $symbol->portfolio_id])->with('success', 'Symbol updated');
        } else {
            return redirect()->back()->with('error', 'Could not save symbol');
        }
    }

    /**
     * Remove the symbol.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function removeSymbol(Request $request, int $id)
    {
        //Get Symbol
        /** @var Symbol|null $symbol */
        $symbol = Symbol::find($id);
        if (is_null($symbol)) {
            return redirect()->back()->with('error', 'Symbol Not Found');
        }

        if ($symbol->delete()) {
            PQCache::removeFromCache('portfolios', $symbol->portfolio_id);
            PQCache::removeFromCache('symbols', $symbol->id);

            return redirect()->route('portfolio.manage', ['id' => $symbol->portfolio_id])->with('success', 'Symbol deleted');
        } else {
            return redirect()->back()->with('error', 'Could not delete symbol');
        }
    }

    protected function togglePosition(Request $request, int $id)
    {
        //Get Symbol
        /** @var Symbol|null $symbol */
        $symbol = Symbol::find($id);
        if (is_null($symbol)) {
            return redirect()->back()->with('error', 'Symbol Not Found');
        }

        $symbol->is_closed = ! $symbol->is_closed;

        if ($symbol->save()) {
            return redirect()->route('portfolio.manage', ['id' => $symbol->portfolio_id])->with('success', 'Symbol updated');
        } else {
            return redirect()->back()->with('error', 'Could not save symbol');
        }
    }
}
