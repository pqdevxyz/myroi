<?php

namespace App\Http\Controllers;

use App\Helpers\PQCache;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /* ==============
     * Transaction Pages
     * ==============
     */

    /**
     * Get the Create Transaction page.
     *
     * @param  Request  $request
     * @param  int  $symbolId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getCreateTransaction(Request $request, int $symbolId)
    {
        $symbol = Symbol::with(['transactions'])->find($symbolId);
        if (is_null($symbol)) {
            abort(404);
        }

        return view('dashboard.transaction.create', ['symbol' => $symbol, 'transaction' => new Transaction()]);
    }

    /**
     * Create the transaction.
     *
     * @param  Request  $request
     * @param  int  $symbolId
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createTransaction(Request $request, int $symbolId)
    {
        $request->validate([
            'quantity' => 'required',
            'buy_price' => 'required',
            'charges' => 'required',
            'tax' => 'required',
            'adjustment' => 'required',
            'buy_date' => 'required',
        ]);

        //Create Transaction
        /** @var Transaction */
        $transaction = new Transaction();
        $transaction->quantity = $request->input('quantity');
        $transaction->buy_price = $request->input('buy_price');
        $transaction->charges = $request->input('charges');
        $transaction->tax = $request->input('tax');
        $transaction->adjustment = $request->input('adjustment');
        $transaction->buy_date = Carbon::parse($request->input('buy_date'))->format('Y-m-d');
        $transaction->symbol_id = $symbolId;

        if ($transaction->save()) {
            /** @var Symbol|null $symbol */
            $symbol = Symbol::find($symbolId);
            if (! is_null($symbol)) {
                PQCache::removeFromCache('portfolios', $symbol->portfolio_id);
                PQCache::removeFromCache('symbols', $symbolId);
            }

            return redirect()->route('symbol.view.open', ['id' => $symbolId])->with('success', 'Transaction created');
        } else {
            return redirect()->back()->with('error', 'Could not save transaction');
        }
    }

    /**
     * Get the Edit Transaction page.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getEditTransaction(Request $request, int $id)
    {
        $transaction = Transaction::find($id);
        if (is_null($transaction)) {
            abort(404);
        }

        return view('dashboard.transaction.edit', ['transaction' => $transaction]);
    }

    /**
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function viewTransaction(Request $request, int $id)
    {
        $transaction = Transaction::find($id);
        if (is_null($transaction)) {
            abort(404);
        }

        return view('dashboard.transaction.view', ['transaction' => $transaction]);
    }

    /**
     * Edit the transaction.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function editTransaction(Request $request, int $id)
    {
        $request->validate([
            'quantity' => 'required',
            'buy_price' => 'required',
            'charges' => 'required',
            'tax' => 'required',
            'adjustment' => 'required',
            'buy_date' => 'required',
        ]);

        //Get Transaction
        /** @var Transaction|null $transaction */
        $transaction = Transaction::find($id);
        if (is_null($transaction)) {
            return redirect()->back()->with('error', 'Transaction Not Found');
        }

        $transaction->quantity = $request->input('quantity');
        $transaction->buy_price = $request->input('buy_price');
        $transaction->charges = $request->input('charges');
        $transaction->tax = $request->input('tax');
        $transaction->adjustment = $request->input('adjustment');
        $transaction->buy_date = Carbon::parse($request->input('buy_date'))->format('Y-m-d');

        if ($transaction->save()) {
            /** @var Symbol|null $symbol */
            $symbol = Symbol::find($transaction->symbol_id);
            if (! is_null($symbol)) {
                PQCache::removeFromCache('portfolios', $symbol->portfolio_id);
                PQCache::removeFromCache('symbols', $transaction->symbol_id);
            }

            return redirect()->route('symbol.view.open', ['id' => $transaction->symbol_id])->with('success', 'Transaction updated');
        } else {
            return redirect()->back()->with('error', 'Could not save transaction');
        }
    }

    /**
     * Remove the transaction.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function removeTransaction(Request $request, int $id)
    {
        //Get Transaction
        /** @var Transaction|null $transaction */
        $transaction = Transaction::find($id);
        if (is_null($transaction)) {
            return redirect()->back()->with('error', 'Transaction Not Found');
        }

        if ($transaction->delete()) {
            /** @var Symbol|null $symbol */
            $symbol = Symbol::find($transaction->symbol_id);
            if (! is_null($symbol)) {
                PQCache::removeFromCache('portfolios', $symbol->portfolio_id);
                PQCache::removeFromCache('symbols', $transaction->symbol_id);
            }

            return redirect()->route('symbol.view.open', ['id' => $transaction->symbol_id])->with('success', 'Transaction deleted');
        } else {
            return redirect()->back()->with('error', 'Could not delete transaction');
        }
    }
}
