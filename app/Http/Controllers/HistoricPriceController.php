<?php

namespace App\Http\Controllers;

use App\Helpers\PQCache;
use App\Jobs\CalculateHistoricPriceROIJob;
use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoricPriceController extends Controller
{
    /* ==============
     * Historic Price Pages
     * ==============
     */

    /**
     * Get the Create Historic Price page.
     *
     * @param  Request  $request
     * @param  int  $symbolId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getCreateHistoricPrice(Request $request, int $symbolId)
    {
        $symbol = Symbol::find($symbolId);
        if (is_null($symbol)) {
            abort(404);
        }

        return view('dashboard.historic-price.create', ['symbol' => $symbol, 'historicPrice' => new HistoricPrice()]);
    }

    /**
     * Create the historic price.
     *
     * @param  Request  $request
     * @param  int  $symbolId
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createHistoricPrice(Request $request, int $symbolId)
    {
        $request->validate([
            'close_price' => 'required',
            'date' => 'required',
        ]);

        //Create Historic Price
        /** @var HistoricPrice */
        $historicPrice = new HistoricPrice();
        $historicPrice->close_price = $request->input('close_price');
        $historicPrice->date = Carbon::parse($request->input('date'))->format('Y-m-d');
        $historicPrice->symbol_id = $symbolId;

        if ($historicPrice->save()) {
            CalculateHistoricPriceROIJob::dispatch($symbolId)->onQueue('long');
            PQCache::removeFromCache('symbols', $symbolId);

            return redirect()->route('symbol.view.open', ['id' => $symbolId])->with('success', 'Historic Price created');
        } else {
            return redirect()->back()->with('error', 'Could not save historic price');
        }
    }

    /**
     * Get the Edit Historic Price page.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getEditHistoricPrice(Request $request, int $id)
    {
        $historicPrice = HistoricPrice::find($id);
        if (is_null($historicPrice)) {
            abort(404);
        }

        return view('dashboard.historic-price.edit', ['historicPrice' => $historicPrice]);
    }

    /**
     * Edit the historic price.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function editHistoricPrice(Request $request, int $id)
    {
        $request->validate([
            'close_price' => 'required',
            'date' => 'required',
        ]);

        //Get Historic Price
        /** @var HistoricPrice|null $historicPrice */
        $historicPrice = HistoricPrice::find($id);
        if (is_null($historicPrice)) {
            return redirect()->back()->with('error', 'Historic Price Not Found');
        }

        $historicPrice->close_price = $request->input('close_price');
        $historicPrice->date = Carbon::parse($request->input('date'))->format('Y-m-d');

        if ($historicPrice->save()) {
            CalculateHistoricPriceROIJob::dispatch($historicPrice->symbol_id)->onQueue('long');
            PQCache::removeFromCache('symbols', $historicPrice->symbol_id);

            return redirect()->route('symbol.view.open', ['id' => $historicPrice->symbol_id])->with('success', 'Historic Price updated');
        } else {
            return redirect()->back()->with('error', 'Could not save historic price');
        }
    }

    /**
     * Remove the historic price.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function removeHistoricPrice(Request $request, int $id)
    {
        //Get Historic Price
        /** @var HistoricPrice|null $historicPrice */
        $historicPrice = HistoricPrice::find($id);
        if (is_null($historicPrice)) {
            return redirect()->back()->with('error', 'Historic Price Not Found');
        }

        if ($historicPrice->delete()) {
            CalculateHistoricPriceROIJob::dispatch($historicPrice->symbol_id)->onQueue('long');
            PQCache::removeFromCache('symbols', $historicPrice->symbol_id);

            return redirect()->route('symbol.view.open', ['id' => $historicPrice->symbol_id])->with('success', 'Historic Price deleted');
        } else {
            return redirect()->back()->with('error', 'Could not delete historic price');
        }
    }
}
