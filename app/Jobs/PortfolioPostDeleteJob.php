<?php

namespace App\Jobs;

use App\Helpers\CleanUp\PortfolioPostDelete;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PortfolioPostDeleteJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public int $portfolioId;

    /**
     * Create a new job instance.
     *
     * @param  int  $portfolioId
     */
    public function __construct(int $portfolioId)
    {
        $this->portfolioId = $portfolioId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PortfolioPostDelete::run($this->portfolioId);
    }
}
