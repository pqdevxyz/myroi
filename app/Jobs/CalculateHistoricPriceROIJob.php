<?php

namespace App\Jobs;

use App\Helpers\PQCache;
use App\Models\HistoricPrice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CalculateHistoricPriceROIJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public int $symbolId;

    /**
     * Create a new job instance.
     *
     * @param  int  $symbolId
     */
    public function __construct(int $symbolId)
    {
        $this->symbolId = $symbolId;
    }

    /**
     * Set the missing ROIs for Historic Prices.
     *
     * @return void
     */
    public function handle()
    {
        //Updated all missing ROI values for this symbol
        $prices = HistoricPrice::where('symbol_id', $this->symbolId)->orderBy('date', 'desc')->get();
        foreach ($prices as $key => $price) {
            if (isset($prices[$key + 1])) {
                $price->roi = $price->close_price - $prices[$key + 1]->close_price;
                $price->save();
            }
        }
        PQCache::removeFromCache('symbols', $this->symbolId);
    }
}
