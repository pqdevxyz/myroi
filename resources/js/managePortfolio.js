document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById('portfolio-add-symbol-button').addEventListener('click', showNewAccordion);

    let accordions = document.getElementsByClassName('symbol-accordion');

    for (let accordion of accordions) {
        accordion.addEventListener('click', toggleAccordion);
    }
});

function toggleAccordion(event) {
    let target = event.target;
    let max = 3;

    let key = checkForAccordionKey(target);

    if (key === null) {
        do {
            target = target.parentNode;
            key = checkForAccordionKey(target);
            max--;
        } while (max > 0 && key === null);
    }

    let forms = document.getElementsByClassName('symbol-accordion-form');

    for (let j = 0; j < forms.length; j++) {
        if (forms[j].dataset.form === key) {
            forms[j].classList.toggle('hidden');
        } else {
            forms[j].classList.add('hidden');
        }
    }

    let icons = document.getElementsByClassName('symbol-accordion-icon');

    for (let k = 0; k < icons.length; k++) {
        if (icons[k].dataset.icon === key) {
            if (forms[k].classList.contains('hidden')) {
                icons[k].classList.remove('fa-chevron-up');
                icons[k].classList.add('fa-chevron-down');
            } else {
                icons[k].classList.remove('fa-chevron-down');
                icons[k].classList.add('fa-chevron-up');
            }
        } else {
            icons[k].classList.remove('fa-chevron-up');
            icons[k].classList.add('fa-chevron-down');
        }
    }
}

function checkForAccordionKey(target) {
    if (target.classList.contains('symbol-accordion')) {
        return target.dataset.selector;
    }
    return null;
}

function showNewAccordion(event) {
    document.getElementById('portfolio-add-symbol-button').classList.add('hidden');
    document.getElementById('portfolio-symbol-new').classList.remove('hidden');
}
