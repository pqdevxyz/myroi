<div class="h-full">
    <a href="{{ $link }}">
        <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto h-full">
            <div class="p-4 grid grid-cols-3 h-full">
                <div class="col-start-1 col-end-3">
                    <p class="text-md font-medium uppercase tracking-wide mb-1 text-neutral-600 dark:text-neutral-300 small-card-title">{{ $title }}</p>
                </div>
                <div class="col-start-3 col-end-4 text-right">
                    <p class="inline-block p-2 rounded-full bg-portfolio-{{ $colour }}" data-belongs-to="{{ $belongsTo }}"></p>
                </div>
                <div class="col-start-1 col-end-3 flex items-end mt-2">
                    <p class="text-2xl text-neutral-600 dark:text-neutral-300 small-card-price">&pound;{{ $price }}</p>
                </div>
                <div class="col-start-3 col-end-4 flex items-end justify-end mt-2">
                    <p class="text-md font-light italic uppercase tracking-wide text-neutral-500 dark:text-neutral-400 small-card-subtitle">{{ $subtitle }}</p>
                </div>
            </div>
        </div>
    </a>
</div>
