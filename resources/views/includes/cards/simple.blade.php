<div>
    <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto">
        <div class="grid col-template-simple-card">
            <div class="bg-{{ $colour }} rounded-l-lg"></div>
            <div class="p-4 grid grid-rows-2">
                <div class="flex justify-center items-center">
                    <p class="text-md font-medium uppercase tracking-wide text-neutral-500 dark:text-neutral-400 simple-card-title">{{ $title }}</p>
                </div>
                <div class="flex justify-center items-center">
                    <p class="text-2xl font-semibold text-neutral-700 dark:text-neutral-300 simple-card-subtitle">{{ $subtitle }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
