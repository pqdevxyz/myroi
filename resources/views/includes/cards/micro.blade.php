<div class="h-full">
    <a href="{{ $link }}">
        <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto h-full">
            <div class="grid h-full col-template-micro-card">
                <div class="bg-portfolio-{{ $colour }} rounded-l-lg"></div>
                <div class="p-4 grid grid-cols-2">
                    <div class="flex justify-start items-center">
                        <p class="text-md font-medium uppercase tracking-wide text-neutral-600 dark:text-neutral-300 mirco-card-title">{{ $title }}</p>
                    </div>
                    <div class="flex justify-end items-center">
                        <p class="text-2xl text-neutral-600 dark:text-neutral-300 mirco-card-price">&pound;{{ $price }}</p>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
