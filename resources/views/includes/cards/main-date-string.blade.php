@if ($diff > 1)
    {{--In future so green--}}
    <p class="inline-block px-2 py-1 rounded-full text-sm font-medium bg-success-300 text-success-700 dark:bg-success-700 dark:text-success-300">{{ $date->diffForHumans() }}</p>
@elseif ($diff <= 1 && $diff >= 0)
    {{--This week so yellow--}}
    <p class="inline-block px-2 py-1 rounded-full text-sm font-medium bg-warning-300 text-warning-700 dark:bg-warning-700 dark:text-warning-300">{{ $date->diffForHumans() }}</p>
@elseif ($diff < 0)
    {{--More than a week late--}}
    <p class="inline-block px-2 py-1 rounded-full text-sm font-medium bg-error-300 text-error-700 dark:bg-error-700 dark:text-error-300">{{ $date->diffForHumans() }}</p>
@endif
