<div>
    <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto">
        <div class="grid col-template-mini-card">
            <div class="p-4 grid grid-cols-2">
                <div>
                    <p class="text-md font-light italic text-neutral-600 dark:text-neutral-300 mini-card-date">{{ $date }}</p>
                </div>
                <div class="text-right">
                    <p class="text-xl text-neutral-600 dark:text-neutral-300 mini-card-quantity">{{ $quantity }}x</p>
                </div>
                <div class="mt-2 flex items-end">
                    <p class="text-3xl font-medium text-neutral-800 dark:text-neutral-200 mini-card-total">&pound;{{ $total }}</p>
                </div>
                <div class="mt-2 flex justify-end items-end">
                    <p class="text-xl text-neutral-600 dark:text-neutral-300 mini-card-unit">&pound;{{ $unit }}</p>
                </div>
            </div>
            <div class="bg-portfolio-gray rounded-r-lg">
                <a href="{{ $link }}">
                    <div class="flex items-center justify-center h-full">
                        <p class="text-2xl w-16 text-center"><em class="fas fa-pen text-neutral-500"></em></p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
