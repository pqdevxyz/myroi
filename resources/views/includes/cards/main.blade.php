<div>
    <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto h-full">
        <div class="grid grid-rows-2 h-full col-template-main-card">
            <div class="p-4 grid grid-cols-2">
                <div class="">
                    <p class="text-md font-semibold uppercase tracking-wide mb-1 text-neutral-800 dark:text-neutral-200">{{ $title }}</p>
                    <p class="text-md font-light italic uppercase tracking-wide text-neutral-600 dark:text-neutral-300">{{ $subtitle }}</p>
                </div>
                <div class="text-right">
                    @include('includes.cards.main-date-string', ['date' => $date, 'diff' => \Carbon\Carbon::now()->diffInWeeks($date, false)])
                </div>
                <div class="flex items-end mt-4">
                    <p class="text-3xl text-neutral-800 dark:text-neutral-200">&pound;{{ $price }}</p>
                </div>
                <div class="flex items-end justify-end mt-4">
                    <p class="text-xl italic text-neutral-600 dark:text-neutral-300">{{ $quantity }}</p>
                </div>
            </div>
            <a href="{{ $link }}">
                <div class="shadow-lg rounded-b-lg w-auto bg-neutral-200 dark:bg-neutral-900 hover:bg-neutral-300 dark:hover:bg-neutral-800">
                    <div class="p-4 text-center">
                        <p class="font-semibold uppercase tracking-wide text-neutral-600 dark:text-neutral-300">View Details</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
