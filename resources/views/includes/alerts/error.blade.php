@if(session('error') || $errors->any())
    <div class="container mx-auto mt-6 max-w-4xl" id="alert-error">
        <div class="bg-error-100 dark:bg-error-900 shadow-lg rounded-lg mx-4">
            <div class="flex">
                <div class="flex flex-grow-0 items-center justify-center bg-error-300 dark:bg-error-700 rounded-l-lg">
                    <p class="text-2xl w-16 text-center"><em class="fas fa-exclamation-circle text-error-700 dark:text-error-300"></em></p>
                </div>
                <div class="flex-grow mx-4">
                    <h4 class="font-medium text-error-900 dark:text-error-100 pt-4">An Error Occurred</h4>
                    <div class="pb-4" id="alert-error-message">
                        @if(is_array(session('error')))
                            @foreach(session('error') as $message)
                                <p class="text-sm text-error-700 dark:text-error-300">
                                    {{ $message }}
                                </p>
                            @endforeach
                        @elseif($errors->any())
                            @foreach($errors->all() as $message)
                                <p class="text-sm text-error-700 dark:text-error-300">
                                    {{ $message }}
                                </p>
                            @endforeach
                        @else
                            <p class="text-sm text-error-700 dark:text-error-300">
                                {{ session('error') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
