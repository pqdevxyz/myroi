@if(session('info'))
    <div class="container mx-auto mt-6 max-w-4xl" id="alert-info">
        <div class="bg-info-100 dark:bg-info-900 shadow-lg rounded-lg mx-4">
            <div class="flex">
                <div class="flex flex-grow-0 items-center justify-center bg-info-300 dark:bg-info-700 rounded-l-lg">
                    <p class="text-2xl w-16 text-center"><em class="fas fa-info-circle text-info-700 dark:text-info-300"></em></p>
                </div>
                <div class="flex-grow mx-4">
                    <h4 class="font-medium text-info-900 dark:text-info-100 pt-4">Info</h4>
                    <div class="pb-4" id="alert-info-message">
                        @if(is_array(session('info')))
                            @foreach(session('info') as $message)
                                <p class="text-sm text-info-700 dark:text-info-300">
                                    {{ $message }}
                                </p>
                            @endforeach
                        @else
                            <p class="text-sm text-info-700 dark:text-info-300">
                                {{ session('info') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
