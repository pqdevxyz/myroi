@extends('dashboard.base')
@section('site-title', 'Edit Transaction | My ROI')

@section('content')
    <div class="max-w-screen-lg mx-auto mt-8 md:mt-16">
        <div class="mx-4 flex justify-between">
            <h1 class="text-2xl text-neutral-600 dark:text-neutral-300 font-light">Edit Transaction</h1>
            <a href="{{ route('symbol.view.open', ['id' => $transaction->symbol->id]) }}" id="back-button" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 dark:hover:bg-neutral-600 dark:text-neutral-300">Back</a>
        </div>
        <hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
    </div>

    {{--Transaction Details--}}
    <div class="max-w-screen-lg mx-auto mt-8" id="transaction-details">
        @include('dashboard.transaction.components.transaction-details', ['action' => route('transaction.edit', ['id' => $transaction->id])])
    </div>

    {{--Delete Transaction--}}
    <div class="max-w-screen-lg mx-auto mt-8" id="transaction-delete">
        @include('dashboard.transaction.components.transaction-delete')
    </div>

    <div class="mb-24"></div>
@endsection
