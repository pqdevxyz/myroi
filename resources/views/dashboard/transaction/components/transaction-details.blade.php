<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Transaction Details</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can change any of the transaction settings here, this includes the quantity dealt, unit price, charges & tax and transaction date.</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can also set an adjustment value for accumulation OEICs.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <form action="{{ $action }}" method="post">
            @csrf
            <div class="space-y-4 mx-auto">
                <div>
                    <label for="quantity" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Quantity Dealt</label>
                    <div class="mt-1 flex rounded-md">
                        <input type="number" step="any" name="quantity" id="quantity" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->quantity ?? 0 }}">
                    </div>
                </div>
                <div>
                    <label for="buy_price" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Unit Price</label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <span class="inline-flex items-center px-3 rounded-l border border-r-0 border-neutral-300 bg-neutral-50 text-neutral-500 dark:border-neutral-600 dark:text-neutral-300 text-sm">&pound;</span>
                        <input type="number" step="any" name="buy_price" id="buy_price" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded-r text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->buy_price ?? 0 }}">
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <label for="charges" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Charges</label>
                        <div class="mt-1 flex rounded-md shadow-sm">
                            <span class="inline-flex items-center px-3 rounded-l border border-r-0 border-neutral-300 bg-neutral-50 text-neutral-500 dark:border-neutral-600 dark:text-neutral-300 text-sm">&pound;</span>
                            <input type="number" step="any" name="charges" id="charges" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded-r text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->charges ?? 0 }}">
                        </div>
                    </div>
                    <div>
                        <label for="tax" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Tax</label>
                        <div class="mt-1 flex rounded-md shadow-sm">
                            <span class="inline-flex items-center px-3 rounded-l border border-r-0 border-neutral-300 bg-neutral-50 text-neutral-500 dark:border-neutral-600 dark:text-neutral-300 text-sm">&pound;</span>
                            <input type="number" step="any" name="tax" id="tax" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded-r text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->tax ?? 0 }}">
                        </div>
                    </div>
                </div>
                <div>
                    <label for="buy_date" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Transaction Date</label>
                    <div class="mt-1 flex rounded-md">
                        <input type="date" name="buy_date" id="buy_date" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->buy_date?->format('Y-m-d') }}">
                    </div>
                </div>
                <hr class="border border-neutral-200 dark:border-neutral-700">
                <div>
                    <label for="adjustment" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Adjustment</label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <span class="inline-flex items-center px-3 rounded-l border border-r-0 border-neutral-300 bg-neutral-50 text-neutral-500 dark:border-neutral-600 dark:text-neutral-300 text-sm">&pound;</span>
                        <input type="number" step="any" name="adjustment" id="adjustment" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded-r text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $transaction->adjustment ?? 0 }}">
                    </div>
                </div>
                <div>
                    <div class="text-right pt-2">
                        <button type="submit" class="px-3 py-2 rounded-md text-sm font-medium text-primary-800 bg-primary-200 hover:bg-primary-300 dark:bg-primary-800 dark:text-primary-500 dark:hover:bg-primary-900 w-full md:w-auto">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
