<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Transaction Details</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can view the transaction here, this includes the quantity dealt, unit price, charges & tax and transaction date.</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can also view the adjustment value for accumulation OEICs.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <div class="space-y-4 mx-auto">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Quantity Dealt</p>
                    <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400">{{ $transaction->quantity }}</p>
                </div>
                <div>
                    <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Unit Price</p>
                    <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400"><span class="text-sm">&pound;</span>{{ $transaction->buy_price }}</p>
                </div>
                <div>
                    <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Charges</p>
                    <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400"><span class="text-sm">&pound;</span>{{ $transaction->charges }}</p>
                </div>
                <div>
                    <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Tax</p>
                    <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400"><span class="text-sm">&pound;</span>{{ $transaction->tax }}</p>
                </div>
            </div>
            <div>
                <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Transaction Date</p>
                <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400">{{ $transaction->buy_date?->format('d M Y') }}</p>
            </div>
            <hr class="border border-neutral-200 dark:border-neutral-700">
            <div>
                <p class="text-sm font-medium text-neutral-700 dark:text-neutral-300">Adjustment</p>
                <p class="text-2xl font-light text-neutral-600 dark:text-neutral-400"><span class="text-sm">&pound;</span>{{ $transaction->adjustment }}</p>
            </div>
        </div>
    </div>
</div>
