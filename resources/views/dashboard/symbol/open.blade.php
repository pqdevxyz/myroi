@extends('dashboard.base')
@section('site-title', 'View Symbol | My ROI')

@push('header-cta')
    @include('includes.cta.secondary', ['name' => 'Add Historic Price', 'link' => route('historic-price.create', ['symbolId' => $symbol->id])])
    @include('includes.cta.primary', ['name' => 'Add Transaction', 'link' => route('transaction.create', ['symbolId' => $symbol->id])])
@endpush

@include('dashboard.symbol.includes.banner-open', ['symbol' => $symbol])

@section('content')
    <div class="container mx-auto mt-16" id="simple-cards">
        <div class="mx-4 grid md:grid-cols-5 gap-4">
            <div class="simple-card">
                @include('includes.cards.simple', [
                    'title' => 'Net ROI',
                    'subtitle' => number_format($symbol->net_roi_percentage, 2) . '%',
                    'colour' => 'primary-300'
                ])
            </div>
            <div class="simple-card">
                @include('includes.cards.simple', [
                    'title' => 'Total Sale',
                    'subtitle' => isset($symbol->total_sale) ? '£' . number_format($symbol->total_sale, 2) : 'No Data' ,
                    'colour' => 'primary-300'
                ])
            </div>
            <div class="simple-card">
                @include('includes.cards.simple', [
                    'title' => 'Latest Pricing',
                    'subtitle' => isset($symbol->latest_price) ? '£' . number_format($symbol->latest_price->close_price, 2) : 'No Data' ,
                    'colour' => 'primary-300'
                ])
            </div>
            <div class="simple-card">
                @include('includes.cards.simple', [
                    'title' => 'Latest Pricing Date',
                    'subtitle' => isset($symbol->latest_price) ? $symbol->latest_price->date->format('jS M Y') : 'No Data',
                    'colour' => 'neutral-300'
                ])
            </div>
            <div class="simple-card">
                @include('includes.cards.simple', [
                   'title' => 'Next Pricing Date',
                   'subtitle' => isset($symbol->next_price_date) ? $symbol->next_price_date->format('jS M Y') : 'No Data',
                   'colour' => 'neutral-300'
                ])
            </div>
        </div>
    </div>

    <div class="container mx-auto mt-16 hidden md:block" id="transaction-cards">
        <p class="mx-4 font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Last 4 Transactions</p>
        <div class="mx-4 grid md:grid-cols-4 gap-4">
            @foreach($symbol->transactions->take(4) as $transaction)
                <div class="transaction-card">
                    @include('includes.cards.mini', [
                        'date' => $transaction->buy_date->format('jS M Y'),
                        'quantity' => number_format($transaction->quantity, 2),
                        'total' => number_format($transaction->total_cost, 2),
                        'unit' => number_format($transaction->buy_price, 2),
                        'link' => route('transaction.edit', ['id' => $transaction->id])
                    ])
                </div>
            @endforeach
        </div>
    </div>

    <div class="container mx-auto mt-16">
        <div class="mx-4 grid md:grid-cols-12 gap-4">
            <div class="md:col-start-1 md:col-end-8" id="transaction-table">
                <p class="font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Transactions</p>
                @include('dashboard.symbol.components.transactions', ['transactions' => $symbol->transactions, 'mode' => 'edit'])
            </div>
            <div class="md:col-start-8 md:col-end-13" id="historic-price-table">
                <p class="font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Historic Prices</p>
                @include('dashboard.symbol.components.historic-prices', ['historicPrices' => $symbol->historicPrices, 'mode' => 'edit'])

            </div>
        </div>
    </div>

    <div class="mb-24"></div>
@endsection
