<div>
    <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto overflow-hidden hidden md:block">
        <table class="w-full divide-y divide-neutral-200 dark:divide-neutral-700" aria-describedby="Historic prices for this symbol">
            <thead class="bg-white dark:bg-neutral-700">
                <tr>
                    <th scope="col" class="px-6 py-4 text-left text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-4 text-right text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Price
                    </th>
                    <th scope="col" class="px-6 py-4 text-right text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Roi
                    </th>
                    <th scope="col" class="relative px-3 py-4">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
            </thead>
            <tbody class="divide-y divide-neutral-200 dark:divide-neutral-700">
                @foreach($historicPrices as $historicPrice)
                    <tr @if($loop->odd) class="bg-neutral-100 dark:bg-neutral-600" @endif>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <p class="font-semibold text-lg text-neutral-600 dark:text-neutral-300">
                                {{ $historicPrice->date->format('jS M Y') }}
                            </p>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <p class="font-medium text-neutral-600 dark:text-neutral-300">
                                &pound;{{ number_format($historicPrice->close_price, 2) }}
                            </p>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <p class="text-neutral-500 dark:text-neutral-400">
                                {{ number_format($historicPrice->roi_percentage, 2) }}&percnt;
                            </p>
                        </td>

                        <td class="px-3 py-4 whitespace-nowrap text-right">
                            <a href="{{ route('historic-price.edit', ['id' => $historicPrice->id]) }}" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 dark:hover:bg-neutral-800 dark:text-neutral-300">Edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="grid gap-4 block md:hidden">
        @foreach($historicPrices as $historicPrice)
            @include('includes.cards.small', [
                'title' => $historicPrice->date->format('jS M Y'),
                'subtitle' => number_format($historicPrice->roi_percentage, 2) . '%',
                'price' => number_format($historicPrice->close_price, 2),
                'link' => route('historic-price.edit', ['id' => $historicPrice->id]),
                'colour' => $historicPrice->roi_percentage > 0 ? 'lime' : 'gray',
                'belongsTo' => $transaction->id,
            ])
        @endforeach
    </div>
</div>
