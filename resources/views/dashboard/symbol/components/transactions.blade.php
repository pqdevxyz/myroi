<div>
    <div class="bg-white dark:bg-neutral-700 shadow-lg rounded-lg w-auto overflow-hidden hidden md:block">
        <table class="w-full divide-y divide-neutral-200 dark:divide-neutral-700" aria-describedby="Transactions for this symbol">
            <thead class="bg-white dark:bg-neutral-700">
                <tr>
                    <th scope="col" class="px-6 py-4 text-left text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-4 text-right text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Unit Price
                    </th>
                    <th scope="col" class="px-6 py-4 text-right text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Quantity
                    </th>
                    <th scope="col" class="px-6 py-4 text-right text-sm font-semibold text-neutral-500 dark:text-neutral-300 uppercase tracking-wide">
                        Total Cost
                    </th>
                    <th scope="col" class="relative px-3 py-4">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
            </thead>
            <tbody class="divide-y divide-neutral-200 dark:divide-neutral-700">
                @foreach($transactions as $transaction)
                    <tr @if($loop->odd) class="bg-neutral-100 dark:bg-neutral-600" @endif>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <p class="font-semibold text-lg text-neutral-600 dark:text-neutral-300">
                                {{ $transaction->buy_date->format('jS M Y') }}
                            </p>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <p class="text-neutral-500 dark:text-neutral-400">
                                &pound;{{ number_format($transaction->buy_price, 2) }}
                            </p>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <p class="text-neutral-500 dark:text-neutral-400">
                                {{ number_format($transaction->quantity, 2) }}
                            </p>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right">
                            <p class="font-medium text-neutral-600 dark:text-neutral-300">
                                &pound;{{ number_format($transaction->total_cost, 2) }}
                            </p>
                        </td>
                        <td class="px-3 py-4 whitespace-nowrap text-right">
                            @if (isset($mode) && $mode == 'edit')
                                <a href="{{ route('transaction.edit', ['id' => $transaction->id]) }}" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 dark:hover:bg-neutral-800 dark:text-neutral-300">Edit</a>
                            @else
                                <a href="{{ route('transaction.view', ['id' => $transaction->id]) }}" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 dark:hover:bg-neutral-800 dark:text-neutral-300">View</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="grid gap-4 block md:hidden">
        @foreach($transactions as $transaction)
            @include('includes.cards.small', [
                'title' => $transaction->buy_date->format('jS M Y'),
                'subtitle' => '£' . number_format($transaction->buy_price, 2),
                'price' => number_format($transaction->total_cost, 2),
                'link' => route('transaction.' . (isset($mode) && $mode == 'edit' ? 'edit' : 'view'), ['id' => $transaction->id]),
                'colour' => $symbol->portfolio->colour,
                'belongsTo' => $transaction->id,
            ])
        @endforeach
    </div>
</div>
