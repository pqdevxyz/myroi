@extends('dashboard.base')
@section('site-title', 'View Symbol | My ROI')

@include('dashboard.symbol.includes.banner-closed', ['symbol' => $symbol])

@section('content')
    <div class="container mx-auto mt-16">
        <div class="mx-4" id="transaction-table">
            <p class="font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Transactions</p>
            @include('dashboard.symbol.components.transactions', ['transactions' => $symbol->transactions, 'mode' => 'view'])
        </div>
    </div>

    <div class="mb-24"></div>
@endsection
