@section('banner')
    <div class="bg-neutral-200 dark:bg-neutral-700" id="banner">
        <div class="container mx-auto">
            <div class="grid">
                <div class="my-8 mx-16 text-center">
                    <p class="font-semibold text-md uppercase tracking-wide text-neutral-500">Name</p>
                    <p class="font-light text-4xl text-neutral-900 dark:text-neutral-200">{{ $symbol->name }}</p>
                </div>
            </div>
            <div class="grid grid-cols-2 md:grid-cols-6 border-t border-neutral-400 dark:border-neutral-500">
                <div class="md:col-start-2 my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500">Index</p>
                    <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300">{{ $symbol->index }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500">Ticker</p>
                    <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300">{{ $symbol->ticker }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500">Portfolio</p>
                    <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300">{{ $symbol->portfolio->name }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-neutral-500">Total Profit</p>
                    <p class="font-light text-2xl text-neutral-800 dark:text-neutral-300">&pound;{{ number_format($symbol->total_profit, 2) }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
