@section('banner')
    <div class="bg-primary-100 dark:bg-primary-900" id="banner">
        <div class="container mx-auto">
            <div class="grid">
                <div class="my-8 mx-16 text-center">
                    <p class="font-semibold text-md uppercase tracking-wide text-primary-500 dark:text-primary-600">Name</p>
                    <p class="font-light text-4xl text-primary-900 dark:text-primary-100">{{ $symbol->name }}</p>
                </div>
            </div>
            <div class="grid grid-cols-2 md:grid-cols-6 border-t border-primary-400 dark:border-primary-700">
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">Index</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">{{ $symbol->index }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">Ticker</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">{{ $symbol->ticker }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">Portfolio</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">{{ $symbol->portfolio->name }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">Total Cost</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">&pound;{{ number_format($symbol->total_cost, 2) }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">Quantity Held</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">{{ number_format($symbol->total_quantity, 2) }}</p>
                </div>
                <div class="my-8 mx-12 text-center">
                    <p class="font-semibold text-sm uppercase tracking-wide text-primary-500 dark:text-primary-600">&percnt; of Portfolio</p>
                    <p class="font-light text-2xl text-primary-800 dark:text-primary-200">{{ number_format($symbol->percentage_of_portfolio, 2) }}&percnt;</p>
                </div>
            </div>
        </div>
    </div>
@endsection
