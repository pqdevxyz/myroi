@extends('dashboard.base')
@section('site-title', 'Portfolios | My ROI')

@push('header-cta')
    @include('includes.cta.primary', ['name' => 'Add Portfolio', 'link' => route('portfolio.create')])
@endpush

@include('dashboard.portfolio.includes.banner', ['total' => $totals['cost'], 'quantity' => $totals['quantity']])

@section('content')
    @if (count($next3) > 0)
        <div class="container mx-auto mt-16">
            <p class="mx-4 font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Next Price Due</p>
            <div class="mx-4 grid md:grid-cols-3 gap-4">
                @foreach($next3 as $next)
                    @include('includes.cards.main', [
                        'title' => $next->name,
                        'subtitle' => $next->ticker,
                        'date' => $next->next_price_date,
                        'price' => number_format($next->total_cost, 2),
                        'quantity' => number_format($next->total_quantity, 2),
                        'link' => route('symbol.view.open', ['id' => $next->id]),
                    ])
                @endforeach
            </div>
        </div>
    @endif

    @if (count($portfolios) > 0)
        <div class="container mx-auto mt-16" id="portfolio-cards-new">
            <p class="mx-4 font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Portfolios</p>
            <div class="mx-4 grid sm:grid-cols-2 md:grid-cols-3 gap-4">
                @foreach($portfolios as $portfolio)
                    <div class="portfolio-card-new">
                        @include('includes.cards.micro', [
                            'title' => $portfolio->name,
                            'price' => number_format($portfolio->total_cost, 2),
                            'link' => route('portfolio.manage', ['id' => $portfolio->id]),
                            'colour' => $portfolio->colour,
                        ])
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @if (count($symbols) > 0)
        <div class="container mx-auto mt-16" id="symbol-cards-new">
            <p class="mx-4 font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Breakdown by Portfolio</p>
            <div class="mx-4 grid sm:grid-cols-2 md:grid-cols-4 gap-4 md:gap-8">
                @foreach($symbols as $group)
                    @foreach($group as $symbol)
                        <div class="symbol-card-new">
                            @include('includes.cards.small', [
                                'title' => $symbol->name,
                                'subtitle' => $symbol->ticker,
                                'price' => number_format($symbol->total_cost, 2),
                                'link' => route('symbol.view.open', ['id' => $symbol->id]),
                                'colour' => $symbol->portfolio->colour,
                                'belongsTo' => $symbol->portfolio->name,
                            ])
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>
    @endif

    @if (count($closed) > 0)
        <div class="container mx-auto mt-16" id="symbol-closed-cards">
            <p class="mx-4 font-semibold uppercase tracking-wide text-sm mb-4 text-neutral-800 dark:text-neutral-300">Closed Symbols</p>
            <div class="mx-4 grid sm:grid-cols-2 md:grid-cols-4 gap-4 md:gap-8">
                @foreach($closed as $close)
                    <div class="symbol-closed-card">
                        @include('includes.cards.small', [
                            'title' => $close->name,
                            'subtitle' => $close->ticker,
                            'price' => number_format($close->total_profit, 2),
                            'link' => route('symbol.view.closed', ['id' => $close->id]),
                            'colour' => $close->portfolio->colour,
                            'belongsTo' => $close->portfolio->name,
                        ])
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    <div class="mb-24"></div>
@endsection
