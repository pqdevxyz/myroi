<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Symbols</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can add & update any symbols that belong to this portfolio.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <div class="text-right">
            <button id="portfolio-add-symbol-button" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-800 bg-neutral-200 hover:bg-neutral-300 dark:hover:bg-neutral-600 dark:text-neutral-300 dark:bg-neutral-700">Add</button>
        </div>
        <div class="hidden" id="portfolio-symbol-new">
            <div class="mt-6">
                <div class="bg-white dark:bg-neutral-900 rounded-lg shadow-lg">
                    <div class="p-4">
                        <div class="flex justify-between items-center">
                            <p class="text-lg text-neutral-600 dark:text-neutral-400">New Symbol</p>
                        </div>
                    </div>
                    <div class="p-4 bg-neutral-100 dark:bg-neutral-800">
                        <div>
                            <form action="{{ route('symbol.create', ['portfolioId' => $portfolio->id]) }}" class="grid md:grid-cols-2 gap-4" method="post">
                                @csrf
                                <div class="md:col-start-1 md:col-end-3">
                                    <div class="rounded-md">
                                        <input type="text" name="name" id="symbols.name.new" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Name">
                                    </div>
                                </div>
                                <div>
                                    <div class="rounded-md">
                                        <input type="text" name="ticker" id="symbols.ticker.new" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Ticker">
                                    </div>
                                </div>
                                <div>
                                    <div class="rounded-md">
                                        <input type="text" name="index" id="symbols.index.new" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Index">
                                    </div>
                                </div>
                                <div class="md:col-start-1 md:col-end-3 md:flex justify-end items-center">
                                    <button type="submit" class="px-3 py-2 rounded-md text-sm font-medium text-primary-800 bg-primary-200 hover:bg-primary-300 w-full md:w-auto dark:bg-primary-800 dark:text-primary-500 dark:hover:bg-primary-900">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portfolio-symbol-outer">
            @foreach($portfolio->symbols as $key => $symbol)
                <div class="mt-6 portfolio-symbol">
                    <div class="bg-white dark:bg-neutral-900 rounded-lg shadow-lg">
                        <div class="p-4 symbol-accordion cursor-pointer" data-selector="{{ $key }}">
                            <div class="flex justify-between items-center">
                                <p class="text-lg text-neutral-600 dark:text-neutral-400">{{ $symbol->name }}</p>
                                <em class="fas fa-chevron-down text-neutral-500 dark:text-neutral-400 symbol-accordion-icon" data-icon="{{ $key }}"></em>
                            </div>
                        </div>
                        <div class="p-4 bg-neutral-100 dark:bg-neutral-800 hidden symbol-accordion-form" data-form="{{ $key }}">
                            <div>
                                <form action="{{ route('symbol.edit', ['id' => $symbol->id]) }}" class="grid md:grid-cols-2 gap-4" method="post">
                                    @csrf
                                    <div class="md:col-start-1 md:col-end-3">
                                        <div class="rounded-md">
                                            <input type="text" name="name" id="symbols.name.{{ $key }}" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Name" value="{{ $symbol->name }}">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="rounded-md">
                                            <input type="text" name="ticker" id="symbols.ticker.{{ $key }}" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Ticker" value="{{ $symbol->ticker }}">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="rounded-md">
                                            <input type="text" name="index" id="symbols.index.{{ $key }}" class="focus:ring-neutral-500 focus:border-neutral-500 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" placeholder="Symbol Index" value="{{ $symbol->index }}">
                                        </div>
                                    </div>
                                    <div class="md:col-span-2">
                                        <a href="{{ route('symbol.remove', ['id' => $symbol->id]) }}" class="px-3 py-2 rounded-md text-sm font-medium text-error-500 hover:bg-error-100 dark:hover:bg-error-700 dark:hover:text-error-300 float-left">Delete</a>
                                        <a href="{{ route('symbol.position', ['id' => $symbol->id]) }}" class="px-3 py-2 ml-2 rounded-md text-sm font-medium text-info-500 hover:bg-info-100 dark:hover:bg-info-700 dark:hover:text-info-300 float-left">{{ $symbol->is_closed ? 'Re-open Symbol':'Close Symbol' }}</a>
                                        <button type="submit" class="px-3 py-2 rounded-md text-sm font-medium text-neutral-500 hover:bg-neutral-200 float-right dark:hover:bg-neutral-900 dark:text-neutral-300">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
