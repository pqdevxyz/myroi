<div class="mx-4 grid md:grid-cols-3 gap-6">
    <div class="md:col-start-1 md:col-end-2">
        <p class="text-lg text-neutral-800 dark:text-neutral-300">Portfolio Details</p>
        <p class="text-sm text-neutral-500 dark:text-neutral-400 mt-2">You can change any of the portfolio settings here, this includes the name, type and colour.</p>
    </div>
    <div class="md:col-start-2 md:col-end-4">
        <form action="{{ $action }}" method="post">
            @csrf
            <div class="space-y-4 mx-auto">
                <div>
                    <label for="name" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Name</label>
                    <div class="mt-1 flex rounded-md">
                        <input type="text" name="name" id="name" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600" value="{{ $portfolio->name }}">
                    </div>
                </div>
                <div>
                    <label for="type" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Type</label>
                    <div class="mt-1 flex rounded-md">
                        <select name="type" id="type" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600">
                            <option value="dealing" @if($portfolio->type == 'dealing') selected @endif>Dealing</option>
                            <option value="isa" @if($portfolio->type == 'isa') selected @endif>ISA</option>
                            <option value="sipp" @if($portfolio->type == 'sipp') selected @endif>SIPP</option>
                        </select>
                    </div>
                </div>
                <div>
                    <label for="colour" class="block text-sm font-medium text-neutral-700 dark:text-neutral-400">Colour</label>
                    <div class="mt-1 flex rounded-md">
                        <select name="colour" id="colour" class="focus:ring-primary-500 focus:border-primary-500 flex-1 block w-full rounded text-sm border-neutral-300 dark:bg-neutral-800 dark:text-neutral-300 dark:border-neutral-600">
                            <option value="blueGray" @if($portfolio->colour == 'blueGray') selected @endif>Blue Gray</option>
                            <option value="coolGray" @if($portfolio->colour == 'coolGray') selected @endif>Cool Gray</option>
                            <option value="gray" @if($portfolio->colour == 'gray') selected @endif>Gray</option>
                            <option value="trueGray" @if($portfolio->colour == 'trueGray') selected @endif>True Gray</option>
                            <option value="warmGray" @if($portfolio->colour == 'warmGray') selected @endif>Warm Gray</option>
                            <option value="red" @if($portfolio->colour == 'red') selected @endif>Red</option>
                            <option value="orange" @if($portfolio->colour == 'orange') selected @endif>Orange</option>
                            <option value="amber" @if($portfolio->colour == 'amber') selected @endif>Amber</option>
                            <option value="yellow" @if($portfolio->colour == 'yellow') selected @endif>Yellow</option>
                            <option value="lime" @if($portfolio->colour == 'lime') selected @endif>Lime</option>
                            <option value="green" @if($portfolio->colour == 'green') selected @endif>Green</option>
                            <option value="emerald" @if($portfolio->colour == 'emerald') selected @endif>Emerald</option>
                            <option value="teal" @if($portfolio->colour == 'teal') selected @endif>Teal</option>
                            <option value="cyan" @if($portfolio->colour == 'cyan') selected @endif>Cyan</option>
                            <option value="lightBlue" @if($portfolio->colour == 'lightBlue') selected @endif>Light Blue</option>
                            <option value="blue" @if($portfolio->colour == 'blue') selected @endif>Blue</option>
                            <option value="indigo" @if($portfolio->colour == 'indigo') selected @endif>Indigo</option>
                            <option value="violet" @if($portfolio->colour == 'violet') selected @endif>Violet</option>
                            <option value="purple" @if($portfolio->colour == 'purple') selected @endif>Purple</option>
                            <option value="fuchsia" @if($portfolio->colour == 'fuchsia') selected @endif>Fuchsia</option>
                            <option value="pink" @if($portfolio->colour == 'pink') selected @endif>Pink</option>
                            <option value="rose" @if($portfolio->colour == 'rose') selected @endif>Rose</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div class="text-right pt-2">
                        <button type="submit" class="px-3 py-2 rounded-md text-sm font-medium text-primary-800 bg-primary-200 hover:bg-primary-300 w-full md:w-auto dark:bg-primary-800 dark:text-primary-500 dark:hover:bg-primary-900">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<hr class="mx-4 mt-8 border border-neutral-200 dark:border-neutral-700">
