@if ($paginator->hasPages())
    <nav role="navigation" aria-label="Pagination Navigation" class="flex items-center justify-between roi-pagination">
        <div class="flex-1 flex items-center justify-between bg-white shadow-lg rounded">
            <div class="px-4 py-2">
                <p class="text-sm text-gray-500">
                    {!! __('Showing') !!}
                    <span class="font-medium">{{ $paginator->firstItem() }}</span>
                    {!! __('to') !!}
                    <span class="font-medium">{{ $paginator->lastItem() }}</span>
                    {!! __('of') !!}
                    <span class="font-medium">{{ $paginator->total() }}</span>
                    {!! __('results') !!}
                </p>
            </div>
            <div class="pr-4 py-2">
                <span class="relative z-0 inline-flex">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span class="roi-pagination-back" aria-disabled="true" aria-label="{{ __('pagination.previous') }}">
                            <span class="relative inline-flex items-center mx-1 px-2 py-2 text-sm font-medium text-gray-400 bg-white cursor-default rounded" aria-hidden="true">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="relative inline-flex items-center mx-1 px-2 py-2 text-sm font-medium text-green-400 bg-white hover:text-white hover:bg-green-400 focus:z-10 focus:outline-none focus:shadow-outline-green active:bg-green-200 active:text-green-500 rounded roi-pagination-back" aria-label="{{ __('pagination.previous') }}">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                            </svg>
                        </a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span aria-disabled="true">
                                <span class="relative inline-flex items-center mx-1 px-4 py-2 -ml-px text-sm font-medium text-gray-400 bg-white cursor-default rounded">{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span aria-current="page" class="@if($page == 1) roi-pagination-first @elseif($page == $paginator->lastPage()) roi-pagination-last @endif">
                                        <span class="relative inline-flex items-center mx-1 px-4 py-2 -ml-px text-sm font-medium text-gray-400 bg-gray-200 cursor-default rounded">{{ $page }}</span>
                                    </span>
                                @else
                                    <a href="{{ $url }}" class="relative inline-flex items-center mx-1 px-4 py-2 -ml-px text-sm font-medium text-green-400 bg-white hover:text-white hover:bg-green-400 focus:z-10 focus:outline-none focus:shadow-outline-green active:bg-green-200 active:text-green-500 rounded @if($page == 1) roi-pagination-first @elseif($page == $paginator->lastPage()) vs-pagination-last @endif" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                        {{ $page }}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="relative inline-flex items-center mx-1 px-2 py-2 -ml-px text-sm font-medium text-green-400 bg-white hover:text-white hover:bg-green-400 focus:z-10 focus:outline-none focus:shadow-outline-green active:bg-green-200 active:text-green-500 rounded roi-pagination-forward" aria-label="{{ __('pagination.next') }}">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg>
                        </a>
                    @else
                        <span class="roi-pagination-forward" aria-disabled="true" aria-label="{{ __('pagination.next') }}">
                            <span class="relative inline-flex items-center mx-1 px-2 py-2 -ml-px text-sm font-medium text-gray-400 bg-white cursor-default rounded" aria-hidden="true">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @endif
                </span>
            </div>
        </div>
    </nav>
@endif
