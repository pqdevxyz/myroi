describe('Portfolio', () => {

    let user = {
        email:'paul@quine.co.uk',
        fullName: 'Paul Q',
    }

    before(() => {
        cy.artisan('roi:init -U');
    });

    describe('New Portfolio', () => {

        it('It works', () => {
            cy.login({
                email: user.email
            });

            //Visit Portfolio Overview page
            cy.visit('portfolio');

            //Check login message is displayed
            cy.get('#nav-auth').contains('Welcome ' + user.fullName);

            //Click on Create portfolio button
            cy.get('#nav-other-links  a[href*="portfolio/create"]').click();
            cy.assertRedirect('portfolio/create');

            //Fill out form
            cy.get('#name').clear().type('Test');
            cy.get('#type').select('isa');
            cy.get('#colour').select('indigo');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Portfolio created');

            //Click on the card for the portfolio we just created
            cy.get('#portfolio-cards-new').contains('Test').click();

            //Click on Add Symbol button
            cy.get('#portfolio-add-symbol-button').click();

            //Fill out form
            cy.get('#symbols\\.name\\.new').clear().type('Test 1');
            cy.get('#symbols\\.ticker\\.new').clear().type('1.Test');
            cy.get('#symbols\\.index\\.new').clear().type('Test');
            cy.get('#portfolio-symbol-new button[type=submit]').click();

            cy.get('#alert-success').contains('Symbol created');

            //Click on Add Symbol button
            cy.get('#portfolio-add-symbol-button').click();

            //Fill out form
            cy.get('#symbols\\.name\\.new').clear().type('Test 2');
            cy.get('#symbols\\.ticker\\.new').clear().type('2.Test');
            cy.get('#symbols\\.index\\.new').clear().type('Test');
            cy.get('#portfolio-symbol-new button[type=submit]').click();

            cy.get('#alert-success').contains('Symbol created');

            //Click on Add Symbol button
            cy.get('#portfolio-add-symbol-button').click();

            //Fill out form
            cy.get('#symbols\\.name\\.new').clear().type('Test 3');
            cy.get('#symbols\\.ticker\\.new').clear().type('3.Test');
            cy.get('#symbols\\.index\\.new').clear().type('Test');
            cy.get('#portfolio-symbol-new button[type=submit]').click();

            cy.get('#alert-success').contains('Symbol created');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Click on 1st New Symbol
            cy.get('#symbol-cards-new').contains('Test 1').click();

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('10');
            cy.get('#buy_price').clear().type('10');
            cy.get('#charges').clear().type('10');
            cy.get('#tax').clear().type('0');
            cy.get('#buy_date').clear().type('2021-01-01');
            cy.get('#adjustment').clear().type('0');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Transaction created');

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('20');
            cy.get('#buy_price').clear().type('10');
            cy.get('#charges').clear().type('10');
            cy.get('#tax').clear().type('0');
            cy.get('#buy_date').clear().type('2021-01-02');
            cy.get('#adjustment').clear().type('0');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Transaction created');

            //Check page has all correct data
            cy.get('#banner').contains('Total Cost').parent().should('contain.text', '£320.00');
            cy.get('#banner').contains('Quantity Held').parent().should('contain.text', '30.00');

            //Check the newest transaction first
            cy.get('#transaction-table tbody > tr:nth-child(1)')
                .should('contain.text', '2nd Jan 2021')
                .and('contain.text', '20.00')
                .and('contain.text', '£10.00')
                .and('contain.text', '£210.00');

            //Then check the oldest transaction
            cy.get('#transaction-table tbody > tr:nth-child(2)')
                .should('contain.text', '1st Jan 2021')
                .and('contain.text', '10.00')
                .and('contain.text', '£10.00')
                .and('contain.text', '£110.00');

            //Click on Add Historic Price button
            cy.get('#nav-other-links').contains('Add Historic Price').click();

            //Fill out form
            cy.get('#close_price').clear().type('15');
            cy.get('#date').clear().type('2021-01-01');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Historic Price created');

            //Click on Add Historic Price button
            cy.get('#nav-other-links').contains('Add Historic Price').click();

            //Fill out form
            cy.get('#close_price').clear().type('20');
            cy.get('#date').clear().type('2022-01-01');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Historic Price created');

            //Check the newest price first
            cy.get('#historic-price-table tbody > tr:nth-child(1)')
                .should('contain.text', '1st Jan 2022')
                .and('contain.text', '£20.00')
                .and('contain.text', '25.00%');

            //Then check the oldest price
            cy.get('#historic-price-table tbody > tr:nth-child(2)')
                .should('contain.text', '1st Jan 2021')
                .and('contain.text', '£15.00')
                .and('contain.text', '0.00%');

            //Check Simple Cards
            cy.get('#simple-cards').contains('Next Pricing Date').parent().parent().should('contain.text', '1st Jul 2022');
            cy.get('#simple-cards').contains('Latest Pricing Date').parent().parent().should('contain.text', '1st Jan 2022');
            cy.get('#simple-cards').contains('Total Sale').parent().parent().should('contain.text', '£600.00');
            cy.get('#simple-cards').contains('Net ROI').parent().parent().should('contain.text', '87.50%');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Click on 2nd New Symbol
            cy.get('#symbol-cards-new').contains('Test 2').click();

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('50');
            cy.get('#buy_price').clear().type('100');
            cy.get('#charges').clear().type('25');
            cy.get('#tax').clear().type('25');
            cy.get('#buy_date').clear().type('2021-01-01');
            cy.get('#adjustment').clear().type('0');
            cy.get('button[type=submit]').click();

            //Check the newest transaction first
            cy.get('#transaction-table tbody > tr:nth-child(1)')
                .should('contain.text', '1st Jan 2021')
                .and('contain.text', '50.00')
                .and('contain.text', '£100.00')
                .and('contain.text', '£5,050.00');

            //Click on Add Historic Price button
            cy.get('#nav-other-links').contains('Add Historic Price').click();

            //Fill out form
            cy.get('#close_price').clear();
            cy.get('#close_price').type('110');
            cy.get('#date').clear();
            cy.get('#date').type('2021-01-01');
            cy.get('button[type=submit]').click();

            //Check the newest price first
            cy.get('#historic-price-table tbody > tr:nth-child(1)')
                .should('contain.text', '1st Jan 2021')
                .and('contain.text', '£110.00')
                .and('contain.text', '0.00%');

            //Check Simple Cards
            cy.get('#simple-cards').contains('Next Pricing Date').parent().parent().should('contain.text', '1st Jul 2021');
            cy.get('#simple-cards').contains('Latest Pricing Date').parent().parent().should('contain.text', '1st Jan 2021');
            cy.get('#simple-cards').contains('Total Sale').parent().parent().should('contain.text', '£5,500.00');
            cy.get('#simple-cards').contains('Net ROI').parent().parent().should('contain.text', '8.91%');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Click on 3rd New Symbol
            cy.get('#symbol-cards-new').contains('Test 3').click();

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('30');
            cy.get('#buy_price').clear().type('10');
            cy.get('#charges').clear().type('10');
            cy.get('#tax').clear().type('0');
            cy.get('#buy_date').clear().type('2021-01-01');
            cy.get('#adjustment').clear().type('10');
            cy.get('button[type=submit]').click();

            //Check the newest transaction first
            cy.get('#transaction-table tbody > tr:nth-child(1)')
                .should('contain.text', '1st Jan 2021')
                .and('contain.text', '30.00')
                .and('contain.text', '£10.00')
                .and('contain.text', '£320.00');

            //Check Simple Cards
            cy.get('#simple-cards').contains('Next Pricing Date').parent().parent().should('contain.text', 'No Data');
            cy.get('#simple-cards').contains('Latest Pricing Date').parent().parent().should('contain.text', 'No Data');
            cy.get('#simple-cards').contains('Total Sale').parent().parent().should('contain.text', '£0.00');
            cy.get('#simple-cards').contains('Net ROI').parent().parent().should('contain.text', '-100.00%');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Check Portfolio Overview Data
            cy.get('#portfolio-cards-new').contains('Test')
                .should('contain.text', '£5,690.00');

            cy.get('#symbol-cards-new').contains('Test 1')
                .should('contain.text', '1.Test')
                .should('contain.text', 'Test 1')
                .and('contain.text', '£320.00')

            cy.get('#symbol-cards-new').contains('Test 2')
                .should('contain.text', '2.Test')
                .should('contain.text', 'Test 2')
                .and('contain.text', '£5,050.00')

            cy.get('#symbol-cards-new').contains('Test 3')
                .should('contain.text', '3.Test')
                .should('contain.text', 'Test 3')
                .and('contain.text', '£320.00')
        });

    });

    describe('Close Symbol in Portfolio', () => {

        it('It works', () => {
            cy.login({
                email: user.email
            });

            //Visit Portfolio Overview page
            cy.visit('portfolio');

            //Check login message is displayed
            cy.get('#nav-auth').contains('Welcome ' + user.fullName);

            //Click on Create portfolio button
            cy.get('#nav-other-links  a[href*="portfolio/create"]').click();
            cy.assertRedirect('portfolio/create');

            //Fill out form
            cy.get('#name').clear().type('Closed');
            cy.get('#type').select('SIPP');
            cy.get('#colour').select('Emerald');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Portfolio created');

            //Click on the card for the portfolio we just created
            cy.get('#portfolio-cards-new').contains('Closed').click();

            //Click on Add Symbol button
            cy.get('#portfolio-add-symbol-button').click();

            //Fill out form
            cy.get('#symbols\\.name\\.new').clear().type('Test A');
            cy.get('#symbols\\.ticker\\.new').clear().type('A.Test');
            cy.get('#symbols\\.index\\.new').clear().type('Test');
            cy.get('#portfolio-symbol-new button[type=submit]').click();

            cy.get('#alert-success').contains('Symbol created');

            //Click on Add Symbol button
            cy.get('#portfolio-add-symbol-button').click();

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Click on 1st New Symbol
            cy.get('#symbol-cards-new').contains('Test A').click();

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('20');
            cy.get('#buy_price').clear().type('1');
            cy.get('#charges').clear().type('0');
            cy.get('#tax').clear().type('0');
            cy.get('#buy_date').clear().type('2021-03-01');
            cy.get('#adjustment').clear().type('0');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Transaction created');

            //Click Add Transaction button
            cy.get('#nav-other-links').contains('Add Transaction').click();

            //Fill out form
            cy.get('#quantity').clear().type('-20');
            cy.get('#buy_price').clear().type('2');
            cy.get('#charges').clear().type('0');
            cy.get('#tax').clear().type('0');
            cy.get('#buy_date').clear().type('2021-03-02');
            cy.get('#adjustment').clear().type('0');
            cy.get('button[type=submit]').click();

            cy.get('#alert-success').contains('Transaction created');

            //Check page has all correct data
            cy.get('#banner').contains('Total Cost').parent().should('contain.text', '£0.00');
            cy.get('#banner').contains('Quantity Held').parent().should('contain.text', '0.00');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            //Click on portfolio card
            cy.get('#portfolio-cards-new').contains('Closed').click();

            //Open the accordion for the symbol
            cy.get('#portfolio-symbols').contains('Test A').click();

            //Click on the close symbol button
            cy.get('#portfolio-symbols .symbol-accordion-form').contains('Close Symbol').click();

            cy.get('#alert-success').contains('Symbol updated');

            //Click Portfolio Overview button
            cy.get('#nav-main-links  a[href*="portfolio"]').click();

            cy.get('#symbol-closed-cards').contains('Test A')
                .should('contain.text', 'A.Test')
                .should('contain.text', 'Test A')
                .and('contain.text', '£20.00')
        });

    });

});
