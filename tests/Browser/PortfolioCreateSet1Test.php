<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Portfolio\PortfolioDetails;
use Tests\Browser\Pages\Portfolio\PortfolioCreate;
use Tests\DuskTestCase;

class PortfolioCreateSet1Test extends DuskTestCase
{
    /**
     * Check portfolio create page has all the correct elements.
     *
     * @test checkPortfolioCreatePageHasAllTheCorrectElements
     * @group portfolio.create
     */
    public function checkPortfolioCreatePageHasAllTheCorrectElements()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioCreate());

            $browser->within(new PortfolioDetails(), function ($browser) {
                $browser->assertEnabled('@name');
                $browser->assertEnabled('@type');
                $browser->assertEnabled('@colour');
            });

            $browser->screenshot('PortfolioCreateSet1Test.checkPortfolioCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check portfolio create page form works correctly.
     *
     * @test checkPortfolioCreatePageFormWorksCorrectly
     * @group portfolio.create
     */
    public function checkPortfolioCreatePageFormWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->make();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioCreate());

            $browser->within(new PortfolioDetails(), function ($browser) use ($portfolio) {
                $browser->type('@name', $portfolio->name);
                $browser->select('@type', $portfolio->type);
                $browser->select('@colour', $portfolio->colour);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Portfolio created');
            });

            $browser->screenshot('PortfolioCreateSet1Test.checkPortfolioCreatePageFormWorksCorrectly');
        });

        $this->assertDatabaseHas('portfolios', [
            'user_id' => $this->adminUser->id,
            'name' => $portfolio->name,
        ]);
    }

    /**
     * Check portfolio create page form returns error if missing data.
     *
     * @test checkPortfolioCreatePageFormReturnsErrorIfMissingData
     * @group portfolio.create
     */
    public function checkPortfolioCreatePageFormReturnsErrorIfMissingData()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioCreate());

            $browser->within(new PortfolioDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
            });

            $browser->screenshot('PortfolioCreateSet1Test.checkPortfolioCreatePageFormReturnsErrorIfMissingData');
        });
    }

    /**
     * Check portfolio create page back link works.
     *
     * @test checkPortfolioCreatePageBackLinkWorks
     * @group portfolio.create
     */
    public function checkPortfolioCreatePageBackLinkWorks()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioCreate());

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('portfolio.overview');
            $browser->screenshot('PortfolioCreateSet1Test.checkPortfolioCreatePageBackLinkWorks');
        });
    }
}
