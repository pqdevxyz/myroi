<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Transaction\TransactionDetails;
use Tests\Browser\Pages\Transaction\TransactionCreate;
use Tests\DuskTestCase;

class TransactionCreateSet1Test extends DuskTestCase
{
    /**
     * Check transaction create page has all the correct elements.
     *
     * @test checkTransactionCreatePageHasAllTheCorrectElements
     * @group transaction.create
     */
    public function checkTransactionCreatePageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionCreate($symbol->id));

            $browser->within(new TransactionDetails(), function ($browser) {
                $browser->assertValue('@quantity', 0);
                $browser->assertValue('@unit-price', 0);
                $browser->assertValue('@charges', 0);
                $browser->assertValue('@tax', 0);
                $browser->assertValue('@adjustment', 0);
            });

            $browser->screenshot('TransactionCreateSet1Test.checkTransactionCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check transaction create page works correctly.
     *
     * @test checkTransactionCreatePageWorksCorrectly
     * @group transaction.create
     */
    public function checkTransactionCreatePageWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->make();

        $this->browse(function (Browser $browser) use ($symbol, $transaction) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionCreate($symbol->id));

            $browser->within(new TransactionDetails(), function ($browser) use ($transaction) {
                $browser->type('@quantity', $transaction->quantity);
                $browser->type('@unit-price', $transaction->buy_price);
                $browser->type('@charges', $transaction->charges);
                $browser->type('@tax', $transaction->tax);
                $browser->script(["document.querySelector('input[name=buy_date]').value = '" . $transaction->buy_date->format('Y-m-d') . "'"]);
                $browser->type('@adjustment', $transaction->adjustment);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Transaction created');
            });

            $browser->screenshot('TransactionCreateSet1Test.checkTransactionCreatePageWorksCorrectly');
        });
    }

    /**
     * Check transaction create page returns error if missing data.
     *
     * @test checkTransactionCreatePageReturnsErrorIfMissingData
     * @group transaction.create
     */
    public function checkTransactionCreatePageReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionCreate($symbol->id));

            $browser->within(new TransactionDetails(), function ($browser) {
                $browser->type('@quantity', '');
                $browser->type('@unit-price', '');
                $browser->type('@charges', '');
                $browser->type('@tax', '');
                $browser->type('@adjustment', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The quantity field is required');
                $browser->assertSeeIn('@list', 'The buy price field is required');
                $browser->assertSeeIn('@list', 'The charges field is required');
                $browser->assertSeeIn('@list', 'The tax field is required');
                $browser->assertSeeIn('@list', 'The adjustment field is required');
            });

            $browser->screenshot('TransactionCreateSet1Test.checkTransactionCreatePageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check transaction create page back link works.
     *
     * @test checkTransactionCreatePageBackLinkWorks
     * @group transaction.create
     */
    public function checkTransactionCreatePageBackLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionCreate($symbol->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('symbol.view.open', ['id' => $symbol->id]);
            $browser->screenshot('TransactionCreateSet1Test.checkTransactionCreatePageBackLinkWorks');
        });
    }
}
