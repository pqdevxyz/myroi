<?php

namespace Tests\Browser;

use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\HistoricPrice\PriceDetails;
use Tests\Browser\Pages\HistoricPrice\HistoricPriceCreate;
use Tests\DuskTestCase;

class HistoricPriceCreateSet1Test extends DuskTestCase
{
    /**
     * Check historic price create page has all the correct elements.
     *
     * @test checkHistoricPriceCreatePageHasAllTheCorrectElements
     * @group historic-price.create
     */
    public function checkHistoricPriceCreatePageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceCreate($symbol->id));

            $browser->within(new PriceDetails(), function ($browser) {
                $browser->assertValue('@unit-price', 0);
            });

            $browser->screenshot('HistoricPriceCreateSet1Test.checkHistoricPriceCreatePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check historic price create page works correctly.
     *
     * @test checkHistoricPriceCreatePageWorksCorrectly
     * @group historic-price.create
     */
    public function checkHistoricPriceCreatePageWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->make();

        $this->browse(function (Browser $browser) use ($symbol, $historicPrice) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceCreate($symbol->id));

            $browser->within(new PriceDetails(), function ($browser) use ($historicPrice) {
                $browser->type('@unit-price', $historicPrice->close_price);
                $browser->script([
                    "document.querySelector('input[name=date]').value = '" . $historicPrice->date->format('Y-m-d') . "'",
                ]);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Historic Price created');
            });

            $browser->screenshot('HistoricPriceCreateSet1Test.checkHistoricPriceCreatePageWorksCorrectly');
        });
    }

    /**
     * Check historic price create page returns error if missing data.
     *
     * @test checkHistoricPriceCreatePageReturnsErrorIfMissingData
     * @group historic-price.create
     */
    public function checkHistoricPriceCreatePageReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceCreate($symbol->id));

            $browser->within(new PriceDetails(), function ($browser) {
                $browser->type('@unit-price', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The close price field is required');
            });

            $browser->screenshot('HistoricPriceCreateSet1Test.checkHistoricPriceCreatePageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check historic price create page back link works.
     *
     * @test checkHistoricPriceCreatePageBackLinkWorks
     * @group historic-price.create
     */
    public function checkHistoricPriceCreatePageBackLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceCreate($symbol->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('symbol.view.open', ['id' => $symbol->id]);
            $browser->screenshot('HistoricPriceCreateSet1Test.checkHistoricPriceCreatePageBackLinkWorks');
        });
    }
}
