<?php

namespace Tests\Browser\Pages\HistoricPrice;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class HistoricPriceCreate extends Page
{
    public int $symbolId;

    public function __construct(int $symbolId)
    {
        $this->symbolId = $symbolId;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('historic-price.create', ['symbolId' => $this->symbolId], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('historic-price.create', ['symbolId' => $this->symbolId]);
        $browser->assertTitle('Add Historic Price | My ROI');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@back-button' => '#back-button',
        ];
    }
}
