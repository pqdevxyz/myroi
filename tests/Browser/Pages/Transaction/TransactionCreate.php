<?php

namespace Tests\Browser\Pages\Transaction;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class TransactionCreate extends Page
{
    public int $symbolId;

    public function __construct(int $symbolId)
    {
        $this->symbolId = $symbolId;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('transaction.create', ['symbolId' => $this->symbolId], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('transaction.create', ['symbolId' => $this->symbolId]);
        $browser->assertTitle('Add Transaction | My ROI');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@back-button' => '#back-button',
        ];
    }
}
