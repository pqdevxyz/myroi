<?php

namespace Tests\Browser\Pages\Symbol;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class SymbolView extends Page
{
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('symbol.view.open', ['id' => $this->id], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('symbol.view.open', ['id' => $this->id]);
        $browser->assertTitle('View Symbol | My ROI');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [];
    }
}
