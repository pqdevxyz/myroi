<?php

namespace Tests\Browser\Pages\Portfolio;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class PortfolioCreate extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('portfolio.create', [], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('portfolio.create');
        $browser->assertTitle('Create Portfolio | My ROI');
        $browser->assertVisible('@name');
        $browser->assertVisible('@type');
        $browser->assertVisible('@colour');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@type' => 'select[name=type]',
            '@colour' => 'select[name=colour]',
            '@save-button' => 'button[type=submit]',
            '@back-button' => '#back-button',
        ];
    }
}
