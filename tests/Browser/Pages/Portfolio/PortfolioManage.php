<?php

namespace Tests\Browser\Pages\Portfolio;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class PortfolioManage extends Page
{
    public int $portfolioId;

    public function __construct(int $portfolioId)
    {
        $this->portfolioId = $portfolioId;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('portfolio.manage', ['id' => $this->portfolioId], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('portfolio.manage', ['id' => $this->portfolioId]);
        $browser->assertTitle('Manage Portfolio | My ROI');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@add-symbol-button' => '#portfolio-add-symbol-button',
            '@back-button' => '#back-button',
        ];
    }
}
