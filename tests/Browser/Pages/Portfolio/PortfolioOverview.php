<?php

namespace Tests\Browser\Pages\Portfolio;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class PortfolioOverview extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route('portfolio.overview', [], false);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $browser->assertRouteIs('portfolio.overview');
        $browser->assertTitle('Portfolios | My ROI');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [];
    }
}
