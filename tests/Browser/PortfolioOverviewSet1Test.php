<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Support\Collection;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Navbar;
use Tests\Browser\Components\Portfolio\PortfolioCard;
use Tests\Browser\Components\Portfolio\SymbolCard;
use Tests\Browser\Pages\Portfolio\PortfolioOverview;
use Tests\DuskTestCase;

class PortfolioOverviewSet1Test extends DuskTestCase
{
    /**
     * Check portfolio overview page has all the correct elements.
     *
     * @test checkPortfolioOverviewPageHasAllTheCorrectElements
     * @group portfolio.overview
     */
    public function checkPortfolioOverviewPageHasAllTheCorrectElements()
    {
        /** @var Collection $portfolios */
        $portfolios = Portfolio::where('user_id', $this->adminUser->id)->get();
        /** @var Collection $symbols */
        $symbols = Symbol::whereIn('portfolio_id', $portfolios->pluck('id')->toArray())->get();

        $this->browse(function (Browser $browser) use ($portfolios, $symbols) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioOverview());
            $browser->screenshot('PortfolioOverviewSet1Test.checkPortfolioOverviewPageHasAllTheCorrectElements');
            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Portfolio');
            });

            //For each portfolio
            foreach ($portfolios as $portfolioKey => $portfolio) {
                //Check the total is displayed
                $browser->within(new PortfolioCard($portfolioKey + 1), function ($browser) use ($portfolio) {
                    $browser->assertSeeIn('@title', strtoupper($portfolio->name));
                    $browser->assertSeeIn('@price', number_format($portfolio->total_cost, 2));
                });
            }

            //For each open symbol
            $openSymbols = $symbols->where('is_closed', 0)->values();
            foreach ($openSymbols as $symbolKey => $symbol) {
                //Check content is displayed
                $browser->within(new SymbolCard($symbolKey + 1, false), function ($browser) use ($symbol) {
                    $browser->assertSeeIn('@subtitle', strtoupper($symbol->ticker));
                    $browser->assertSeeIn('@title', strtoupper($symbol->name));
                    $browser->assertSeeIn('@price', number_format($symbol->total_cost, 2));
                });
            }

            //For each closed symbol
            $closedSymbols = $symbols->where('is_closed', 1)->values();
            foreach ($closedSymbols as $symbolKey => $symbol) {
                //Check content is displayed
                $browser->within(new SymbolCard($symbolKey + 1, true), function ($browser) use ($symbol) {
                    $browser->assertSeeIn('@subtitle', strtoupper($symbol->ticker));
                    $browser->assertSeeIn('@title', strtoupper($symbol->name));
                    $browser->assertSeeIn('@price', number_format($symbol->total_profit, 2));
                });
            }
        });
    }

    /**
     * Check portfolio overview page has link to create new portfolio.
     *
     * @test checkPortfolioOverviewPageHasLinkToCreateNewPortfolio
     * @group portfolio.overview
     */
    public function checkPortfolioOverviewPageHasLinkToCreateNewPortfolio()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioOverview());
            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Portfolio');
                $browser->clickLink('Add Portfolio');
            });
            $browser->assertRouteIs('portfolio.create');
            $browser->screenshot('PortfolioOverviewSet1Test.checkPortfolioOverviewPageHasLinkToCreateNewPortfolio');
        });
    }

    /**
     * Check portfolio overview page has link to manage a portfolio.
     *
     * @test checkPortfolioOverviewPageHasLinkToManageAPortfolio
     * @group portfolio.overview
     */
    public function checkPortfolioOverviewPageHasLinkToManageAPortfolio()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioOverview());
            $browser->within(new PortfolioCard(1), function ($browser) {
                $browser->click('@title');
            });
            $browser->assertRouteIs('portfolio.manage', ['id' => $portfolio->id]);
            $browser->screenshot('PortfolioOverviewSet1Test.checkPortfolioOverviewPageHasLinkToManageAPortfolio');
        });
    }

    /**
     * Check portfolio overview page has link to view symbol.
     *
     * @test checkPortfolioOverviewPageHasLinkToViewSymbol
     * @group portfolio.overview
     */
    public function checkPortfolioOverviewPageHasLinkToViewSymbol()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        /** @var Symbol $openSymbol */
        $openSymbol = Symbol::where('portfolio_id', $portfolio->id)->where('is_closed', 0)->first();
        $closedSymbol = Symbol::where('portfolio_id', $portfolio->id)->where('is_closed', 1)->first();

        $this->browse(function (Browser $browser) use ($openSymbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioOverview());
            $browser->within(new SymbolCard(1, false), function ($browser) {
                $browser->click('@title');
            });
            $browser->assertRouteIs('symbol.view.open', ['id' => $openSymbol->id]);
            $browser->screenshot('PortfolioOverviewSet1Test.checkPortfolioOverviewPageHasLinkToViewOpenSymbol');
        });

        $this->browse(function (Browser $browser) use ($closedSymbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioOverview());
            $browser->within(new SymbolCard(1, true), function ($browser) {
                $browser->click('@title');
            });
            $browser->assertRouteIs('symbol.view.closed', ['id' => $closedSymbol->id]);
            $browser->screenshot('PortfolioOverviewSet1Test.checkPortfolioOverviewPageHasLinkToViewClosedSymbol');
        });
    }
}
