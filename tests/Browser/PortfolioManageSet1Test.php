<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Portfolio\PortfolioDelete;
use Tests\Browser\Components\Portfolio\PortfolioDetails;
use Tests\Browser\Components\Portfolio\PortfolioSymbol;
use Tests\Browser\Components\Portfolio\PortfolioSymbolNew;
use Tests\Browser\Pages\Portfolio\PortfolioManage;
use Tests\DuskTestCase;

class PortfolioManageSet1Test extends DuskTestCase
{
    /**
     * Check portfolio manage page has all the correct elements.
     *
     * @test checkPortfolioManagePageHasAllTheCorrectElements
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioDetails(), function ($browser) use ($portfolio) {
                $browser->assertValue('@name', $portfolio->name);
                $browser->assertValue('@type', $portfolio->type);
                $browser->assertValue('@colour', $portfolio->colour);
            });

            foreach ($portfolio->symbols as $key => $symbol) {
                $browser->within(new PortfolioSymbol($key + 1), function ($browser) use ($symbol) {
                    $browser->assertSeeIn('@title', $symbol->name);
                    $browser->click('@dropdown');
                    $browser->assertVisible('@form-name');
                    $browser->assertVisible('@form-ticker');
                    $browser->assertVisible('@form-index');
                    $browser->assertValue('@form-name', $symbol->name);
                    $browser->assertValue('@form-ticker', $symbol->ticker);
                    $browser->assertValue('@form-index', $symbol->index);
                });
            }

            $browser->within(new PortfolioDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Portfolio');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageHasAllTheCorrectElements');
        });
    }

    /**
     * Check portfolio manage page edit form works correctly.
     *
     * @test checkPortfolioManagePageEditFormWorksCorrectly
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageEditFormWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        /** @var Portfolio $newPortfolio */
        $newPortfolio = Portfolio::factory()->make();

        $this->browse(function (Browser $browser) use ($portfolio, $newPortfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioDetails(), function ($browser) use ($newPortfolio) {
                $browser->type('@name', $newPortfolio->name);
                $browser->select('@type', $newPortfolio->type);
                $browser->select('@colour', $newPortfolio->colour);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Portfolio updated');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageEditFormWorksCorrectly');
        });

        $this->assertDatabaseHas('portfolios', [
            'user_id' => $this->adminUser->id,
            'name' => $newPortfolio->name,
        ]);
    }

    /**
     * Check portfolio manage page edit form returns error if missing data.
     *
     * @test checkPortfolioManagePageEditFormReturnsErrorIfMissingData
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageEditFormReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioDetails(), function ($browser) {
                $browser->type('@name', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageEditFormReturnsErrorIfMissingData');
        });
    }

    /**
     * Check portfolio manage page add symbol form works correctly.
     *
     * @test checkPortfolioManagePageAddSymbolFormWorksCorrectly
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageAddSymbolFormWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        /** @var Symbol $newSymbol */
        $newSymbol = Symbol::factory()->make();

        $this->browse(function (Browser $browser) use ($portfolio, $newSymbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->assertSeeIn('@add-symbol-button', 'Add');
            $browser->click('@add-symbol-button');

            $browser->within(new PortfolioSymbolNew(), function ($browser) use ($newSymbol) {
                $browser->assertVisible('@form-name');
                $browser->assertVisible('@form-ticker');
                $browser->assertVisible('@form-index');
                $browser->type('@form-name', $newSymbol->name);
                $browser->type('@form-ticker', $newSymbol->ticker);
                $browser->type('@form-index', $newSymbol->index);
                $browser->click('@form-save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Symbol created');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageAddSymbolFormWorksCorrectly');
        });
    }

    /**
     * Check portfolio manage page add symbol form returns error if missing data.
     *
     * @test checkPortfolioManagePageAddSymbolFormReturnsErrorIfMissingData
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageAddSymbolFormReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->assertSeeIn('@add-symbol-button', 'Add');
            $browser->click('@add-symbol-button');

            $browser->within(new PortfolioSymbolNew(), function ($browser) {
                $browser->type('@form-name', '');
                $browser->type('@form-ticker', '');
                $browser->type('@form-index', '');
                $browser->click('@form-save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
                $browser->assertSeeIn('@list', 'The ticker field is required');
                $browser->assertSeeIn('@list', 'The index field is required');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageAddSymbolFormReturnsErrorIfMissingData');
        });
    }

    /**
     * Check portfolio manage page back link works.
     *
     * @test checkPortfolioManagePageBackLinkWorks
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageBackLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('portfolio.overview');
            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageBackLinkWorks');
        });
    }

    /**
     * Check portfolio manage page edit symbol works correctly.
     *
     * @test checkPortfolioManagePageEditSymbolWorksCorrectly
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageEditSymbolWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        /** @var Symbol $newSymbol */
        $newSymbol = Symbol::factory()->make();

        $this->browse(function (Browser $browser) use ($portfolio, $newSymbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioSymbol(1), function ($browser) use ($newSymbol) {
                $browser->click('@dropdown');
                $browser->assertVisible('@form-name');
                $browser->assertVisible('@form-ticker');
                $browser->assertVisible('@form-index');
                $browser->type('@form-name', $newSymbol->name);
                $browser->type('@form-ticker', $newSymbol->ticker);
                $browser->type('@form-index', $newSymbol->index);
                $browser->click('@form-save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Symbol updated');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageEditSymbolWorksCorrectly');
        });
    }

    /**
     * Check portfolio manage page edit symbol returns error if missing data.
     *
     * @test checkPortfolioManagePageEditSymbolReturnsErrorIfMissingData
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageEditSymbolReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioSymbol(1), function ($browser) {
                $browser->click('@dropdown');
                $browser->assertVisible('@form-name');
                $browser->type('@form-name', '');
                $browser->click('@form-save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The name field is required');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageEditSymbolReturnsErrorIfMissingData');
        });
    }

    /**
     * Check portfolio manage page delete symbol link works.
     *
     * @test checkPortfolioManagePageDeleteSymbolLinkWorks
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageDeleteSymbolLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioSymbol(1), function ($browser) {
                $browser->click('@dropdown');
                $browser->click('@form-delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Symbol deleted');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageDeleteSymbolLinkWorks');
        });

        $this->assertSoftDeleted('symbols', [
            'id' => $symbol->id,
        ]);
    }

    /**
     * Check portfolio manage page delete portfolio link works.
     *
     * @test checkPortfolioManagePageDeletePortfolioLinkWorks
     * @group portfolio.manage
     */
    public function checkPortfolioManagePageDeletePortfolioLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();

        $this->browse(function (Browser $browser) use ($portfolio) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new PortfolioManage($portfolio->id));

            $browser->within(new PortfolioDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Portfolio');
                $browser->click('@delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Portfolio deleted');
            });

            $browser->screenshot('PortfolioManageSet1Test.checkPortfolioManagePageDeletePortfolioLinkWorks');
        });

        $this->assertSoftDeleted('portfolios', [
            'id' => $portfolio->id,
        ]);
    }
}
