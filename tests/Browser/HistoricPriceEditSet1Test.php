<?php

namespace Tests\Browser;

use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\HistoricPrice\PriceDelete;
use Tests\Browser\Components\HistoricPrice\PriceDetails;
use Tests\Browser\Pages\HistoricPrice\HistoricPriceEdit;
use Tests\DuskTestCase;

class HistoricPriceEditSet1Test extends DuskTestCase
{
    /**
     * Check historic price edit page has all the correct elements.
     *
     * @test checkHistoricPriceEditPageHasAllTheCorrectElements
     * @group historic-price.edit
     */
    public function checkHistoricPriceEditPageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $historicPrice = HistoricPrice::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($historicPrice) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceEdit($historicPrice->id));

            $browser->within(new PriceDetails(), function ($browser) use ($historicPrice) {
                $browser->assertValue('@unit-price', $historicPrice->close_price);
                $browser->assertValue('@date', $historicPrice->date->format('Y-m-d'));
            });

            $browser->screenshot('HistoricPriceEditSet1Test.checkHistoricPriceEditPageHasAllTheCorrectElements');
        });
    }

    /**
     * Check historic price edit page works correctly.
     *
     * @test checkHistoricPriceEditPageWorksCorrectly
     * @group historic-price.edit
     */
    public function checkHistoricPriceEditPageWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $historicPrice = HistoricPrice::where('symbol_id', $symbol->id)->first();
        /** @var HistoricPrice $newHistoricPrice */
        $newHistoricPrice = HistoricPrice::factory()->make();

        $this->browse(function (Browser $browser) use ($historicPrice, $newHistoricPrice) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceEdit($historicPrice->id));

            $browser->within(new PriceDetails(), function ($browser) use ($newHistoricPrice) {
                $browser->type('@unit-price', $newHistoricPrice->close_price);
                $browser->script([
                    "document.querySelector('input[name=date]').value = '" . $newHistoricPrice->date->format('Y-m-d') . "'",
                ]);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Historic Price updated');
            });

            $browser->screenshot('HistoricPriceEditSet1Test.checkHistoricPriceEditPageWorksCorrectly');
        });
    }

    /**
     * Check historic price edit page returns error if missing data.
     *
     * @test checkHistoricPriceEditPageReturnsErrorIfMissingData
     * @group historic-price.edit
     */
    public function checkHistoricPriceEditPageReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $historicPrice = HistoricPrice::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($historicPrice) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceEdit($historicPrice->id));

            $browser->within(new PriceDetails(), function ($browser) {
                $browser->type('@unit-price', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The close price field is required');
            });

            $browser->screenshot('HistoricPriceEditSet1Test.checkHistoricPriceEditPageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check historic price edit page back link works.
     *
     * @test checkHistoricPriceEditPageBackLinkWorks
     * @group historic-price.edit
     */
    public function checkHistoricPriceEditPageBackLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $historicPrice = HistoricPrice::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($historicPrice, $symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceEdit($historicPrice->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('symbol.view.open', ['id' => $symbol->id]);

            $browser->screenshot('HistoricPriceEditSet1Test.checkHistoricPriceEditPageBackLinkWorks');
        });
    }

    /**
     * Check historic price edit page delete historic price link works.
     *
     * @test checkHistoricPriceEditPageDeleteHistoricPriceLinkWorks
     * @group historic-price.edit
     */
    public function checkHistoricPriceEditPageDeleteHistoricPriceLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $historicPrice = HistoricPrice::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($historicPrice) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new HistoricPriceEdit($historicPrice->id));

            $browser->within(new PriceDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Historic Price');
                $browser->click('@delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Historic Price deleted');
            });

            $browser->screenshot('HistoricPriceEditSet1Test.checkHistoricPriceEditPageDeleteHistoricPriceLinkWorks');
        });
        $this->assertDatabaseMissing('historic_prices', [
            'id' => $historicPrice->id,
        ]);
    }
}
