<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Navbar;
use Tests\Browser\Components\Symbol\HistoricPricesTable;
use Tests\Browser\Components\Symbol\SimpleCard;
use Tests\Browser\Components\Symbol\TransactionCard;
use Tests\Browser\Components\Symbol\TransactionsTable;
use Tests\Browser\Pages\Symbol\SymbolView;
use Tests\DuskTestCase;

class SymbolViewSet1Test extends DuskTestCase
{
    /**
     * Check view symbol page has all the correct elements.
     *
     * @test checkViewSymbolPageHasAllTheCorrectElements
     * @group symbol.view.open
     */
    public function checkViewSymbolPageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new SymbolView($symbol->id));

            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Historic Price');
                $browser->assertSeeIn('@other-links', 'Add Transaction');
            });

            $browser->within(new SimpleCard(1), function ($browser) use ($symbol) {
                $browser->assertSeeIn('@title', strtoupper('Net ROI'));
                $browser->assertSeeIn('@subtitle', number_format($symbol->net_roi_percentage, 2));
            });

            $browser->within(new SimpleCard(2), function ($browser) use ($symbol) {
                $browser->assertSeeIn('@title', strtoupper('Total Sale'));
                $browser->assertSeeIn('@subtitle', number_format($symbol->total_sale, 2));
            });

            $browser->within(new SimpleCard(3), function ($browser) use ($symbol) {
                $browser->assertSeeIn('@title', strtoupper('Latest Pricing'));
                $browser->assertSeeIn('@subtitle', number_format($symbol->latest_price->close_price, 2));
            });

            $browser->within(new SimpleCard(4), function ($browser) use ($symbol) {
                $browser->assertSeeIn('@title', strtoupper('Latest Pricing Date'));
                $browser->assertSeeIn('@subtitle', $symbol->latest_price->date->format('jS M Y'));
            });

            $browser->within(new SimpleCard(5), function ($browser) use ($symbol) {
                $browser->assertSeeIn('@title', strtoupper('Next Pricing Date'));
                $browser->assertSeeIn('@subtitle', $symbol->next_price_date->format('jS M Y'));
            });

            foreach ($symbol->transactions->take(4) as $key => $transaction) {
                $browser->within(new TransactionCard($key + 1), function ($browser) use ($transaction) {
                    $browser->assertSeeIn('@total', number_format($transaction->total_cost, 2));
                    $browser->assertSeeIn('@unit', number_format($transaction->buy_price, 2));
                    $browser->assertSeeIn('@quantity', number_format($transaction->quantity, 2));
                    $browser->assertSeeIn('@date', $transaction->buy_date->format('jS M Y'));
                });
            }

            $browser->within(new TransactionsTable(), function ($browser) {
                $browser->assertSeeIn('@header', strtoupper('Date'));
                $browser->assertSeeIn('@header', strtoupper('Quantity'));
                $browser->assertSeeIn('@header', strtoupper('Unit Price'));
                $browser->assertSeeIn('@header', strtoupper('Total Cost'));
            });

            $browser->within(new HistoricPricesTable(), function ($browser) {
                $browser->assertSeeIn('@header', strtoupper('Date'));
                $browser->assertSeeIn('@header', strtoupper('Price'));
                $browser->assertSeeIn('@header', strtoupper('Roi'));
            });
            $browser->screenshot('SymbolViewSet1Test.checkViewSymbolPageHasAllTheCorrectElements');
        });
    }

    /**
     * Check view symbol page add transaction link works.
     *
     * @test checkViewSymbolPageAddTransactionLinkWorks
     * @group symbol.view.open
     */
    public function checkViewSymbolPageAddTransactionLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new SymbolView($symbol->id));

            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Transaction');
                $browser->clickLink('Add Transaction');
            });

            $browser->assertRouteIs('transaction.create', ['symbolId' => $symbol->id]);

            $browser->screenshot('SymbolViewSet1Test.checkViewSymbolPageAddTransactionLinkWorks');
        });
    }

    /**
     * Check view symbol page add historic price link works.
     *
     * @test checkViewSymbolPageAddHistoricPriceLinkWorks
     * @group symbol.view.open
     */
    public function checkViewSymbolPageAddHistoricPriceLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();

        $this->browse(function (Browser $browser) use ($symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new SymbolView($symbol->id));

            $browser->within(new Navbar(), function ($browser) {
                $browser->assertSeeIn('@other-links', 'Add Historic Price');
                $browser->clickLink('Add Historic Price');
            });

            $browser->assertRouteIs('historic-price.create', ['symbolId' => $symbol->id]);

            $browser->screenshot('SymbolViewSet1Test.checkViewSymbolPageAddHistoricPriceLinkWorks');
        });
    }
}
