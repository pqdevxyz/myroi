<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class SymbolCard extends BaseComponent
{
    public int $symbolNumber;
    public bool $isClosed;

    public function __construct(int $symbolNumber, bool $isClosed)
    {
        $this->symbolNumber = $symbolNumber;
        $this->isClosed = $isClosed;
    }

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        if ($this->isClosed) {
            return '#symbol-closed-cards .symbol-closed-card:nth-of-type(' . $this->symbolNumber . ')';
        }

        return '#symbol-cards-new .symbol-card-new:nth-of-type(' . $this->symbolNumber . ')';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@title' => '.small-card-title',
            '@subtitle' => '.small-card-subtitle',
            '@price' => '.small-card-price',
        ];
    }
}
