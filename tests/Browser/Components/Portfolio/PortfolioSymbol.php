<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PortfolioSymbol extends BaseComponent
{
    public int $symbolNumber;

    public function __construct(int $symbolNumber)
    {
        $this->symbolNumber = $symbolNumber;
    }

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#portfolio-symbols .portfolio-symbol-outer .portfolio-symbol:nth-of-type(' . $this->symbolNumber . ')';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@dropdown' => '.symbol-accordion',
            '@title' => '.symbol-accordion p',
            '@form-name' => '.symbol-accordion-form input[name=name]',
            '@form-ticker' => '.symbol-accordion-form input[name=ticker]',
            '@form-index' => '.symbol-accordion-form input[name=index]',
            '@form-save-button' => '.symbol-accordion-form button[type=submit]',
            '@form-delete-button' => '.symbol-accordion-form a',
        ];
    }
}
