<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PortfolioDetails extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#portfolio-details';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@name');
        $browser->assertVisible('@type');
        $browser->assertVisible('@colour');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@type' => 'select[name=type]',
            '@colour' => 'select[name=colour]',
            '@save-button' => 'button[type=submit]',
        ];
    }
}
