<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PortfolioCard extends BaseComponent
{
    public int $number;

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#portfolio-cards-new .portfolio-card-new:nth-of-type(' . $this->number . ')';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@title' => '.mirco-card-title',
            '@price' => '.mirco-card-price',
        ];
    }
}
