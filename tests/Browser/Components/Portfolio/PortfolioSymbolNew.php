<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PortfolioSymbolNew extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#portfolio-symbol-new';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@form-name' => 'input[name=name]',
            '@form-ticker' => 'input[name=ticker]',
            '@form-index' => 'input[name=index]',
            '@form-save-button' => 'button[type=submit]',
        ];
    }
}
