<?php

namespace Tests\Browser\Components\Portfolio;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PortfolioDelete extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#portfolio-delete';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@delete-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@delete-button' => '#portfolio-delete-button',
        ];
    }
}
