<?php

namespace Tests\Browser\Components\HistoricPrice;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PriceDetails extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#price-details';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@unit-price');
        $browser->assertVisible('@date');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@unit-price' => 'input[name=close_price]',
            '@date' => 'input[name=date]',
            '@save-button' => 'button[type=submit]',
        ];
    }
}
