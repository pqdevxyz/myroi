<?php

namespace Tests\Browser\Components\HistoricPrice;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class PriceDelete extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#price-delete';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@delete-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@delete-button' => '#price-delete-button',
        ];
    }
}
