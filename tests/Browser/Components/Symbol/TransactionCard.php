<?php

namespace Tests\Browser\Components\Symbol;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class TransactionCard extends BaseComponent
{
    public int $number;

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#transaction-cards .transaction-card:nth-of-type(' . $this->number . ')';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@total' => '.mini-card-total',
            '@unit' => '.mini-card-unit',
            '@date' => '.mini-card-date',
            '@quantity' => '.mini-card-quantity',
        ];
    }
}
