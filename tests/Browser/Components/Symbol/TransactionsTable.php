<?php

namespace Tests\Browser\Components\Symbol;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class TransactionsTable extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#transaction-table';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@header');
        $browser->assertVisible('@body');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@header' => 'thead > tr',
            '@body' => 'tbody > tr',
        ];
    }
}
