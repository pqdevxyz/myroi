<?php

namespace Tests\Browser\Components\Transaction;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class TransactionDetails extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#transaction-details';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
        $browser->assertVisible('@quantity');
        $browser->assertVisible('@unit-price');
        $browser->assertVisible('@charges');
        $browser->assertVisible('@tax');
        $browser->assertVisible('@transaction-date');
        $browser->assertVisible('@adjustment');
        $browser->assertVisible('@save-button');
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@quantity' => 'input[name=quantity]',
            '@unit-price' => 'input[name=buy_price]',
            '@charges' => 'input[name=charges]',
            '@tax' => 'input[name=tax]',
            '@transaction-date' => 'input[name=buy_date]',
            '@adjustment' => 'input[name=adjustment]',
            '@save-button' => 'button[type=submit]',
        ];
    }
}
