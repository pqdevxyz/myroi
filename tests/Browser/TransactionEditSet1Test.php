<?php

namespace Tests\Browser;

use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alerts\AlertError;
use Tests\Browser\Components\Alerts\AlertSuccess;
use Tests\Browser\Components\Transaction\TransactionDelete;
use Tests\Browser\Components\Transaction\TransactionDetails;
use Tests\Browser\Pages\Transaction\TransactionEdit;
use Tests\DuskTestCase;

class TransactionEditSet1Test extends DuskTestCase
{
    /**
     * Check transaction edit page has all the correct elements.
     *
     * @test checkTransactionEditPageHasAllTheCorrectElements
     * @group transaction.edit
     */
    public function checkTransactionEditPageHasAllTheCorrectElements()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $transaction = Transaction::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($transaction) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionEdit($transaction->id));

            $browser->within(new TransactionDetails(), function ($browser) use ($transaction) {
                $browser->assertValue('@quantity', $transaction->quantity);
                $browser->assertValue('@unit-price', $transaction->buy_price);
                $browser->assertValue('@charges', $transaction->charges);
                $browser->assertValue('@tax', $transaction->tax);
                $browser->assertValue('@transaction-date', $transaction->buy_date->format('Y-m-d'));
                $browser->assertValue('@adjustment', $transaction->adjustment);
            });

            $browser->screenshot('TransactionEditSet1Test.checkTransactionEditPageHasAllTheCorrectElements');
        });
    }

    /**
     * Check transaction edit page works correctly.
     *
     * @test checkTransactionEditPageWorksCorrectly
     * @group transaction.edit
     */
    public function checkTransactionEditPageWorksCorrectly()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $transaction = Transaction::where('symbol_id', $symbol->id)->first();
        /** @var Transaction $newTransaction */
        $newTransaction = Transaction::factory()->make();

        $this->browse(function (Browser $browser) use ($transaction, $newTransaction) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionEdit($transaction->id));

            $browser->within(new TransactionDetails(), function ($browser) use ($newTransaction) {
                $browser->type('@quantity', $newTransaction->quantity);
                $browser->type('@unit-price', $newTransaction->buy_price);
                $browser->type('@charges', $newTransaction->charges);
                $browser->type('@tax', $newTransaction->tax);
                $browser->script(["document.querySelector('input[name=buy_date]').value = '" . $newTransaction->buy_date->format('Y-m-d') . "'"]);
                $browser->type('@adjustment', $newTransaction->adjustment);
                $browser->click('@save-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Transaction updated');
            });

            $browser->screenshot('TransactionEditSet1Test.checkTransactionEditPageWorksCorrectly');
        });
    }

    /**
     * Check transaction edit page returns error if missing data.
     *
     * @test checkTransactionEditPageReturnsErrorIfMissingData
     * @group transaction.edit
     */
    public function checkTransactionEditPageReturnsErrorIfMissingData()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $transaction = Transaction::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($transaction) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionEdit($transaction->id));

            $browser->within(new TransactionDetails(), function ($browser) {
                $browser->type('@quantity', '');
                $browser->type('@unit-price', '');
                $browser->type('@charges', '');
                $browser->type('@tax', '');
                $browser->type('@adjustment', '');
                $browser->click('@save-button');
            });

            $browser->within(new AlertError(), function ($browser) {
                $browser->assertSeeIn('@title', 'An Error Occurred');
                $browser->assertSeeIn('@list', 'The quantity field is required');
                $browser->assertSeeIn('@list', 'The buy price field is required');
                $browser->assertSeeIn('@list', 'The charges field is required');
                $browser->assertSeeIn('@list', 'The tax field is required');
                $browser->assertSeeIn('@list', 'The adjustment field is required');
            });

            $browser->screenshot('TransactionEditSet1Test.checkTransactionEditPageReturnsErrorIfMissingData');
        });
    }

    /**
     * Check transaction edit page back link works.
     *
     * @test checkTransactionEditPageBackLinkWorks
     * @group transaction.edit
     */
    public function checkTransactionEditPageBackLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $transaction = Transaction::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($transaction, $symbol) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionEdit($transaction->id));

            $browser->assertSeeIn('@back-button', 'Back');
            $browser->click('@back-button');

            $browser->assertRouteIs('symbol.view.open', ['id' => $symbol->id]);
            $browser->screenshot('TransactionEditSet1Test.checkTransactionEditPageBackLinkWorks');
        });
    }

    /**
     * Check transaction edit page delete transaction link works.
     *
     * @test checkTransactionEditPageDeleteTransactionLinkWorks
     * @group transaction.edit
     */
    public function checkTransactionEditPageDeleteTransactionLinkWorks()
    {
        $portfolio = Portfolio::where('user_id', $this->adminUser->id)->first();
        $symbol = Symbol::where('portfolio_id', $portfolio->id)->first();
        $transaction = Transaction::where('symbol_id', $symbol->id)->first();

        $this->browse(function (Browser $browser) use ($transaction) {
            $browser->loginAs($this->adminUser);
            $browser->visit(new TransactionEdit($transaction->id));

            $browser->within(new TransactionDelete(), function ($browser) {
                $browser->assertSeeIn('@delete-button', 'Delete Transaction');
                $browser->click('@delete-button');
            });

            $browser->within(new AlertSuccess(), function ($browser) {
                $browser->assertSeeIn('@title', 'Success');
                $browser->assertSeeIn('@list', 'Transaction deleted');
            });

            $browser->screenshot('TransactionEditSet1Test.checkTransactionEditPageDeleteTransactionLinkWorks');
        });

        $this->assertSoftDeleted('transactions', [
            'id' => $transaction->id,
        ]);
    }
}
