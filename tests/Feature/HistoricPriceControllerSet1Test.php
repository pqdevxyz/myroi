<?php

namespace Tests\Feature;

use App\Helpers\PQCache;
use App\Jobs\CalculateHistoricPriceROIJob;
use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class HistoricPriceControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;
    use DataProviderHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * @test checkHistoricPriceCreatePageLoads
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:getCreateHistoricPrice
     */
    public function checkHistoricPriceCreatePageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        $response = $this->getJson(route('historic-price.create', ['symbolId' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.historic-price.create');
    }

    /**
     * @test checkHistoricPriceCreatePageReturnsNotFoundError
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:getCreateHistoricPrice
     */
    public function checkHistoricPriceCreatePageReturnsNotFoundError()
    {
        $response = $this->getJson(route('historic-price.create', ['symbolId' => 0]));
        $response->assertStatus(404);
    }

    /**
     * @test checkHistoricPriceCreatePageSavesCorrectly
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:createHistoricPrice
     */
    public function checkHistoricPriceCreatePageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->make();
        $body = [
            'close_price' => $historicPrice->close_price,
            'date' => $historicPrice->date,
        ];

        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->postJson(route('historic-price.create', ['symbolId' => $symbol->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $this->assertDatabaseHas('historic_prices', [
            'date' => $historicPrice->date->format('Y-m-d'),
            'close_price' => $historicPrice->close_price,
            'symbol_id' => $symbol->id,
        ]);
        Queue::assertPushedOn('long', CalculateHistoricPriceROIJob::class);
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkHistoricPriceCreatePageValidationWorksCorrectly
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:createHistoricPrice
     * @dataProvider twoArgsDataProvider
     */
    public function checkHistoricPriceCreatePageValidationWorksCorrectly($closePrice, $date)
    {
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->make();

        $body = [];
        $body = $closePrice ? array_merge($body, ['close_price' => $historicPrice->close_price]) : $body;
        $body = $date ? array_merge($body, ['date' => $historicPrice->date]) : $body;
        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('historic-price.create', ['symbolId' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkHistoricPriceEditPageLoads
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:getEditHistoricPrice
     */
    public function checkHistoricPriceEditPageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
        ]);

        $response = $this->getJson(route('historic-price.edit', ['id' => $historicPrice->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.historic-price.edit');
    }

    /**
     * @test checkHistoricPriceEditPageReturnsNotFoundError
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:getEditHistoricPrice
     */
    public function checkHistoricPriceEditPageReturnsNotFoundError()
    {
        $response = $this->getJson(route('historic-price.edit', ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * @test checkHistoricPriceEditPageSavesCorrectly
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:editHistoricPrice
     */
    public function checkHistoricPriceEditPageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        /** @var HistoricPrice $newHistoricPrice */
        $newHistoricPrice = HistoricPrice::factory()->make();
        $body = [
            'close_price' => $newHistoricPrice->close_price,
            'date' => $newHistoricPrice->date,
        ];

        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->postJson(route('historic-price.edit', ['id' => $historicPrice->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $data = HistoricPrice::where('symbol_id', $symbol->id)->where('date', $newHistoricPrice->date->format('Y-m-d'))->first();
        $this->assertEqualsWithDelta($data->close_price, $newHistoricPrice->close_price, 0.001);
        Queue::assertPushedOn('long', CalculateHistoricPriceROIJob::class);
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkHistoricPriceEditPageValidationWorksCorrectly
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:editHistoricPrice
     * @dataProvider twoArgsDataProvider
     */
    public function checkHistoricPriceEditPageValidationWorksCorrectly($closePrice, $date)
    {
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->make();
        $body = [];
        $body = $closePrice ? array_merge($body, ['close_price' => $historicPrice->close_price]) : $body;
        $body = $date ? array_merge($body, ['date' => $historicPrice->date]) : $body;

        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('historic-price.edit', ['id' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkHistoricPriceEditPageReturnsErrorWhenNoTransactionIsFound
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:editHistoricPrice
     */
    public function checkHistoricPriceEditPageReturnsErrorWhenNoTransactionIsFound()
    {
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->make();
        $body = [
            'close_price' => $historicPrice->close_price,
            'date' => $historicPrice->date,
        ];

        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('historic-price.edit', ['id' => 0]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => 0]));
    }

    /**
     * @test checkHistoricPriceRemoveWorksCorrectly
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:removeHistoricPrice
     */
    public function checkHistoricPriceRemoveWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->getJson(route('historic-price.remove', ['id' => $historicPrice->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $this->assertDatabaseMissing('historic_prices', [
            'date' => $historicPrice->date->format('Y-m-d'),
            'close_price' => $historicPrice->close_price,
            'symbol_id' => $symbol->id,
        ]);
        Queue::assertPushedOn('long', CalculateHistoricPriceROIJob::class);
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkHistoricPriceRemoveReturnsNotFoundError
     * @group app/Http/Controllers/HistoricPriceController
     * @group app/Http/Controllers/HistoricPriceController:removeHistoricPrice
     */
    public function checkHistoricPriceRemoveReturnsNotFoundError()
    {
        $response = $this->from(route('symbol.view.open', ['id' => 0]))->getJson(route('historic-price.remove', ['id' => 0]));
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => 0]));
    }
}
