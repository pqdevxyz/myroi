<?php

namespace Tests\Feature;

use App\Helpers\PQCache;
use App\Models\Portfolio;
use App\Models\Symbol;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class SymbolControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;
    use DataProviderHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * @test checkSymbolCreatePageSavesCorrectly
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:createSymbol
     */
    public function checkSymbolCreatePageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->make();
        $body = [
            'name' => $symbol->name,
            'ticker' => $symbol->ticker,
            'index' => $symbol->index,
        ];

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->postJson(route('symbol.create', ['portfolioId' => $portfolio->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertDatabaseHas('symbols', [
            'name' => $symbol->name,
            'portfolio_id' => $portfolio->id,
        ]);
    }

    /**
     * @test checkSymbolCreatePageValidationWorksCorrectly
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:createSymbol
     * @dataProvider threeArgsDataProvider
     */
    public function checkSymbolCreatePageValidationWorksCorrectly($name, $ticker, $index)
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->make();

        $body = [];
        $body = $name ? array_merge($body, ['name' => $symbol->name]) : $body;
        $body = $ticker ? array_merge($body, ['ticker' => $symbol->ticker]) : $body;
        $body = $index ? array_merge($body, ['index' => $symbol->index]) : $body;

        $response = $this->from(route('portfolio.manage', ['id' => 0]))->postJson(route('symbol.create', ['portfolioId' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkSymbolEditPageSavesCorrectly
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:editSymbol
     */
    public function checkSymbolEditPageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Symbol $newSymbol */
        $newSymbol = Symbol::factory()->make();
        $body = [
            'name' => $newSymbol->name,
            'ticker' => $newSymbol->ticker,
            'index' => $newSymbol->index,
        ];

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->postJson(route('symbol.edit', ['id' => $symbol->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertDatabaseHas('symbols', [
            'name' => $newSymbol->name,
            'portfolio_id' => $portfolio->id,
        ]);
    }

    /**
     * @test checkSymbolEditPageValidationWorksCorrectly
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:editSymbol
     * @dataProvider threeArgsDataProvider
     */
    public function checkSymbolEditPageValidationWorksCorrectly($name, $ticker, $index)
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->make();

        $body = [];
        $body = $name ? array_merge($body, ['name' => $symbol->name]) : $body;
        $body = $ticker ? array_merge($body, ['ticker' => $symbol->ticker]) : $body;
        $body = $index ? array_merge($body, ['index' => $symbol->index]) : $body;

        $response = $this->from(route('portfolio.manage', ['id' => 0]))->postJson(route('symbol.edit', ['id' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkSymbolEditPageReturnsErrorWhenNoSymbolIsFound
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:editSymbol
     */
    public function checkSymbolEditPageReturnsErrorWhenNoSymbolIsFound()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->make();
        $body = [
            'name' => $symbol->name,
            'ticker' => $symbol->ticker,
            'index' => $symbol->index,
        ];

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->postJson(route('symbol.edit', ['id' => 0]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
    }

    /**
     * @test checkSymbolRemoveWorksCorrectly
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:removeSymbol
     */
    public function checkSymbolRemoveWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);

        PQCache::retrieveFromCache('portfolios', $portfolio->id, 'example', fn () => true);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('symbol.remove', ['id' => $symbol->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertSoftDeleted('symbols', [
            'name' => $symbol->name,
            'portfolio_id' => $portfolio->id,
        ]);
        $this->assertNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkSymbolRemoveReturnsNotFoundError
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:removeSymbol
     */
    public function checkSymbolRemoveReturnsNotFoundError()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('symbol.remove', ['id' => 0]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
    }
}
