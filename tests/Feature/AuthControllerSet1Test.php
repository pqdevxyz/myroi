<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * @test checkLoginPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewLoginPage
     */
    public function checkLoginPageLoads()
    {
        $response = $this->getJson(route('auth.login'));
        $response->assertStatus(200);
        $response->assertViewIs('auth.login.login');
    }

    /**
     * @test checkLoginPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewLoginPage
     */
    public function checkLoginPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route('auth.login'));
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /**
     * @test checkLoginPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:login
     */
    public function checkLoginPageWorksCorrectly()
    {
        $body = [
            'email' => $this->adminUser->email,
            'password' => 'myroi',
        ];
        $response = $this->from(route('auth.login'))->postJson(route('auth.login'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.overview'));
        $this->assertAuthenticatedAs($this->adminUser);
    }

    /**
     * @test checkLoginPageDisplaysErrorWhenPasswordIsIncorrect
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:login
     */
    public function checkLoginPageDisplaysErrorWhenPasswordIsIncorrect()
    {
        $body = [
            'email' => $this->adminUser->email,
            'password' => 'password',
        ];
        $response = $this->from(route('auth.login'))->postJson(route('auth.login'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.login'));
        $this->assertFalse($this->isAuthenticated());
    }

    /**
     * @test checkLoginPageValidationWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:login
     */
    public function checkLoginPageValidationWorksCorrectly()
    {
        $body = [];
        $response = $this->from(route('auth.login'))->postJson(route('auth.login'), $body);
        $response->assertStatus(422);
        $this->assertFalse($this->isAuthenticated());

        $body = [
            'email' => $this->adminUser->email,
        ];
        $response = $this->from(route('auth.login'))->postJson(route('auth.login'), $body);
        $response->assertStatus(422);
        $this->assertFalse($this->isAuthenticated());

        $body = [
            'password' => 'password',
        ];
        $response = $this->from(route('auth.login'))->postJson(route('auth.login'), $body);
        $response->assertStatus(422);
        $this->assertFalse($this->isAuthenticated());
    }
}
