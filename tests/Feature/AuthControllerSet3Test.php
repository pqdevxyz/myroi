<?php

namespace Tests\Feature;

use App\Models\SecureToken;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet3Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;
    use DataProviderHelper;

    /**
     * @test checkSetPasswordPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageLoads()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $response = $this->getJson(route('auth.set-password') . '?token=' . $passwordReset->token);
        $response->assertStatus(200);
        $response->assertViewIs('auth.password.set-password');
    }

    /**
     * @test checkSetPasswordPageErrorsWithNoToken
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageErrorsWithNoToken()
    {
        $response = $this->getJson(route('auth.set-password'));
        $response->assertStatus(400);
    }

    /**
     * @test checkSetPasswordPageErrorsWithExpiredToken
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageErrorsWithExpiredToken()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
            'created_at' => Carbon::now()->subMinutes(120),
        ]);
        $response = $this->getJson(route('auth.set-password') . '?token=' . $passwordReset->token);
        $response->assertStatus(400);
    }

    /**
     * @test checkSetPasswordPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewSetPasswordPage
     */
    public function checkSetPasswordPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route('auth.set-password'));
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /**
     * @test checkSetPasswordPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageWorksCorrectly()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'myroi',
            'confirm-password' => 'myroi',
        ];

        $response = $this->from(route('auth.set-password'))->postJson(route('auth.set-password'), $body);

        $response->assertStatus(302);
        $response->assertRedirect(route('auth.login'));

        $user = User::find($this->adminUser->id);

        $this->assertEqualsWithDelta((int)now()->format('U'), (int)$user->password_last_changed->format('U'), 5);
        $this->assertDatabaseMissing('secure_tokens', [
            'token' => $passwordReset->token,
            'type' => 'password_reset',
        ]);
    }

    /**
     * @test checkSetPasswordPageValidationWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     * @dataProvider threeArgsDataProvider
     */
    public function checkSetPasswordPageValidationWorksCorrectly($token, $password, $confirmPassword)
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);

        $body = [];
        $body = $token ? array_merge($body, ['token' => $passwordReset->token]) : $body;
        $body = $password ? array_merge($body, ['password' => 'myroi']) : $body;
        $body = $confirmPassword ? array_merge($body, ['confirm-password' => 'myroi']) : $body;
        $response = $this->from(route('auth.set-password'))->postJson(route('auth.set-password'), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkSetPasswordPageErrorsWhenTokenHasExpired
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenTokenHasExpired()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
            'created_at' => Carbon::now()->subMinutes(120),
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'myroi',
            'confirm-password' => 'myroi',
        ];
        $response = $this->from(route('auth.set-password'))->postJson(route('auth.set-password'), $body);
        $response->assertStatus(400);
    }

    /**
     * @test checkSetPasswordPageErrorsWhenPasswordsDoesNotMatch
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenPasswordsDontMatch()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->adminUser->email,
            'user_id' => $this->adminUser->id,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'myroi',
            'confirm-password' => 'myroi2',
        ];
        $response = $this->from(route('auth.set-password'))->postJson(route('auth.set-password'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.set-password'));
    }

    /**
     * @test checkSetPasswordPageErrorsWhenUserDoesNotExist
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:setPassword
     */
    public function checkSetPasswordPageErrorsWhenUserDoesNotExist()
    {
        /** @var SecureToken $passwordReset */
        $passwordReset = SecureToken::factory()->create([
            'email' => $this->faker->email,
            'user_id' => 0,
            'type' => 'password_reset',
        ]);
        $body = [
            'token' => $passwordReset->token,
            'password' => 'myroi',
            'confirm-password' => 'myroi',
        ];
        $response = $this->from(route('auth.set-password'))->postJson(route('auth.set-password'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.set-password'));
    }

    /**
     * @test checkLogoutRedirectAfterLoggingOutTheUser
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:logout
     */
    public function checkLogoutRedirectAfterLoggingOutTheUser()
    {
        $this->actingAs($this->adminUser);
        $response = $this->getJson(route('auth.logout'));
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.login'));
        $this->assertGuest();
    }
}
