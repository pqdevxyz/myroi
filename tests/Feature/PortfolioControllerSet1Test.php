<?php

namespace Tests\Feature;

use App\Helpers\PQCache;
use App\Jobs\PortfolioPostDeleteJob;
use App\Models\Portfolio;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class PortfolioControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;
    use DataProviderHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * @test checkPortfolioOverviewPageLoads
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:viewPortfolio
     */
    public function checkPortfolioOverviewPageLoads()
    {
        $response = $this->getJson(route('portfolio.overview'));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.portfolio.overview');

        $totalCost = 0;
        $totalQuantity = 0;

        collect($response['portfolios'])->each(function ($portfolio) use (&$totalCost, &$totalQuantity) {
            $totalCost += $portfolio->total_cost;
            $totalQuantity += $portfolio->total_quantity;
        });

        $this->assertEquals(number_format($totalCost, 2), $response['totals']['cost']);
        $this->assertEquals(number_format($totalQuantity, 2), $response['totals']['quantity']);
    }

    /**
     * @test checkPortfolioCreatePageLoads
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:getCreatePortfolio
     */
    public function checkPortfolioCreatePageLoads()
    {
        $response = $this->getJson(route('portfolio.create'));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.portfolio.create');
    }

    /**
     * @test checkPortfolioCreatePageSavesCorrectly
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:createPortfolio
     */
    public function checkPortfolioCreatePageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->make();
        $body = [
            'name' => $portfolio->name,
            'type' => $portfolio->type,
            'colour' => $portfolio->colour,
        ];

        $response = $this->from(route('portfolio.overview'))->postJson(route('portfolio.create'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.overview'));
        $this->assertDatabaseHas('portfolios', [
            'name' => $portfolio->name,
            'user_id' => Auth::id(),
        ]);
    }

    /**
     * @test checkPortfolioCreatePageValidationWorksCorrectly
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:createPortfolio
     * @dataProvider threeArgsDataProvider
     */
    public function checkPortfolioCreatePageValidationWorksCorrectly($name, $type, $colour)
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->make();

        $body = [];
        $body = $name ? array_merge($body, ['name' => $portfolio->name]) : $body;
        $body = $type ? array_merge($body, ['type' => $portfolio->type]) : $body;
        $body = $colour ? array_merge($body, ['colour' => $portfolio->colour]) : $body;

        $response = $this->from(route('portfolio.overview'))->postJson(route('portfolio.create'), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkPortfolioManagePageLoads
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:getManagePortfolio
     */
    public function checkPortfolioManagePageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $response = $this->getJson(route('portfolio.manage', ['id' => $portfolio->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.portfolio.manage');
    }

    /**
     * @test checkPortfolioManagePageReturnsNotFound
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:getManagePortfolio
     */
    public function checkPortfolioManagePageReturnsNotFound()
    {
        $response = $this->getJson(route('portfolio.manage', ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * @test checkPortfolioUpdatePageSavesCorrectly
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:updatePortfolio
     */
    public function checkPortfolioUpdatePageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser,
        ]);
        /** @var Portfolio $newPortfolio */
        $newPortfolio = Portfolio::factory()->make();
        $body = [
            'name' => $newPortfolio->name,
            'type' => $newPortfolio->type,
            'colour' => $newPortfolio->colour,
        ];

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->postJson(route('portfolio.update', ['id' => $portfolio->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertDatabaseHas('portfolios', [
            'name' => $newPortfolio->name,
            'user_id' => Auth::id(),
        ]);
    }

    /**
     * @test checkPortfolioUpdatePageValidationWorksCorrectly
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:updatePortfolio
     * @dataProvider threeArgsDataProvider
     */
    public function checkPortfolioUpdatePageValidationWorksCorrectly($name, $type, $colour)
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->make();

        $body = [];
        $body = $name ? array_merge($body, ['name' => $portfolio->name]) : $body;
        $body = $type ? array_merge($body, ['type' => $portfolio->type]) : $body;
        $body = $colour ? array_merge($body, ['colour' => $portfolio->colour]) : $body;

        $response = $this->from(route('portfolio.manage', ['id' => 0]))->postJson(route('portfolio.update', ['id' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkPortfolioUpdatePageReturnsErrorWhenNoPortfolioIsFound
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:updatePortfolio
     */
    public function checkPortfolioUpdatePageReturnsErrorWhenNoPortfolioIsFound()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->make();
        $body = [
            'name' => $portfolio->name,
            'type' => $portfolio->type,
            'colour' => $portfolio->colour,
        ];

        $response = $this->from(route('portfolio.overview'))->postJson(route('portfolio.update', ['id' => 0]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.overview'));
    }

    /**
     * @test checkPortfolioDeleteWorksCorrectly
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:deletePortfolio
     */
    public function checkPortfolioDeleteWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);

        PQCache::retrieveFromCache('portfolios', $portfolio->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('portfolio.delete', ['id' => $portfolio->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.overview'));
        $this->assertSoftDeleted('portfolios', [
            'id' => $portfolio->id,
        ]);
        Queue::assertPushedOn('long', PortfolioPostDeleteJob::class);
        $this->assertNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
    }

    /**
     * @test checkPortfolioDeleteReturnsNotFoundError
     * @group app/Http/Controllers/PortfolioController
     * @group app/Http/Controllers/PortfolioController:deletePortfolio
     */
    public function checkPortfolioDeleteReturnsNotFoundError()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('portfolio.delete', ['id' => 0]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
    }
}
