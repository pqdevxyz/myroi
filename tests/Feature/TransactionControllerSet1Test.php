<?php

namespace Tests\Feature;

use App\Helpers\PQCache;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class TransactionControllerSet1Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;
    use DataProviderHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * @test checkTransactionCreatePageLoads
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:getCreateTransaction
     */
    public function checkTransactionCreatePageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        $response = $this->getJson(route('transaction.create', ['symbolId' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.transaction.create');
    }

    /**
     * @test checkTransactionCreatePageReturnsNotFoundError
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:getCreateTransaction
     */
    public function checkTransactionCreatePageReturnsNotFoundError()
    {
        $response = $this->getJson(route('transaction.create', ['symbolId' => 0]));
        $response->assertStatus(404);
    }

    /**
     * @test checkTransactionCreatePageSavesCorrectly
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:createTransaction
     */
    public function checkTransactionCreatePageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->make();
        $body = [
            'quantity' => $transaction->quantity,
            'buy_price' => $transaction->buy_price,
            'charges' => $transaction->charges,
            'tax' => $transaction->tax,
            'adjustment' => $transaction->adjustment,
            'buy_date' => $transaction->buy_date,
        ];

        PQCache::retrieveFromCache('portfolios', $portfolio->id, 'example', fn () => true);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->postJson(route('transaction.create', ['symbolId' => $symbol->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $this->assertDatabaseHas('transactions', [
            'buy_date' => $transaction->buy_date->format('Y-m-d'),
            'quantity' => $transaction->quantity,
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkTransactionCreatePageValidationWorksCorrectly
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:createTransaction
     * @dataProvider sixArgsDataProvider
     */
    public function checkTransactionCreatePageValidationWorksCorrectly($quantity, $buy_price, $charges, $tax, $adjustment, $buy_date)
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->make();

        $body = [];
        $body = $quantity ? array_merge($body, ['quantity' => $transaction->quantity]) : $body;
        $body = $buy_price ? array_merge($body, ['buy_price' => $transaction->buy_price]) : $body;
        $body = $charges ? array_merge($body, ['charges' => $transaction->charges]) : $body;
        $body = $tax ? array_merge($body, ['tax' => $transaction->tax]) : $body;
        $body = $adjustment ? array_merge($body, ['adjustment' => $transaction->adjustment]) : $body;
        $body = $buy_date ? array_merge($body, ['buy_date' => $transaction->buy_date]) : $body;

        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('transaction.create', ['symbolId' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkTransactionEditPageLoads
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:getEditTransaction
     */
    public function checkTransactionEditPageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        $response = $this->getJson(route('transaction.edit', ['id' => $transaction->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.transaction.edit');
    }

    /**
     * @test checkTransactionEditPageReturnsNotFoundError
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:getEditTransaction
     */
    public function checkTransactionEditPageReturnsNotFoundError()
    {
        $response = $this->getJson(route('transaction.edit', ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * @test checkTransactionEditPageSavesCorrectly
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:editTransaction
     */
    public function checkTransactionEditPageSavesCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        /** @var Transaction $newTransaction */
        $newTransaction = Transaction::factory()->make();
        $body = [
            'quantity' => $newTransaction->quantity,
            'buy_price' => $newTransaction->buy_price,
            'charges' => $newTransaction->charges,
            'tax' => $newTransaction->tax,
            'adjustment' => $transaction->adjustment,
            'buy_date' => $newTransaction->buy_date,
        ];

        PQCache::retrieveFromCache('portfolios', $portfolio->id, 'example', fn () => true);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->postJson(route('transaction.edit', ['id' => $transaction->id]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $this->assertDatabaseHas('transactions', [
            'buy_date' => $newTransaction->buy_date->format('Y-m-d'),
            'quantity' => $newTransaction->quantity,
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkTransactionEditPageValidationWorksCorrectly
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:editTransaction
     * @dataProvider sixArgsDataProvider
     */
    public function checkTransactionEditPageValidationWorksCorrectly($quantity, $buy_price, $charges, $tax, $adjustment, $buy_date)
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->make();

        $body = [];
        $body = $quantity ? array_merge($body, ['quantity' => $transaction->quantity]) : $body;
        $body = $buy_price ? array_merge($body, ['buy_price' => $transaction->buy_price]) : $body;
        $body = $charges ? array_merge($body, ['charges' => $transaction->charges]) : $body;
        $body = $tax ? array_merge($body, ['tax' => $transaction->tax]) : $body;
        $body = $adjustment ? array_merge($body, ['adjustment' => $transaction->adjustment]) : $body;
        $body = $buy_date ? array_merge($body, ['buy_date' => $transaction->buy_date]) : $body;

        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('transaction.edit', ['id' => 0]), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkTransactionEditPageReturnsErrorWhenNoTransactionIsFound
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:editTransaction
     */
    public function checkTransactionEditPageReturnsErrorWhenNoTransactionIsFound()
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->make();
        $body = [
            'quantity' => $transaction->quantity,
            'buy_price' => $transaction->buy_price,
            'charges' => $transaction->charges,
            'tax' => $transaction->tax,
            'adjustment' => $transaction->adjustment,
            'buy_date' => $transaction->buy_date,
        ];

        $response = $this->from(route('symbol.view.open', ['id' => 0]))->postJson(route('transaction.edit', ['id' => 0]), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => 0]));
    }

    /**
     * @test checkTransactionRemoveWorksCorrectly
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:removeTransaction
     */
    public function checkTransactionRemoveWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => $symbol->id,
        ]);

        PQCache::retrieveFromCache('portfolios', $portfolio->id, 'example', fn () => true);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        $response = $this->from(route('symbol.view.open', ['id' => $symbol->id]))->getJson(route('transaction.remove', ['id' => $transaction->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => $symbol->id]));
        $this->assertSoftDeleted('transactions', [
            'buy_date' => $transaction->buy_date->format('Y-m-d'),
            'quantity' => $transaction->quantity,
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNull(Cache::get(PQCache::getCacheKey('portfolios', $portfolio->id)));
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }

    /**
     * @test checkTransactionRemoveReturnsNotFoundError
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:removeTransaction
     */
    public function checkTransactionRemoveReturnsNotFoundError()
    {
        $response = $this->from(route('symbol.view.open', ['id' => 0]))->getJson(route('transaction.remove', ['id' => 0]));
        $response->assertStatus(302);
        $response->assertRedirect(route('symbol.view.open', ['id' => 0]));
    }

    /**
     * @test checkTransactionViewPageLoads
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:viewTransaction
     */
    public function checkTransactionViewPageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        $response = $this->getJson(route('transaction.view', ['id' => $transaction->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.transaction.view');
    }

    /**
     * @test checkTransactionViewPageReturnsNotFoundError
     * @group app/Http/Controllers/TransactionController
     * @group app/Http/Controllers/TransactionController:viewTransaction
     */
    public function checkTransactionViewPageReturnsNotFoundError()
    {
        $response = $this->getJson(route('transaction.view', ['id' => 0]));
        $response->assertStatus(404);
    }
}
