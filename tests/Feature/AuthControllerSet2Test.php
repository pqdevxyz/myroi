<?php

namespace Tests\Feature;

use App\Mail\ResetYourPassword;
use App\Models\SecureToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class AuthControllerSet2Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * @test checkForgotPasswordPageLoads
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewForgotPasswordPage
     */
    public function checkForgotPasswordPageLoads()
    {
        $response = $this->getJson(route('auth.forgot-password'));
        $response->assertStatus(200);
        $response->assertViewIs('auth.password.forgot-password');
    }

    /**
     * @test checkForgotPasswordPageRedirectsWhenAlreadyLoggedIn
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:viewForgotPasswordPage
     */
    public function checkForgotPasswordPageRedirectsWhenAlreadyLoggedIn()
    {
        $response = $this->actingAs($this->adminUser)->getJson(route('auth.forgot-password'));
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /**
     * @test checkForgotPasswordPageWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:sendPasswordReset
     */
    public function checkForgotPasswordPageWorksCorrectly()
    {
        $body = [
            'email' => $this->adminUser->email,
        ];
        $response = $this->from(route('auth.forgot-password'))->postJson(route('auth.forgot-password'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.forgot-password'));
        $this->assertDatabaseHas('secure_tokens', [
            'email' => $this->adminUser->email,
        ]);

        Mail::assertQueued(ResetYourPassword::class);
    }

    /**
     * @test checkForgotPasswordPageValidationWorksCorrectly
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:sendPasswordReset
     */
    public function checkForgotPasswordPageValidationWorksCorrectly()
    {
        $body = [];
        $response = $this->from(route('auth.forgot-password'))->postJson(route('auth.forgot-password'), $body);
        $response->assertStatus(422);
    }

    /**
     * @test checkForgotPasswordPageRedirectsIfUserDoesntExist
     * @group app/Http/Controllers/AuthController
     * @group app/Http/Controllers/AuthController:sendPasswordReset
     */
    public function checkForgotPasswordPageRedirectsIfUserDoesntExist()
    {
        $email = 'test@example.com';
        $body = [
            'email' => $email,
        ];
        $response = $this->from(route('auth.forgot-password'))->postJson(route('auth.forgot-password'), $body);
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.forgot-password'));
        $this->assertDatabaseMissing('secure_tokens', [
            'email' => $email,
        ]);
    }
}
