<?php

namespace Tests\Feature;

use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class SymbolControllerSet2Test extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs($this->adminUser);
    }

    /**
     * Check open symbol view page loads.
     *
     * @test checkOpenSymbolViewPageLoads
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewOpenSymbol
     */
    public function checkOpenSymbolViewPageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        Transaction::factory()->count(3)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count(3)->create([
            'symbol_id' => $symbol->id,
        ]);
        $response = $this->getJson(route('symbol.view.open', ['id' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.symbol.open');
    }

    /**
     * Check open symbol view page loads without historic prices.
     *
     * @test checkOpenSymbolViewPageLoadsWithoutHistoricPrices
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewOpenSymbol
     */
    public function checkOpenSymbolViewPageLoadsWithoutHistoricPrices()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        Transaction::factory()->count(3)->create([
            'symbol_id' => $symbol->id,
        ]);
        $response = $this->getJson(route('symbol.view.open', ['id' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.symbol.open');
    }

    /**
     * Check open symbol view page loads without historic prices and transactions.
     *
     * @test checkOpenSymbolViewPageLoadsWithoutHistoricPricesAndTransactions
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewOpenSymbol
     */
    public function checkOpenSymbolViewPageLoadsWithoutHistoricPricesAndTransactions()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        $response = $this->getJson(route('symbol.view.open', ['id' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.symbol.open');
    }

    /**
     * Check open symbol view page returns not found error.
     *
     * @test checkOpenSymbolViewPageReturnsNotFoundError
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewOpenSymbol
     */
    public function checkOpenSymbolViewPageReturnsNotFoundError()
    {
        $response = $this->getJson(route('symbol.view.open', ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * Check close symbol view page loads.
     *
     * @test checkCloseSymbolViewPageLoads
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewClosedSymbol
     */
    public function checkCloseSymbolViewPageLoads()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
            'is_closed' => true,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol->id,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol->id,
            'quantity' => -10,
            'buy_price' => 0.5,
            'charges' => 0,
        ]);
        $response = $this->getJson(route('symbol.view.closed', ['id' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.symbol.closed');
    }

    /**
     * Check close symbol view page loads without historic prices and transactions.
     *
     * @test checkCloseSymbolViewPageLoadsWithoutHistoricPricesAndTransactions
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewClosedSymbol
     */
    public function checkCloseSymbolViewPageLoadsWithoutHistoricPricesAndTransactions()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
            'is_closed' => true,
        ]);
        $response = $this->getJson(route('symbol.view.closed', ['id' => $symbol->id]));
        $response->assertStatus(200);
        $response->assertViewIs('dashboard.symbol.closed');
    }

    /**
     * Check close symbol view page returns not found error.
     *
     * @test checkCloseSymbolViewPageReturnsNotFoundError
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:viewClosedSymbol
     */
    public function checkCloseSymbolViewPageReturnsNotFoundError()
    {
        $response = $this->getJson(route('symbol.view.closed', ['id' => 0]));
        $response->assertStatus(404);
    }

    /**
     * Check togglePosition method closes a symbol.
     *
     * @test checkTogglePositionMethodClosesASymbol
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:togglePosition
     */
    public function checkTogglePositionMethodClosesASymbol()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
            'is_closed' => false,
        ]);

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('symbol.position', ['id' => $symbol->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertDatabaseHas('symbols', [
            'name' => $symbol->name,
            'portfolio_id' => $portfolio->id,
            'is_closed' => true,
        ]);
    }

    /**
     * Check togglePosition method reopens a symbol.
     *
     * @test checkTogglePositionMethodReopensASymbol
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:togglePosition
     */
    public function checkTogglePositionMethodReopensASymbol()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
            'is_closed' => true,
        ]);

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('symbol.position', ['id' => $symbol->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
        $this->assertDatabaseHas('symbols', [
            'name' => $symbol->name,
            'portfolio_id' => $portfolio->id,
            'is_closed' => false,
        ]);
    }

    /**
     * Check togglePosition returns error when symbol is not found.
     *
     * @test checkTogglePositionReturnsErrorWhenSymbolIsNotFound
     * @group app/Http/Controllers/SymbolController
     * @group app/Http/Controllers/SymbolController:togglePosition
     */
    public function checkTogglePositionReturnsErrorWhenSymbolIsNotFound()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();

        $response = $this->from(route('portfolio.manage', ['id' => $portfolio->id]))->getJson(route('symbol.position', ['id' => 0]));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.manage', ['id' => $portfolio->id]));
    }
}
