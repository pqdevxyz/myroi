<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\HandleDBTransactions;
use Tests\TestCase;

class BaseControllerTest extends TestCase
{
    use HandleDBTransactions;
    use WithFaker;

    /**
     * Check view home method returns correct redirect if not logged in.
     *
     * @test checkViewHomeMethodReturnsCorrectRedirectIfNotLoggedIn
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:viewHome
     */
    public function checkViewHomeMethodReturnsCorrectRedirectIfNotLoggedIn()
    {
        $response = $this->getJson(route('home'));
        $response->assertStatus(302);
        $response->assertRedirect(route('auth.login'));
    }

    /**
     * Check view home method returns correct redirect if logged in.
     *
     * @test checkViewHomeMethodReturnsCorrectRedirectIfLoggedIn
     * @group app/Http/Controllers/Controller
     * @group app/Http/Controllers/Controller:viewHome
     */
    public function checkViewHomeMethodReturnsCorrectRedirectIfLoggedIn()
    {
        $this->actingAs($this->adminUser);
        $response = $this->getJson(route('home'));
        $response->assertStatus(302);
        $response->assertRedirect(route('portfolio.overview'));
    }
}
