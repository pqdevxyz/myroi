<?php

namespace Tests\Unit\Mail;

use App\Mail\ResetYourPassword;
use Tests\TestCase;

class ResetYourPasswordMailTest extends TestCase
{
    /**
     * Check build method works correctly.
     *
     * @test checkBuildMethodWorksCorrectly
     * @group app/Mail/ResetYourPassword
     * @group app/Mail/ResetYourPassword:build
     */
    public function checkBuildMethodWorksCorrectly()
    {
        $mail = new ResetYourPassword('token');
        $mail = $mail->build();
        $this->assertTrue($mail->view == 'mail.auth.reset-your-password');
        $this->assertTrue($mail->viewData['token'] == route('auth.set-password') . '?token=token');
    }
}
