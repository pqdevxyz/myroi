<?php

namespace Tests\Unit\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Tests\TestCase;

class HorizonServiceProviderTest extends TestCase
{
    /* =================================
     * viewHorizon gate
     * =================================*/

    /**
     * Check viewHorizon gate allows access to user with an access level greater than or equal than 10 when environment is set to anything but local.
     *
     * @test checkViewHorizonGateAllowsAccessToUserWithAnAccessLevelGreaterThanOrEqualTo10WhenEnvironmentIsSetToAnythingButLocal
     * @group app/Providers/HorizonServiceProvider
     * @group app/Providers/HorizonServiceProvider:gate
     */
    public function checkViewHorizonGateAllowsAccessToUserWithAnAccessLevelGreaterThanOrEqualTo10WhenEnvironmentIsSetToAnythingButLocal()
    {
        $this->assertTrue(Gate::has('viewHorizon'));

        /** @var User $user */
        $user = User::factory()->create([
            'access_level' => 10,
        ]);
        $this->actingAs($user);

        $this->assertTrue(Gate::allows('viewHorizon'));
    }

    /**
     * Check viewHorizon gate blocks access to user with an access level less than 10 when environment is set to anything but local.
     *
     * @test checkViewHorizonGateBlocksAccessToUserWithAnAccessLevelLessThan10WhenEnvironmentIsSetToAnythingButLocal
     * @group app/Providers/HorizonServiceProvider
     * @group app/Providers/HorizonServiceProvider:gate
     */
    public function checkViewHorizonGateBlocksAccessToUserWithAnAccessLevelLessThan10WhenEnvironmentIsSetToAnythingButLocal()
    {
        $this->assertTrue(Gate::has('viewHorizon'));

        /** @var User $user */
        $user = User::factory()->create([
            'access_level' => 0,
        ]);
        $this->actingAs($user);

        $this->assertFalse(Gate::allows('viewHorizon'));
    }
}
