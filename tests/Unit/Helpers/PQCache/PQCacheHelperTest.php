<?php

namespace Tests\Unit\Helpers\PQCache;

use App\Helpers\PQCache;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class PQCacheHelperTest extends TestCase
{
    public $cache;

    protected function setUp(): void
    {
        parent::setUp();
        $this->cache = new PQCache();
    }

    /* =================================
     * getCacheKey method
     * =================================*/

    /**
     * Check getCacheKey method works correctly
     * @test checkGetCacheKeyMethodWorksCorrectly
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:getCacheKey
     */
    public function checkGetCacheKeyMethodWorksCorrectly()
    {
        $this->actingAs($this->adminUser);
        $key = $this->cache::getCacheKey('portfolios', 1);
        $this->assertTrue($this->adminUser->id . ':portfolios.1' == $key);
    }

    /* =================================
     * getCacheTTL method
     * =================================*/

    /**
     * Check getCacheTTL method returns correct value for portfolios
     * @test checkGetCacheTtlMethodReturnsCorrectValueForPortfolios
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:getCacheTTL
     */
    public function checkGetCacheTtlMethodReturnsCorrectValueForPortfolios()
    {
        $this->assertEquals(3600, $this->cache::getCacheTTL('portfolios'));
    }

    /**
     * Check getCacheTTL method returns correct value for symbols
     * @test checkGetCacheTtlMethodReturnsCorrectValueForSymbols
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:getCacheTTL
     */
    public function checkGetCacheTtlMethodReturnsCorrectValueForSymbols()
    {
        $this->assertEquals(3600, $this->cache::getCacheTTL('symbols'));
    }

    /**
     * Check getCacheTTL method returns correct value for default
     * @test checkGetCacheTtlMethodReturnsCorrectValueForDefault
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:getCacheTTL
     */
    public function checkGetCacheTtlMethodReturnsCorrectValueForDefault()
    {
        $this->assertEquals(60, $this->cache::getCacheTTL('default'));
    }

    /* =================================
     * removeFromCache method
     * =================================*/

    /**
     * Check removeFromCache method return true when item is removed from cache
     * @test checkRemoveFromCacheMethodReturnTrueWhenItemIsRemovedFromCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:removeFromCache
     */
    public function checkRemoveFromCacheMethodReturnTrueWhenItemIsRemovedFromCache()
    {
        $this->actingAs($this->adminUser);
        Cache::put($this->cache::getCacheKey('test', 1), 'value');
        $this->assertTrue($this->cache::removeFromCache('test', 1));
    }

    /**
     * Check removeFromCache method return false when item does not exist in cache
     * @test checkRemoveFromCacheMethodReturnFalseWhenItemDoesNotExistInCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:removeFromCache
     */
    public function checkRemoveFromCacheMethodReturnFalseWhenItemDoesNotExistInCache()
    {
        $this->actingAs($this->adminUser);
        $this->assertFalse($this->cache::removeFromCache('test', 1));
    }

    /* =================================
     * retrieveFromCache method
     * =================================*/

    /**
     * Check retrieveFromCache method works when item exists in the cache
     * @test checkRetrieveFromCacheMethodWorksWhenItemExistsInTheCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:retrieveFromCache
     */
    public function checkRetrieveFromCacheMethodWorksWhenItemExistsInTheCache()
    {
        $this->actingAs($this->adminUser);
        Cache::put($this->cache::getCacheKey('test', 1), ['key' => 'value']);
        $this->assertEquals('value', $this->cache::retrieveFromCache('test', 1, 'key'));
    }

    /**
     * Check retrieveFromCache method works when key exists and other items are in cache
     * @test checkRetrieveFromCacheMethodWorksWhenKeyExistsAndOtherItemsAreInCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:retrieveFromCache
     */
    public function checkRetrieveFromCacheMethodWorksWhenKeyExistsAndOtherItemsAreInCache()
    {
        $this->actingAs($this->adminUser);
        Cache::put($this->cache::getCacheKey('test', 1), ['test' => 'example']);
        $response = $this->cache::retrieveFromCache('test', 1, 'key', function () {
            return 'value';
        });
        $this->assertEquals('value', $response);
        $this->assertSame('example', $this->cache::retrieveFromCache('test', 1, 'test'));
    }

    /**
     * Check retrieveFromCache method works when key exists and no items are in cache
     * @test checkRetrieveFromCacheMethodWorksWhenKeyExistsAndNoItemsAreInCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:retrieveFromCache
     */
    public function checkRetrieveFromCacheMethodWorksWhenKeyExistsAndNoItemsAreInCache()
    {
        $this->actingAs($this->adminUser);
        Cache::put($this->cache::getCacheKey('test', 1), null);
        $response = $this->cache::retrieveFromCache('test', 1, 'key', function () {
            return 'value';
        });
        $this->assertEquals('value', $response);
    }

    /**
     * Check retrieveFromCache method works when key does not exist in cache
     * @test checkRetrieveFromCacheMethodWorksWhenKeyDoesNotExistInCache
     * @group app/Helpers/PQCache
     * @group app/Helpers/PQCache:retrieveFromCache
     */
    public function checkRetrieveFromCacheMethodWorksWhenKeyDoesNotExistInCache()
    {
        $this->actingAs($this->adminUser);
        $response = $this->cache::retrieveFromCache('test', 1, 'key', function () {
            return 'value';
        });
        $this->assertEquals('value', $response);
    }
}
