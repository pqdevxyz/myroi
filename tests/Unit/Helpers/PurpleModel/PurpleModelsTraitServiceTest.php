<?php

namespace Unit\Helpers\PurpleModel;

use App\Models\User;
use Tests\Doubles\Helpers\PurpleModel;
use Tests\TestCase;

class PurpleModelsTraitServiceTest extends TestCase
{
    public PurpleModel $purpleModel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->purpleModel = new PurpleModel();
    }

    /* =================================
     * Created By Field
     * =================================*/

    /**
     * @test checkGetCreatedByAttributeReturnsNull
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsNull()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);
        $this->assertNull($user->created_by);
    }

    /**
     * @test checkGetCreatedByAttributeReturnsUserModel
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:getCreatedByAttribute
     */
    public function checkGetCreatedByAttributeReturnsUserModel()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'created_by' => $this->adminUser->id,
        ]);
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /**
     * @test checkCreatedByAttributeSetsField
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setCreatedByAttribute
     */
    public function checkCreatedByAttributeSetsField()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);
        $user->created_by = $this->adminUser->id;
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /**
     * @test checkCreatedBySetsFieldWithoutChangingExistingFields
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setCreatedByAttribute
     */
    public function checkCreatedBySetsFieldWithoutChangingExistingFields()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => [
                'test' => 'phpunit'
            ],
        ]);

        $this->assertSame('phpunit', $user->additional_data['test']);

        $user->created_by = $this->adminUser->id;

        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
        $this->assertSame('phpunit', $user->additional_data['test']);
    }

    /**
     * @test checkCreatedByAttributeUpdatesField
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setCreatedByAttribute
     */
    public function checkCreatedByAttributeUpdatesField()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'created_by' => 0,
        ]);
        $this->assertNull($user->created_by);
        $user->created_by = $this->adminUser->id;
        $this->assertNotNull($user->created_by);
        $this->assertTrue($user->created_by == $this->adminUser->id);
    }

    /* =================================
     * Updated By Field
     * =================================*/

    /**
     * @test checkGetUpdatedByAttributeReturnsNull
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsNull()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);
        $this->assertNull($user->updated_by);
    }

    /**
     * @test checkGetUpdatedByAttributeReturnsUserModel
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:getUpdatedByAttribute
     */
    public function checkGetUpdatedByAttributeReturnsUserModel()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'updated_by' => $this->adminUser->id,
        ]);
        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }

    /**
     * @test checkUpdatedByAttributeSetsField
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeSetsField()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => [
                'test' => 'phpunit'
            ],
        ]);

        $this->assertSame('phpunit', $user->additional_data['test']);

        $user->updated_by = $this->adminUser->id;

        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
        $this->assertSame('phpunit', $user->additional_data['test']);
    }

    /**
     * @test checkUpdatedByAttributeSetsFieldWithoutChangingExistingFields
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeSetsFieldWithoutChangingExistingFields()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);


        $user->updated_by = $this->adminUser->id;

        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }

    /**
     * @test checkUpdatedByAttributeUpdatesField
     * @group app/Helpers/PurpleModelsTrait
     * @group app/Helpers/PurpleModelsTrait:setUpdatedByAttribute
     */
    public function checkUpdatedByAttributeUpdatesField()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'updated_by' => 0,
        ]);
        $this->assertNull($user->updated_by);
        $user->updated_by = $this->adminUser->id;
        $this->assertNotNull($user->updated_by);
        $this->assertTrue($user->updated_by == $this->adminUser->id);
    }
}
