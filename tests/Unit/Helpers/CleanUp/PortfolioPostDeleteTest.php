<?php

namespace Unit\Helpers\CleanUp;

use App\Helpers\CleanUp\PortfolioPostDelete;
use App\Helpers\PQCache;
use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class PortfolioPostDeleteTest extends TestCase
{
    /* =================================
     * run method
     * =================================*/

    /**
     * Check run method works correctly.
     *
     * @test checkRunMethodWorksCorrectly
     * @group app/Helpers/CleanUp/PortfolioPostDelete
     * @group app/Helpers/CleanUp/PortfolioPostDelete:run
     */
    public function checkRunMethodWorksCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
            'deleted_at' => Carbon::now(),
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Transaction $transactions */
        $transactions = Transaction::factory()->count(3)->create([
            'symbol_id' => $symbol->id,
        ]);
        /** @var HistoricPrice $historicPrices */
        $historicPrices = HistoricPrice::factory()->count(3)->create([
            'symbol_id' => $symbol->id,
        ]);

        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        PortfolioPostDelete::run($portfolio->id);

        $this->assertSoftDeleted('symbols', [
            'id' => $symbol->id,
        ]);

        $this->assertSoftDeleted('transactions', [
            'id' => $transactions->pluck('id')->toArray(),
        ]);

        $this->assertDatabaseMissing('historic_prices', [
            'id' => $historicPrices->pluck('id')->toArray(),
        ]);

        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }
}
