<?php

namespace Tests\Unit\Jobs;

use App\Helpers\PQCache;
use App\Jobs\CalculateHistoricPriceROIJob;
use App\Models\HistoricPrice;
use App\Models\Symbol;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class CalculateHistoricPriceROIJobTest extends TestCase
{
    /* =================================
     * handle method
     * =================================*/

    /**
     * Check handle method works correctly.
     *
     * @test checkHandleMethodWorksCorrectly
     * @group app/Jobs/CalculateHistoricPriceROI
     * @group app/Jobs/CalculateHistoricPriceROI:handle
     */
    public function checkHandleMethodWorksCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var HistoricPrice $price1 */
        $price1 = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
            'close_price' => 20,
            'roi' => 0,
            'date' => Carbon::now()->format('Y-m-d'),
        ]);
        /** @var HistoricPrice $price2 */
        $price2 = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
            'close_price' => 10,
            'roi' => 0,
            'date' => Carbon::now()->subDays(30)->format('Y-m-d'),
        ]);
        PQCache::retrieveFromCache('symbols', $symbol->id, 'example', fn () => true);
        $this->assertNotNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));

        (new CalculateHistoricPriceROIJob($symbol->id))->handle();

        $roi = HistoricPrice::where('symbol_id', $symbol->id)->where('date', Carbon::now()->format('Y-m-d'))->first();
        $this->assertEquals($price1->close_price - $price2->close_price, $roi->roi);
        $this->assertNull(Cache::get(PQCache::getCacheKey('symbols', $symbol->id)));
    }
}
