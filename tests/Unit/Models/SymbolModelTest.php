<?php

namespace Tests\Unit\Models;

use App\Models\HistoricPrice;
use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Support\Collection;
use Tests\TestCase;

class SymbolModelTest extends TestCase
{
    /* =================================
     * portfolio relationship
     * =================================*/

    /**
     * Check portfolio relationship returns linked Portfolio model correctly.
     *
     * @test checkPortfolioRelationshipReturnsLinkedPortfolioModelCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:portfolio
     */
    public function checkPortfolioRelationshipReturnsLinkedPortfolioModelCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        $this->assertNotNull($symbol->portfolio);
    }

    /* =================================
     * transactions relationship
     * =================================*/

    /**
     * Check transactions relationship returns linked Transaction model correctly.
     *
     * @test checkTransactionsRelationshipReturnsLinkedTransactionModelCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:transactions
     */
    public function checkTransactionsRelationshipReturnsLinkedTransactionModelCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNotNull($symbol->transactions);
        $this->assertCount($count, $symbol->transactions);
    }

    /* =================================
     * historicPrices relationship
     * =================================*/

    /**
     * Check historicPrices relationship returns linked HistoricPrice model correctly.
     *
     * @test checkHistoricPricesRelationshipReturnsLinkedHistoricPriceModelCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:transactions
     */
    public function checkHistoricPricesRelationshipReturnsLinkedHistoricPriceModelCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNotNull($symbol->historicPrices);
        $this->assertCount($count, $symbol->historicPrices);
    }

    /* =================================
     * getTotalCostAttribute method
     * =================================*/

    /**
     * Check getTotalCostAttribute method returns value correctly.
     *
     * @test checkGetTotalCostAttributeMethodReturnsValueCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsValueCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Collection $transactions */
        $transactions = Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol,
        ]);

        //Calculate running total
        $runningTotal = 0;
        foreach ($transactions as $transaction) {
            $runningTotal += (($transaction->quantity * $transaction->buy_price) + $transaction->charges + $transaction->tax + $transaction->adjustment);
        }
        $this->assertEqualsWithDelta($runningTotal, $symbol->total_cost, 1);
    }

    /**
     * Check getTotalCostAttribute method returns value correctly when total quantity is zero.
     *
     * @test checkGetTotalCostAttributeMethodReturnsValueCorrectlyWhenTotalQuantityIsZero
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsValueCorrectlyWhenTotalQuantityIsZero()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => -10,
            'buy_price' => 1.5,
            'charges' => 0,
        ]);

        $this->assertTrue($symbol->total_quantity == 0);
        $this->assertTrue($symbol->total_cost == 0);
    }

    /* =================================
     * getTotalQuantityAttribute method
     * =================================*/

    /**
     * Check getTotalQuantityAttribute method returns value correctly.
     *
     * @test checkGetTotalQuantityAttributeMethodReturnsValueCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalQuantityAttribute
     */
    public function checkGetTotalQuantityAttributeMethodReturnsValueCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Collection $transactions */
        $transactions = Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol,
        ]);

        //Calculate running total
        $runningTotal = 0;
        foreach ($transactions as $transaction) {
            $runningTotal += $transaction->quantity;
        }
        $this->assertEqualsWithDelta($runningTotal, $symbol->total_quantity, 1);
    }

    /* =================================
     * getTotalConsiderationAttribute method
     * =================================*/

    /**
     * Check getTotalConsiderationAttribute method returns value correctly.
     *
     * @test checkGetTotalConsiderationAttributeMethodReturnsValueCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalConsiderationAttribute
     */
    public function checkGetTotalConsiderationAttributeMethodReturnsValueCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Collection $transactions */
        $transactions = Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol,
        ]);

        //Calculate running total
        $runningTotal = 0;
        foreach ($transactions as $transaction) {
            $runningTotal += ($transaction->quantity * $transaction->buy_price);
        }
        $this->assertEqualsWithDelta($runningTotal, $symbol->total_consideration, 1);
    }

    /* =================================
     * getLatestPriceAttribute method
     * =================================*/

    /**
     * Check getLatestPriceAttribute method returns latest historic price.
     *
     * @test checkGetLatestPriceAttributeMethodReturnsLatestHistoricPrice
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getLatestPriceAttribute
     */
    public function checkGetLatestPriceAttributeMethodReturnsLatestHistoricPrice()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Collection $historicPrices */
        $historicPrices = HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $this->assertTrue($historicPrices->sortByDesc('date')->first()->id == $symbol->latest_price->id);
    }

    /* =================================
     * getTotalSaleAttribute method
     * =================================*/

    /**
     * Check getTotalSaleAttribute method returns value correctly.
     *
     * @test checkGetTotalSaleAttributeMethodReturnsValueCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalSaleAttribute
     */
    public function checkGetTotalSaleAttributeMethodReturnsValueCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $total = $symbol->latest_price->close_price * $symbol->total_quantity;
        $this->assertEqualsWithDelta($total, $symbol->total_sale, 1);
    }

    /**
     * Check getTotalSaleAttribute method returns zero when latest price is not set.
     *
     * @test checkGetTotalSaleAttributeMethodReturnsZeroWhenLatestPriceIsNotSet
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalSaleAttribute
     */
    public function checkGetTotalSaleAttributeMethodReturnsZeroWhenLatestPriceIsNotSet()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        $this->assertEquals(0, $symbol->total_sale);
    }

    /* =================================
     * getGrossRoiAttribute method
     * =================================*/

    /**
     * Check getGrossRoiAttribute method returns roi correctly.
     *
     * @test checkGetGrossRoiAttributeMethodReturnsRoiCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiAttribute
     */
    public function checkGetGrossRoiAttributeMethodReturnsRoiCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $roi = $symbol->total_sale - $symbol->total_consideration;
        $this->assertEqualsWithDelta($roi, $symbol->gross_roi, 1);
    }

    /* =================================
     * getGrossRoiPercentageAttribute method
     * =================================*/

    /**
     * Check getGrossRoiPercentageAttribute method returns roi correctly.
     *
     * @test checkGetGrossRoiPercentageAttributeMethodReturnsRoiCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiPercentageAttribute
     */
    public function checkGetGrossRoiPercentageAttributeMethodReturnsRoiCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $roi = ($symbol->gross_roi / $symbol->total_consideration) * 100;
        $this->assertEqualsWithDelta($roi, $symbol->gross_roi_percentage, 0.5);
    }

    /**
     * Check getGrossRoiPercentageAttribute method returns zero when total consideration is zero.
     *
     * @test checkGetGrossRoiPercentageAttributeMethodReturnsZeroWhenTotalConsiderationIsZero
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiPercentageAttribute
     */
    public function checkGetGrossRoiPercentageAttributeMethodReturnsZeroWhenTotalConsiderationIsZero()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $this->assertEquals(0, $symbol->gross_roi_percentage);
    }

    /* =================================
     * getNetRoiAttribute method
     * =================================*/

    /**
     * Check getNetRoiAttribute method returns roi correctly.
     *
     * @test checkGetNetRoiAttributeMethodReturnsRoiCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiPercentageAttribute
     */
    public function checkGetNetRoiAttributeMethodReturnsRoiCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $roi = $symbol->total_sale - $symbol->total_cost;
        $this->assertEqualsWithDelta($roi, $symbol->net_roi, 1);
    }

    /* =================================
     * getNetRoiPercentageAttribute method
     * =================================*/

    /**
     * Check getNetRoiPercentageAttribute method returns roi correctly.
     *
     * @test checkGetNetRoiPercentageAttributeMethodReturnsRoiCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiPercentageAttribute
     */
    public function checkGetNetRoiPercentageAttributeMethodReturnsRoiCorrectly()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $roi = ($symbol->net_roi / $symbol->total_cost) * 100;
        $this->assertEqualsWithDelta($roi, $symbol->net_roi_percentage, 0.5);
    }

    /**
     * Check getNetRoiPercentageAttribute method returns zero when total cost is zero.
     *
     * @test checkGetNetRoiPercentageAttributeMethodReturnsZeroWhenTotalCostIsZero
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getGrossRoiPercentageAttribute
     */
    public function checkGetNetRoiPercentageAttributeMethodReturnsZeroWhenTotalCostIsZero()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $this->assertEquals(0, $symbol->net_roi_percentage);
    }

    /* =================================
     * getPercentageOfPortfolioAttribute method
     * =================================*/

    /**
     * Check getPercentageOfPortfolioAttribute method returns percentage correctly.
     *
     * @test checkGetPercentageOfPortfolioAttributeMethodReturnsPercentageCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getPercentageOfPortfolioAttribute
     */
    public function checkGetPercentageOfPortfolioAttributeMethodReturnsPercentageCorrectly()
    {
        $count = 3;
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);
        HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $percentage = ($symbol->total_cost / $symbol->portfolio->total_cost) * 100;
        $this->assertEqualsWithDelta($percentage, $symbol->percentage_of_portfolio, 0.5);
    }

    /**
     * Check getPercentageOfPortfolioAttribute method returns zero when portfolio total cost is zero.
     *
     * @test checkGetPercentageOfPortfolioAttributeMethodReturnsZeroWhenPortfolioTotalCostIsZero
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getPercentageOfPortfolioAttribute
     */
    public function checkGetPercentageOfPortfolioAttributeMethodReturnsZeroWhenPortfolioTotalCostIsZero()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);

        $this->assertEquals(0, $symbol->percentage_of_portfolio);
    }

    /* =================================
     * getNextPriceDateAttribute method
     * =================================*/

    /**
     * Check getNextPriceDateAttribute method returns next historic price date.
     *
     * @test checkGetNextPriceDateAttributeMethodReturnsNextHistoricPriceDate
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getNextPriceDateAttribute
     */
    public function checkGetNextPriceDateAttributeMethodReturnsNextHistoricPriceDate()
    {
        $count = 3;
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Collection $historicPrices */
        $historicPrices = HistoricPrice::factory()->count($count)->create([
            'symbol_id' => $symbol->id,
        ]);

        $this->assertTrue($historicPrices->sortByDesc('date')->first()->date->addMonths(6)->format('Y-m-d') == $symbol->next_price_date->format('Y-m-d'));
    }

    /* =================================
     * getTotalProfitAttribute method
     * =================================*/

    /**
     * Check getTotalProfitAttribute method returns profit correctly.
     *
     * @test checkGetTotalProfitAttributeMethodReturnsProfitCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalProfitAttribute
     */
    public function checkGetTotalProfitAttributeMethodReturnsProfitCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
            'is_closed' => 1,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => -10,
            'buy_price' => 1.5,
            'charges' => 0,
        ]);

        $this->assertTrue($symbol->total_quantity == 0);
        $this->assertTrue($symbol->total_profit == 5);
    }

    /**
     * Check getTotalProfitAttribute method returns loss correctly.
     *
     * @test checkGetTotalProfitAttributeMethodReturnsLossCorrectly
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalProfitAttribute
     */
    public function checkGetTotalProfitAttributeMethodReturnsLossCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
            'is_closed' => 1,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => -10,
            'buy_price' => 0.5,
            'charges' => 0,
        ]);

        $this->assertTrue($symbol->total_quantity == 0);
        $this->assertTrue($symbol->total_profit == -5);
    }

    /**
     * Check getTotalProfitAttribute method returns zero when symbol is not closed.
     *
     * @test checkGetTotalProfitAttributeMethodReturnsZeroWhenSymbolIsNotClosed
     * @group app/Models/Symbol
     * @group app/Models/Symbol:getTotalProfitAttribute
     */
    public function checkGetTotalProfitAttributeMethodReturnsZeroWhenSymbolIsNotClosed()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => -10,
            'buy_price' => 0.5,
            'charges' => 0,
        ]);

        $this->assertTrue($symbol->total_quantity == 0);
        $this->assertTrue($symbol->total_profit == 0);
    }
}
