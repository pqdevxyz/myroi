<?php

namespace Tests\Unit\Models;

use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    /* =================================
     * Full Name Field
     * =================================*/

    /**
     * Check method returns correctly.
     *
     * @test checkGetFullNameAttributeReturnsCorrectly
     * @group app/Models/User
     * @group app/Models/User:getFullNameAttribute
     */
    public function checkGetFullNameAttributeReturnsCorrectly()
    {
        /** @var User $user */
        $user = User::factory()->make();
        $this->assertNotNull($user->full_name);
        $this->assertTrue($user->full_name == $user->first_name . ' ' . $user->last_name);
    }

    /* =================================
     * Password Last Changed Field
     * =================================*/

    /**
     * Check method returns null.
     *
     * @test checkGetPasswordLastChangedAttributeReturnsNull
     * @group app/Models/User
     * @group app/Models/User:getPasswordLastChangedAttribute
     */
    public function checkGetPasswordLastChangedAttributeReturnsNull()
    {
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => null,
        ]);
        $this->assertNull($user->password_last_changed);
    }

    /**
     * Check method returns User model.
     *
     * @test checkGetPasswordLastChangedAttributeReturnsUserModel
     * @group app/Models/User
     * @group app/Models/User:getPasswordLastChangedAttribute
     */
    public function checkGetPasswordLastChangedAttributeReturnsUserModel()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => $now,
        ]);
        $this->assertNotNull($user->password_last_changed);
        $this->assertTrue($user->password_last_changed == $now);
    }

    /**
     * Check method sets field.
     *
     * @test checkPasswordLastChangedAttributeSetsField
     * @group app/Models/User
     * @group app/Models/User:setPasswordLastChangedAttribute
     */
    public function checkPasswordLastChangedAttributeSetsField()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'additional_data' => null,
        ]);
        $user->password_last_changed = $now;
        $this->assertNotNull($user->password_last_changed);
        $this->assertTrue($user->password_last_changed == $now);
    }

    /**
     * Check method updates field.
     *
     * @test checkPasswordLastChangedAttributeUpdatesField
     * @group app/Models/User
     * @group app/Models/User:setPasswordLastChangedAttribute
     */
    public function checkPasswordLastChangedAttributeUpdatesField()
    {
        $now = Carbon::now();
        /** @var User $user */
        $user = User::factory()->make([
            'password_last_changed' => Carbon::parse('0'),
        ]);

        $this->assertEqualsWithDelta(Carbon::parse('0')->format('U'), $user->password_last_changed->format('U'), 1);

        $user->password_last_changed = $now;

        $this->assertNotNull($user->password_last_changed);
        $this->assertEqualsWithDelta($now->format('U'), $user->password_last_changed->format('U'), 2);
    }
}
