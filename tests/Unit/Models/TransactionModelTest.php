<?php

namespace Tests\Unit\Models;

use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionModelTest extends TestCase
{
    use WithFaker;

    /* =================================
     * symbol relationship
     * =================================*/

    /**
     * Check symbol relationship returns linked Symbol model correctly.
     *
     * @test checkSymbolRelationshipReturnsLinkedSymbolModelCorrectly
     * @group app/Models/Transaction
     * @group app/Models/Transaction:symbol
     */
    public function checkSymbolRelationshipReturnsLinkedSymbolModelCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNotNull($transaction->symbol);
    }

    /* =================================
     * getTotalCostAttribute method
     * =================================*/

    /**
     * Check getTotalCostAttribute method returns total correctly.
     *
     * @test checkGetTotalCostAttributeMethodReturnsTotalCorrectly
     * @group app/Models/Transaction
     * @group app/Models/Transaction:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsTotalCorrectly()
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => 0,
        ]);

        $total = ($transaction->quantity * $transaction->buy_price) + $transaction->charges + $transaction->tax + $transaction->adjustment;
        $this->assertEqualsWithDelta($total, $transaction->total_cost, 1);
    }

    /**
     * Check getTotalCostAttribute method returns total correctly when only adjustment value is present.
     *
     * @test checkGetTotalCostAttributeMethodReturnsTotalCorrectlyWhenOnlyAdjustmentValueIsPresent
     * @group app/Models/Transaction
     * @group app/Models/Transaction:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsTotalCorrectlyWhenOnlyAdjustmentValueIsPresent()
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => 0,
            'quantity' => 0,
            'buy_price' => 0,
            'charges' => 0,
            'tax' => 0,
            'adjustment' => $this->faker->randomFloat(2, 10, 500),
        ]);
        $total = ($transaction->quantity * $transaction->buy_price) + $transaction->charges + $transaction->tax + $transaction->adjustment;
        $this->assertEqualsWithDelta($total, $transaction->total_cost, 1);
    }

    /* =================================
     * getConsiderationCostAttribute method
     * =================================*/

    /**
     * Check getConsiderationCostAttribute method returns total correctly.
     *
     * @test checkGetConsiderationCostAttributeMethodReturnsTotalCorrectly
     * @group app/Models/Transaction
     * @group app/Models/Transaction:getConsiderationCostAttribute
     */
    public function checkGetConsiderationCostAttributeMethodReturnsTotalCorrectly()
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::factory()->create([
            'symbol_id' => 0,
        ]);

        $consideration = $transaction->quantity * $transaction->buy_price;
        $this->assertEqualsWithDelta($consideration, $transaction->consideration_cost, 1);
    }
}
