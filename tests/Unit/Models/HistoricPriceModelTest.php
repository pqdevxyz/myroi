<?php

namespace Tests\Unit\Models;

use App\Models\HistoricPrice;
use App\Models\Symbol;
use Carbon\Carbon;
use Tests\TestCase;

class HistoricPriceModelTest extends TestCase
{
    /* =================================
     * symbol relationship
     * =================================*/

    /**
     * Check symbol relationship returns linked Symbol model correctly.
     *
     * @test checkSymbolRelationshipReturnsLinkedSymbolModelCorrectly
     * @group app/Models/HistoricPrice
     * @group app/Models/HistoricPrice:symbol
     */
    public function checkSymbolRelationshipReturnsLinkedSymbolModelCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var HistoricPrice $historicPrice */
        $historicPrice = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
        ]);
        $this->assertNotNull($historicPrice->symbol);
    }

    /* =================================
     * getRoiPercentageAttribute method
     * =================================*/

    /**
     * Check getRoiPercentageAttribute method returns value correctly.
     *
     * @test checkGetRoiPercentageAttributeMethodReturnsValueCorrectly
     * @group app/Models/HistoricPrice
     * @group app/Models/HistoricPrice:getRoiPercentageAttribute
     */
    public function checkGetRoiPercentageAttributeMethodReturnsValueCorrectly()
    {
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => 0,
        ]);
        /** @var HistoricPrice $price1 */
        $price1 = HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
            'close_price' => 20,
            'roi' => 10,
            'date' => Carbon::now()->format('Y-m-d'),
        ]);
        HistoricPrice::factory()->create([
            'symbol_id' => $symbol->id,
            'close_price' => 10,
            'roi' => 0,
            'date' => Carbon::now()->subDays(30)->format('Y-m-d'),
        ]);

        $this->assertEqualsWithDelta(50, $price1->roi_percentage, 0.01);
    }
}
