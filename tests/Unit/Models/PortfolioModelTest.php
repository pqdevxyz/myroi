<?php

namespace Tests\Unit\Models;

use App\Models\Portfolio;
use App\Models\Symbol;
use App\Models\Transaction;
use Illuminate\Support\Collection;
use Tests\TestCase;

class PortfolioModelTest extends TestCase
{
    /* =================================
     * user relationship
     * =================================*/

    /**
     * Check user relationship returns linked User model correctly.
     *
     * @test checkUserRelationshipReturnsUserModelCorrectly
     * @group app/Models/Portfolio
     * @group app/Models/Portfolio:user
     */
    public function checkUserRelationshipReturnsUserModelCorrectly()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create([
            'user_id' => $this->adminUser->id,
        ]);
        $this->assertNotNull($portfolio->user->email);
    }

    /* =================================
     * symbols relationship
     * =================================*/

    /**
     * Check symbols relationship returns Symbol model correctly.
     *
     * @test checkSymbolsRelationshipReturnsSymbolModelCorrectly
     * @group app/Models/Portfolio
     * @group app/Models/Portfolio:symbols
     */
    public function checkSymbolsRelationshipReturnsSymbolModelCorrectly()
    {
        $count = 3;
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        Symbol::factory()->count($count)->create([
            'portfolio_id' => $portfolio->id,
        ]);
        $this->assertNotNull($portfolio->symbols);
        $this->assertCount($count, $portfolio->symbols);
    }

    /* =================================
     * getTotalCostAttribute method
     * =================================*/

    /**
     * Check getTotalCostAttribute method returns total correctly.
     *
     * @test checkGetTotalCostAttributeMethodReturnsTotalCorrectly
     * @group app/Models/Portfolio
     * @group app/Models/Portfolio:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsTotalCorrectly()
    {
        $count = 3;
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Collection $transactions */
        $transactions = Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol,
        ]);

        //Calculate running total
        $runningTotal = 0;
        foreach ($transactions as $transaction) {
            $runningTotal += (($transaction->quantity * $transaction->buy_price) + $transaction->charges + $transaction->tax + $transaction->adjustment);
        }
        $this->assertEqualsWithDelta($runningTotal, $portfolio->total_cost, 1);
    }

    /**
     * Check getTotalCostAttribute method returns total correctly when symbol total cost is zero.
     *
     * @test checkGetTotalCostAttributeMethodReturnsTotalCorrectlyWhenSymbolTotalCostIsZero
     * @group app/Models/Portfolio
     * @group app/Models/Portfolio:getTotalCostAttribute
     */
    public function checkGetTotalCostAttributeMethodReturnsTotalCorrectlyWhenSymbolTotalCostIsZero()
    {
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => 10,
            'buy_price' => 1,
            'charges' => 0,
        ]);
        Transaction::factory()->create([
            'symbol_id' => $symbol,
            'quantity' => -10,
            'buy_price' => 1.5,
            'charges' => 0,
        ]);

        $this->assertTrue($portfolio->total_quantity == 0);
        $this->assertTrue($portfolio->total_cost == 0);
    }

    /* =================================
     * getTotalQuantityAttribute method
     * =================================*/

    /**
     * Check getTotalQuantityAttribute method returns total correctly.
     *
     * @test checkGetTotalQuantityAttributeMethodReturnsTotalCorrectly
     * @group app/Models/Portfolio
     * @group app/Models/Portfolio:getTotalQuantityAttribute
     */
    public function checkGetTotalQuantityAttributeMethodReturnsTotalCorrectly()
    {
        $count = 3;
        /** @var Portfolio $portfolio */
        $portfolio = Portfolio::factory()->create();
        /** @var Symbol $symbol */
        $symbol = Symbol::factory()->create([
            'portfolio_id' => $portfolio->id,
        ]);
        /** @var Collection $transactions */
        $transactions = Transaction::factory()->count($count)->create([
            'symbol_id' => $symbol,
        ]);

        //Calculate running total
        $runningTotal = 0;
        foreach ($transactions as $transaction) {
            $runningTotal += $transaction->quantity;
        }
        $this->assertEqualsWithDelta($runningTotal, $portfolio->total_quantity, 1);
    }
}
