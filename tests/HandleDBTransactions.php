<?php

namespace Tests;

use Illuminate\Support\Facades\DB;

trait HandleDBTransactions
{
    public function setUp(): void
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown(): void
    {
        DB::rollback();
        parent::tearDown();
    }
}
