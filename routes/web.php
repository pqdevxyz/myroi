<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HistoricPriceController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\SymbolController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['headers'])->group(function () {
    Route::get('/', [Controller::class, 'viewHome'])->name('home');

    Route::prefix('auth')->name('auth.')->group(function () {
        Route::prefix('forgot-password')->group(function () {
            Route::post('', [AuthController::class, 'sendPasswordReset']);
            Route::get('', [AuthController::class, 'viewForgotPasswordPage'])->name('forgot-password');
        });
        Route::prefix('set-password')->group(function () {
            Route::post('', [AuthController::class, 'setPassword']);
            Route::get('', [AuthController::class, 'viewSetPasswordPage'])->name('set-password');
        });
        Route::prefix('login')->group(function () {
            Route::post('', [AuthController::class, 'login']);
            Route::get('', [AuthController::class, 'viewLoginPage'])->name('login');
        });
        Route::prefix('logout')->name('logout')->group(function () {
            Route::get('', [AuthController::class, 'logout']);
        });
    });

    Route::middleware(['auth'])->group(function () {
        Route::prefix('portfolio')->name('portfolio.')->group(function () {
            Route::prefix('delete')->name('delete')->group(function () {
                Route::get('{id}', [PortfolioController::class, 'deletePortfolio']);
            });
            Route::prefix('manage')->name('manage')->group(function () {
                Route::get('{id}', [PortfolioController::class, 'getManagePortfolio']);
            });
            Route::prefix('update')->name('update')->group(function () {
                Route::post('{id}', [PortfolioController::class, 'updatePortfolio']);
            });
            Route::prefix('create')->group(function () {
                Route::post('', [PortfolioController::class, 'createPortfolio']);
                Route::get('', [PortfolioController::class, 'getCreatePortfolio'])->name('create');
            });
            Route::get('', [PortfolioController::class, 'viewPortfolio'])->name('overview');
        });
        Route::prefix('symbol')->name('symbol.')->group(function () {
            Route::prefix('toggle-position')->name('position')->group(function () {
                Route::get('{id}', [SymbolController::class, 'togglePosition']);
            });
            Route::prefix('remove')->name('remove')->group(function () {
                Route::get('{id}', [SymbolController::class, 'removeSymbol']);
            });
            Route::prefix('edit')->name('edit')->group(function () {
                Route::post('{id}', [SymbolController::class, 'editSymbol']);
            });
            Route::prefix('create')->name('create')->group(function () {
                Route::post('{portfolioId}', [SymbolController::class, 'createSymbol']);
            });
            Route::prefix('view')->name('view')->group(function () {
                Route::prefix('closed')->name('.closed')->group(function () {
                    Route::get('{id}', [SymbolController::class, 'viewClosedSymbol']);
                });
                Route::prefix('open')->name('.open')->group(function () {
                    Route::get('{id}', [SymbolController::class, 'viewOpenSymbol']);
                });
            });
        });
        Route::prefix('transaction')->name('transaction.')->group(function () {
            Route::prefix('remove')->name('remove')->group(function () {
                Route::get('{id}', [TransactionController::class, 'removeTransaction']);
            });
            Route::prefix('edit')->group(function () {
                Route::post('{id}', [TransactionController::class, 'editTransaction']);
                Route::get('{id}', [TransactionController::class, 'getEditTransaction'])->name('edit');
            });
            Route::prefix('view')->name('view')->group(function () {
                Route::get('{id}', [TransactionController::class, 'viewTransaction']);
            });
            Route::prefix('create')->group(function () {
                Route::post('{symbolId}', [TransactionController::class, 'createTransaction']);
                Route::get('{symbolId}', [TransactionController::class, 'getCreateTransaction'])->name('create');
            });
        });
        Route::prefix('historic-price')->name('historic-price.')->group(function () {
            Route::prefix('remove')->name('remove')->group(function () {
                Route::get('{id}', [HistoricPriceController::class, 'removeHistoricPrice']);
            });
            Route::prefix('edit')->group(function () {
                Route::post('{id}', [HistoricPriceController::class, 'editHistoricPrice']);
                Route::get('{id}', [HistoricPriceController::class, 'getEditHistoricPrice'])->name('edit');
            });
            Route::prefix('create')->group(function () {
                Route::post('{symbolId}', [HistoricPriceController::class, 'createHistoricPrice']);
                Route::get('{symbolId}', [HistoricPriceController::class, 'getCreateHistoricPrice'])->name('create');
            });
        });
    });
});
